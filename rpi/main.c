#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <math.h>
#include <wiringPiSPI.h>
#include <bcm2835.h>

#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>

#include <errno.h>
#include <unistd.h>
#include <sys/time.h>

#define SPI_CHANNEL 0
#define SPI_SPEED 1000000
#define WAIT_MICROS 10000
#define PORT 5000

/*define command bits, first 3 bits count*/
#define CMD_CH_READ_DIR 0b00010000
#define CMD_CH_READ_REG 0b00100000 /*1 MUL must by 1 */
#define CMD_RG_READ     0b01000000
#define CMD_RG_WRITE    0b01100000
#define CMD_RESET       0b11000000 /* 0b110xxxxx,  x=rest don't care */

#define CONFIG0 0x00 /* 0 SPIRST MUXMOD BYPAS CLKENB CHOP STAT 0 */ /* 0000001010 */
#define CONFIG1 0x01 /* IDLMOD DLY2 DLY1 DLY0 SBSC1 SBSC0 DRATE1 DRATE0 */
#define MUXSCH  0x02 /* AINP3 AINP2 AINP1 AINP0 AINN3 AINN2 AINN1 AINN0 */
#define MUXDIF  0x03 /* DIFF7 DIFF6 DIFF5 DIFF4 DIFF3 DIFF2 DIFF1 DIFF0 */
#define MUXSG0  0x04 /* AIN7 AIN6 AIN5 AIN4 AIN3 AIN2 AIN1 AIN0 */ /* 11111111 */
#define MUXSG1  0x05 /* AIN15 AIN14 AIN13 AIN12 AIN11 AIN10 AIN9 AIN8 */
#define SYSRED  0x06 /* 0 0 REF GAIN TEMP VCC 0 OFFSET */
#define GPIOC   0x07 /* CIO7 CIO6 CIO5 CIO4 CIO3 CIO2 CIO1 CIO0 */
#define GPIOD   0x08 /* DIO7 DIO6 DIO5 DIO4 DIO3 DIO2 DIO1 DIO0 */
#define ID      0x09 /* ID7 ID6 ID5 ID4 ID3 ID2 ID1 ID0 */

int listen_connect(void)
{
	int listenfd = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int));
	
	struct sockaddr_in serv_addr;
	memset((void*)&serv_addr, 0, sizeof(struct sockaddr_in));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(PORT);

	bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(struct sockaddr_in));
	printf("listening\n");
	listen(listenfd, 10);

	int sockfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
	close(listenfd);
	return sockfd;
}

struct data_acquiring {
	int sockfd, ongoing;
	pthread_mutex_t mut;
};

unsigned char read_reg(unsigned char reg)
{
	unsigned char cmd[] = { reg | CMD_RG_READ, 0 };
	wiringPiSPIDataRW(SPI_CHANNEL, cmd, 2);
	printf("reading %d %d\n", reg, cmd[1]);
	return cmd[1];
}

void write_reg(unsigned char add, unsigned char val)
{
	unsigned char cmd[] = { add | CMD_RG_WRITE, val };
	wiringPiSPIDataRW(SPI_CHANNEL, cmd, 2);
	printf("writing %d %d\n", add, val);
}

void* acquire_data(void* arg)
{
	struct timeval t1, t2;
	gettimeofday(&t1, NULL);

	struct data_acquiring* dacq = arg;
	const unsigned int buff_len = 2000;
	unsigned char buff[buff_len + 1];
	unsigned int i = 0;

	while (dacq->ongoing) {
		if (i >= 2000) {
			pthread_mutex_lock(&dacq->mut);
			send(dacq->sockfd, buff, buff_len, 0);
			pthread_mutex_unlock(&dacq->mut);
			i = 0;
		}

		gettimeofday(&t2, NULL);
		int micros = (t2.tv_sec - t1.tv_sec) * 100000;
		micros += (t2.tv_usec - t1.tv_usec) / 10;
		*(int*)(buff + i) = micros;

		unsigned char cmd[] = { CMD_CH_READ_REG, 0, 0, 0, 0 };
		unsigned char* data = buff + i + sizeof(int);
		pthread_mutex_lock(&dacq->mut);
		wiringPiSPIDataRW(SPI_CHANNEL, cmd, 5);
		pthread_mutex_unlock(&dacq->mut);

		memcpy(data, cmd + 1, 4);
		if (data[0] & 0b10000000) {
			printf("%d %d %d %d\n", data[0], data[1], data[2], data[3]);

			unsigned char man = data[1];
			data[1] = data[3];
			data[3] = man;
			
			i += sizeof(int) + 4;
		}
		usleep(250);
	}

	return NULL;
}

#define SYNC 0xA0
#define READ 0xA1
#define WRITE 0xA2
#define ENDP 0xA3
#define LCMD 4

unsigned char get_checksum(unsigned char* buff, int len)
{
    unsigned char b = 0;
    for (int i = 0; i < len - 1; i++)
        b ^= buff[i];
    return b;
}

void reset(void)
{
	unsigned char cmd = CMD_RESET;
	wiringPiSPIDataRW(SPI_CHANNEL, &cmd, 1);
	usleep(WAIT_MICROS);
}

void change_channels(unsigned char m1, unsigned char m0)
{
	write_reg(MUXSG0, m0);
	usleep(WAIT_MICROS);

	write_reg(MUXSG1, m1);
	usleep(WAIT_MICROS);
}

void init_ads(void)
{
	reset();
	write_reg(CONFIG0, 0b00011010);
    write_reg(CONFIG1, 0b10000011);
    change_channels(0b00000000, 0b000010000);

    unsigned char cmd = CMD_CH_READ_REG;
    wiringPiSPIDataRW(SPI_CHANNEL, &cmd, 1);
    usleep(WAIT_MICROS);
}

void receive_commands(struct data_acquiring* dacq)
{
	unsigned char resp[LCMD] = { SYNC, 1, 1, 1 };

	int ok = 1;
	while (ok) {
		unsigned char v[4];
		if (recv(dacq->sockfd, v, LCMD, 0) < LCMD)
			continue;

		pthread_mutex_lock(&dacq->mut);
		usleep(300);
		switch (v[0]) {
		case READ:
			resp[1] = v[1];
			resp[2] = read_reg(v[1]);
			resp[3] = get_checksum(resp, LCMD);

			send(dacq->sockfd, resp, LCMD, 0);
			break;

		case WRITE:
			write_reg(v[1], v[2]);
			break;

		case ENDP:
			ok = 0;
			break;

		default:
			break;
		}
		usleep(300);
		pthread_mutex_unlock(&dacq->mut);
	}
}

void proba(void)
{
	int count = 0;
	while (1) {
		unsigned char cmd[] = { CMD_CH_READ_REG, 0, 0, 0, 0 };
		// unsigned char m1 = read_reg(MUXSG1);
		// unsigned char m0 = read_reg(MUXSG0);
		wiringPiSPIDataRW(SPI_CHANNEL, cmd, 5);

		if (cmd[1] & 0b10000000 && !(cmd[1] & 0b01000000))
			printf("%d %d %d %d\n", cmd[1] & 0b10000000, cmd[2], cmd[3], cmd[4]);
		usleep(250);
	}
}

int main(int argc, char* argv[])
{
	wiringPiSPISetup(SPI_CHANNEL, SPI_SPEED);
	init_ads();

	// proba();

	while (1) {
		struct data_acquiring dacq;
		dacq.sockfd = listen_connect();
		dacq.ongoing = 1;
		pthread_mutex_init(&dacq.mut, NULL);

		printf("connected\n");
		pthread_t thidacq;
		pthread_create(&thidacq, NULL, acquire_data, (void*)&dacq);

		receive_commands(&dacq);

		printf("done\n");
		dacq.ongoing = 0;
		pthread_join(thidacq, NULL);

		unsigned char cmd[] = { SYNC, 0, 0, 0 };
		printf("trimit en\n");
		cmd[3] = get_checksum(cmd, LCMD);
		send(dacq.sockfd, cmd, LCMD, 0);
		pthread_mutex_destroy(&dacq.mut);
		close(dacq.sockfd);
	}
	return 0;
}
