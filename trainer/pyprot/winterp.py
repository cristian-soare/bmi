#!/usr/bin/python2

import sys
from xml.etree import ElementTree
from edflib.edfreader import EdfReader
from sets import Set

import numpy as np
import pylab as pl
import matplotlib.mlab as mlab

from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Convolution2D
from keras.optimizers import SGD

class Signal:
	def __init__(self, samples, rate, label, annotation):
		self.samples = samples
		self.rate = rate
		self.channel = label
		self.annotation = annotation

	def level_samples(self, n):
		self.samples = self.samples[0:n]

	def check_compat(self, sig):
		if self.rate != sig.rate or len(self.samples) != len(sig.samples):
			raise Exception('Incompatible signals')

	def specgram(self):
		nfft = self.rate / 3
		(s, f, t, im) = pl.specgram(self.samples, NFFT=int(nfft), 
			noverlap=int(nfft * 9 / 10), Fs=int(self.rate), window=mlab.window_hanning)
		return (s, f, t)

class EdfChannel:
	def __init__(self, edf, idx, anno_replace={}):
		self.samples = np.zeros(edf.samples_in_file(idx), dtype='float64')
		edf.readsignal(idx, 0, edf.samples_in_file(idx), self.samples)

		self.label = edf.signal_label(idx)
		self.rate = edf.samplefrequency(idx)
		self.duration = edf.file_duration

		self.annotations = edf.read_annotation()
		self.anno_replace = anno_replace
		self.subsig = None

	def split(self):
		if self.subsig is not None:
			return self.subsig

		self.subsig = []
		idx = lastidx = 0
		for i in range(len(self.annotations)):
			lim = self.annotations[i + 1][0] if i < len(self.annotations) - 1 else float('inf')

			while idx < len(self.samples) and idx / self.rate < lim:
				idx += 1

			anno = self.annotations[i][2]
			if self.anno_replace.has_key(anno):
				anno = self.anno_replace[anno]
			self.subsig.append(Signal(self.samples[lastidx:idx], self.rate, self.label, anno))
			lastidx = idx

		return self.subsig

class Interpreter:
	def __init__(self):
		self.signals = []
		self.labels = []

	def add_recordings(self, edfchannels):
		print 'to add', len(edfchannels[0].split())
		for i in range(len(edfchannels[0].split())):
			sgroup = []
			for chn in edfchannels:
				sgroup.append(chn.split()[i])
			self.signals.append(sgroup)
			self.labels.append(edfchannels[0].split()[i].annotation)

	def level_signals(self):
		minsamples = min(map(lambda sgroup:
			min(map(lambda sig: 
				len(sig.samples), sgroup)), self.signals))
		map(lambda sgroup: 
			map(lambda sig: 
				sig.level_samples(minsamples), sgroup), self.signals)

	def build(self):
		self.level_signals()
		print 'Signals leveled'

		map(lambda sgroup:
			map(lambda sig:
				sig.check_compat(self.signals[0][0]), sgroup), self.signals)
		print 'Signals checked'

		self.form_io()
		print 'IO built'

		self.init_mlp_model()
		print 'Model built'


	def form_io(self):
		self.inputs = []
		count = 0
		for i in self.signals:
			print '\rInputs built:', count + 1, '/', len(self.signals), self.labels[count],
			sys.stdout.flush()
			count += 1

			ingroup = []
			for j in i:
				(s, f, t) = j.specgram()
				# print '\n', s.shape
				# print f
				# print t
				# pl.show()
				# sys.exit(0)
				s /= s.max()
				ingroup.append(s)

			self.inputs.append(ingroup)
		self.inputs = np.array(self.inputs)
		self.freqs = f
		self.times = t
		print

		self.ldict = {}
		self.ndict = {}
		self.outputs = []
		count = 0
		for i in self.labels:
			if not self.ldict.has_key(i):
				self.ldict[count] = i
				self.ndict[i] = count
				count += 1
			outs = np.zeros(len(self.labels))
			outs[self.ndict[i]] = 1
			self.outputs.append(outs)
		self.outputs = np.array(self.outputs)

	def init_conv_model(self):
		self.model = Sequential()

		self.model.add(Convolution2D(32, 3, 3, 
			input_shape=(len(self.inputs[0]), len(self.inputs[0][0]), len(self.inputs[0][0][0]))))
		self.model.add(Activation('relu'))

		self.model.add(Convolution2D(32, 3, 3))
		self.model.add(Activation('relu'))

		self.model.add(Flatten())
		self.model.add(Dense(256))
		self.model.add(Activation('relu'))

		self.model.add(Dense(len(self.outputs)))
		self.model.add(Activation('softmax'))

		self.compile_model()

	def init_mlp_model(self):
		self.model = Sequential([
			Flatten(input_shape=(len(self.inputs[0]), len(self.inputs[0][0]), len(self.inputs[0][0][0]))),
			Dense(64),
			Activation('relu'),
			Dense(len(self.outputs)),
			Activation('softmax'),
		])

		self.compile_model()

	def compile_model(self):
		print 'Compiling model ...',
		sys.stdout.flush()
		sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
		self.model.compile(loss='categorical_crossentropy', optimizer=sgd)

	def train(self, lim=None):
		print 'Training ...',
		print len(self.inputs), len(self.outputs)
		if lim is None:
			lim = len(self.inputs)
		self.model.fit(self.inputs[:lim], self.outputs[:lim], batch_size=32)
		print 'completed'

	def eval(self, lim=None):
		print 'Evaluating'
		if lim is None:
			lim = 0
		score = self.model.evaluate(self.inputs[lim:], self.outputs[lim:], batch_size=32)
		print 'completed - score', score

if __name__ == '__main__':
	interp = Interpreter()
	xroot = ElementTree.parse(sys.argv[1]).getroot()

	allowed_channels = Set()
	for i in xroot.findall('channel'):
		allowed_channels.add(i.text)
	
	for i in xroot.findall('input'):
		anno_replace = {}
		for j in i.findall('label'):
			anno_replace[j.get('old')] = j.text

		edf = EdfReader(i.get('path'))
		edfchannels = []
		for j in range(edf.signals_in_file):
			if edf.signal_label(j).strip() in allowed_channels:
				edfchannels.append(EdfChannel(edf, j, anno_replace))

		interp.add_recordings(edfchannels)

	interp.build()
	interp.train(175)
	interp.eval(175)
