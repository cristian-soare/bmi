#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

struct learn_entity {
	float value, delta;
};
typedef struct learn_entity weight;
typedef struct learn_entity bias;

struct neuron {
	float value, error;
	bias b;
};
typedef struct neuron neuron;

struct single_layer {
	neuron* l;
	int len;
};
typedef struct single_layer single_layer;

struct full_con {
	weight** w;
	int n, m;
};
typedef struct full_con full_con;

struct config {
	single_layer** layers;
	full_con** connections;
	int num;
};
typedef struct config config;

struct train_data {
	neuron* input;
	float* ideal;
};
typedef struct train_data train_data;

struct data_set {
	train_data* td;
	int n;
};
typedef struct data_set data_set;

#define randomw() (rand() % 10000 - 5000) / 10000.
#define sigmoid(z) (1.0 / (1.0 + exp(-(z))))
#define deriv_sigout(y) ((y) * (1.0 - (y)))
#define pow2(x) ((x) * (x))

single_layer* new_single_layer(int n)
{
	single_layer* sl = malloc(sizeof(single_layer));
	sl->l = malloc(n * sizeof(neuron));
	sl->len = n;

	int i;
	for (i = 0; i < n; i++)
		sl->l[i].b.value = randomw();
	return sl;
}

void free_single_layer(single_layer* sl)
{
	free(sl->l);
	free(sl);
}

full_con* new_full_weights(int n, int m)
{
	int i, j;
	full_con* fc = malloc(sizeof(full_con));
	fc->n = n; fc->m = m;

	fc->w = malloc(n * sizeof(weight*));
	for (i = 0; i < n; i++)
		fc->w[i] = malloc(m * sizeof(weight));

	for (i = 0; i < n; i++)
		for (j = 0; j < m; j++) {
			fc->w[i][j].value = randomw();
			fc->w[i][j].delta = 0;
		}

	return fc;
}

void free_full_weights(full_con* fc)
{
	int i;
	for (i = 0; i < fc->n; i++)
		free(fc->w[i]);
	free(fc->w);
	free(fc);
}

config* read_config(char* fname)
{
	int i, x;
	char buf[20];

	config* cfg = malloc(sizeof(config));
	FILE* f = fopen(fname, "r");

	fscanf(f, "%d", &cfg->num);

	cfg->layers = malloc(cfg->num * sizeof(void*));
	for (i = 0; i < cfg->num; i++) {
		fscanf(f, "%s", buf);
		if (strcmp(buf, "input") == 0) {
			cfg->layers[i] = malloc(sizeof(single_layer));
			fscanf(f, "%d", &((single_layer*)cfg->layers[i])->len);
		} else if (strcmp(buf, "norm") == 0) {
			fscanf(f, "%d", &x);
			cfg->layers[i] = new_single_layer(x);
		}
	}

	cfg->connections = malloc((cfg->num - 1) * sizeof(void*));
	for (i = 0; i < cfg->num - 1; i++) {
		fscanf(f, "%s", buf);
		if (strcmp(buf, "full") == 0)
			cfg->connections[i] = new_full_weights(((single_layer*)cfg->layers[i])->len, 
												((single_layer*)cfg->layers[i + 1])->len);
	}

	fclose(f);
	return cfg;
}

void free_net(config* cfg)
{
	int i;
	for (i = 0; i < cfg->num; i++) {
		free_single_layer(cfg->layers[i]);
		if (i < cfg->num - 1)
			free_full_weights(cfg->connections[i]);
	}
	free(cfg->layers);
	free(cfg->connections);
	free(cfg);
}

#define TRAIN 0
#define RUN 1

data_set* read_data(char* fname, int len, int classes, int way)
{
	int i, j, n;
	FILE* f = fopen(fname, "r");

	fscanf(f, "%d", &n);
	train_data* td = malloc(n * sizeof(train_data));

	for (i = 0; i < n; i++) {
		td[i].input = malloc(len * sizeof(neuron));
		for (j = 0; j < len; j++) {
			fscanf(f, "%f", &td[i].input[j].value);
			td[i].input[j].value += 0.0;
		}

		if (way == TRAIN) {
			td[i].ideal = malloc(classes * sizeof(float));
			for (j = 0; j < classes; j++)
				fscanf(f, "%f", &td[i].ideal[j]);
		}
	}

	fclose(f);
	data_set* ds = malloc(sizeof(data_set));
	ds->td = td; ds->n = n;
	return ds;
}

void free_data_set(data_set* ds)
{
	int i;
	for (i = 0; i < ds->n; i++) {
		free(ds->td[i].input);
		free(ds->td[i].ideal);
	}
	free(ds->td);
	free(ds);
}

void full_feed_forward(neuron* l1, neuron* l2, weight** w, int n, int m)
{
	int i, j;
	float sum;

	#pragma omp parallel for private(i, sum)
	for (j = 0; j < m; j++) {
		sum = 0;
		for (i = 0; i < n; i++)
			sum += l1[i].value * w[i][j].value;
		sum += l2[j].b.value;
		l2[j].value = sigmoid(sum);
	}
}

/* backprop */

void out_errors(neuron* layer, float* ideal, int n)
{
	int i;
	#pragma omp parallel for
	for (i = 0; i < n; i++)
		layer[i].error = deriv_sigout(layer[i].value) * (ideal[i] - layer[i].value);
}

void error_backprop(neuron* l1, neuron* l2, weight** w, int n, int m)
{
	int i, j;
	float sumerr;

	#pragma omp parallel for private(sumerr, j)
	for (i = 0; i < n; i++) {
		sumerr = 0;
		for (j = 0; j < m; j++)
			sumerr += w[i][j].value * l2[j].error;
		l1[i].error = deriv_sigout(l1[i].value) * sumerr;
	}
}

void update_weight_deltas(neuron* l1, neuron* l2, weight** w, int n, int m)
{
	int i, j;
	#pragma omp parallel for private(j)
	for (i = 0; i < n; i++)
		for (j = 0; j < m; j++)
			w[i][j].delta += l1[i].value * l2[j].error;
}

void update_bias_deltas(neuron* l, int n)
{
	int i;
	#pragma omp parallel for
	for (i = 0; i < n; i++)
		l[i].b.delta += l[i].error;
}

/* learn */

void update_weights(weight** w, int n, int m, float lrate)
{
	int i, j;
	#pragma omp parallel for private(j)
	for (i = 0; i < n; i++)
		for (j = 0; j < m; j++)
			w[i][j].value += lrate * w[i][j].delta;
}

void update_bias(neuron* l, int n, float lrate)
{
	int i;
	#pragma omp parallel for
	for (i = 0; i < n; i++)
		l[i].b.value += lrate * l[i].b.delta;
}

float get_error(neuron* out, float* ideal, int n)
{
	int i;
	float localerr = 0;

	for (i = 0; i < n; i++)
		localerr += pow2(ideal[i] - out[i].value);
	return localerr / 2.0;
}

void clear_weight_deltas(weight** w, int n, int m)
{
	int i, j;
	#pragma omp parallel for private(j)
	for (i = 0; i < n; i++)
		for (j = 0; j < m; j++)
			w[i][j].delta = 0;
}

void clear_bias_deltas(neuron* l, int n)
{
	int i;
	#pragma omp parallel for
	for (i = 0; i < n; i++)
		l[i].b.delta = 0;
}

void free_matrix(void** v, int n)
{
	int i;
	for (i = 0; i < n; i++)
		free(v[i]);
	free(v);
}

#define MOMENTUM 0.7

inline void clear_deltas(config* cfg)
{
	int i, j, k;
	for (k = 0; k < cfg->num - 1; k++) {
		full_con* conn = (full_con*)cfg->connections[k];
		#pragma omp parallel for private(j)
		for (i = 0; i < conn->n; i++)
			for (j = 0; j < conn->m; j++)
				conn->w[i][j].delta = conn->w[i][j].delta * MOMENTUM;

		single_layer* sl = (single_layer*)cfg->layers[k + 1];
		#pragma omp parallel for
		for (i = 0; i < sl->len; i++)
			sl->l[i].b.delta = sl->l[i].b.delta * MOMENTUM;
	}
}

inline void feed_forward(config* cfg, train_data td)
{
	int i;
	cfg->layers[0]->l = td.input;
	for (i = 0; i < cfg->num - 1; i++)
		full_feed_forward(cfg->layers[i]->l, cfg->layers[i + 1]->l, 
							cfg->connections[i]->w, 
							cfg->layers[i]->len, cfg->layers[i + 1]->len);
}

inline void backprop(config* cfg, train_data td)
{
	int i;
	cfg->layers[0]->l = td.input;

	out_errors(cfg->layers[cfg->num - 1]->l, td.ideal, cfg->layers[cfg->num - 1]->len);
	for (i = cfg->num - 2; i > 0; i--)
		error_backprop(cfg->layers[i]->l, cfg->layers[i + 1]->l, 
						cfg->connections[i]->w, 
						cfg->layers[i]->len, cfg->layers[i + 1]->len);

	for (i = cfg->num - 2; i >= 0; i--) {
		update_weight_deltas(cfg->layers[i]->l, cfg->layers[i + 1]->l, 
						cfg->connections[i]->w, 
						cfg->layers[i]->len, cfg->layers[i + 1]->len);
		update_bias_deltas(cfg->layers[i + 1]->l, cfg->layers[i + 1]->len);
	}
}

#define LRATE 0.2

inline void learn(config* cfg)
{
	int i;
	for (i = 0; i < cfg->num - 1; i++) {
		full_con* fc = cfg->connections[i];
		single_layer* sl = cfg->layers[i + 1];

		update_weights(fc->w, fc->n, fc->m, LRATE);
		update_bias(sl->l, sl->len, LRATE);
	}
}

void train(config* cfg, data_set* ds)
{
	float totalerror = 1;
	int count = 0;

	while (1) {
		clear_deltas(cfg);

		int i;
		totalerror = 0;
		for (i = 0; i < ds->n; i++) {
			feed_forward(cfg, ds->td[i]);

			single_layer* sl = cfg->layers[cfg->num - 1];
			totalerror += get_error(sl->l, ds->td[i].ideal, sl->len);
			
			backprop(cfg, ds->td[i]);
		}

		if (totalerror <= 0.0001)
			break;
		learn(cfg);

		count++;
		if (count % 100 == 0)
			printf("%f\n", totalerror);
	}
	printf("%f in %d epochs\n", totalerror, count);
}

inline void print_vec(neuron* v, int n)
{
	int i;
	for (i = 0; i < n; i++)
		printf("%f ", v[i].value);
	printf("\n");
}

void run(config* cfg, data_set* ds)
{
	int i;
	for (i = 0; i < ds->n; i++) {
		feed_forward(cfg, ds->td[i]);
		single_layer* sl = cfg->layers[cfg->num - 1];
		print_vec(sl->l, sl->len);
	}
}

inline void usage(char* name)
{
	printf("Usage:\n"
		"training: %s -f net_configuration -s training_set -o save_path\n"
		"classifying: %s -f net_configuration -n trained_net -i inputs_to_be_classified\n",
		name, name);
	exit(0);
}

void save_load_net(config* cfg, char* savepath, int way)
{
	int i, j, k;
	FILE* f = NULL;

	switch (way) {
		case TRAIN: f = fopen(savepath, "wb"); break;
		case RUN: f = fopen(savepath, "rb"); break;
	}

	for (k = 0; k < cfg->num - 1; k++) {
		full_con* fc = cfg->connections[k];
		for (i = 0; i < fc->n; i++)
			for (j = 0; j < fc->m; j++)
				switch (way) {
					case TRAIN: fwrite(&fc->w[i][j].value, sizeof(float), 1, f); break;
					case RUN: fread(&fc->w[i][j].value, sizeof(float), 1, f); break;
				}

		single_layer* sl = cfg->layers[k + 1];
		for (i = 0; i < sl->len; i++)
			switch (way) {
				case TRAIN: fwrite(&sl->l[i].b.value, sizeof(float), 1, f); break;
				case RUN: fread(&sl->l[i].b.value, sizeof(float), 1, f); break;
			}
	}

	fclose(f);
}

int main(int argc, char* argv[])
{
	srand(time(NULL));

	if (argc < 5 || argc > 7) usage(argv[0]);

	int way = 0;
	char *netcfg, *netset, *savepath;
	netcfg  = netset = savepath = NULL;

	int i;
	for (i = 1; i < argc; i++)
		if (strcmp(argv[i], "-f") == 0)
			netcfg = argv[i + 1];
		else if (strcmp(argv[i], "-n") == 0) {
			savepath = argv[i + 1];
			way = RUN;
		} else if (strcmp(argv[i], "-i") == 0)
			netset = argv[i + 1];
		else if (strcmp(argv[i], "-s") == 0) {
			netset = argv[i + 1];
			way = TRAIN;
		} else if (strcmp(argv[i], "-o") == 0)
			savepath = argv[i + 1];

	config* cfg = NULL;
	data_set* ds = NULL;

	if (netcfg) cfg = read_config(netcfg);
	else usage(argv[0]);

	if (netset) 
		ds = read_data(netset, 
			((single_layer*)cfg->layers[0])->len,
			((single_layer*)cfg->layers[cfg->num - 1])->len,
			way);
	else 
		usage(argv[0]);

	printf("retea ");
	for (i = 0; i < cfg->num; i++)
		printf("%d ", cfg->layers[i]->len);
	printf("\nweights ");
	for (i = 0; i < cfg->num - 1; i++)
		printf("%dx%d ", cfg->connections[i]->n, cfg->connections[i]->m);
	printf("\n");

	switch (way) {
		case TRAIN:
			train(cfg, ds);
			save_load_net(cfg, savepath, TRAIN);
			run(cfg, ds);
			break;

		case RUN:
			save_load_net(cfg, savepath, RUN);
			run(cfg, ds);
			break;
	}

	cfg->layers[0]->l = NULL;
	free_net(cfg);
	free_data_set(ds);

	return 0;
}
