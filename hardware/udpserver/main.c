#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>

#include <unistd.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#define DEVNAME "/dev/ads1258"
#define PORT 5000

#define RECVBUF 20
#define SENDBUF 1000000

#define CREAD 0xA1
#define CWRITE 0xA2

static void die(char s[])
{
	perror(s);
	exit(-1);
}

struct sdata {
	int sfd, fd;
	struct sockaddr_in me, other;
	pthread_mutex_t mut;
};

void* run(void* arg)
{
	uint8_t buf[RECVBUF];
	struct sdata* sd = arg;
	socklen_t slen = sizeof(sd->other);

	while (1) {
		recvfrom(sd->sfd, buf, RECVBUF, 0, (struct sockaddr*)&sd->other, &slen);
		printf("primit ceva orice\n");

		pthread_mutex_lock(&sd->mut);
		switch (buf[0]) {
		case CREAD:
			printf("incerc sa citesc %x\n", buf[1]);
			buf[2] = ioctl(sd->fd, CREAD, buf[1]);
			sendto(sd->sfd, buf + 1, 2, 0, (struct sockaddr*)&sd->other, sizeof(sd->other));
			break;

		case CWRITE:
			printf("incerc sa scriu\n");
			ioctl(sd->fd, buf[1], buf[2]);
			// write(sd->fd, buf + 1, recvlen - 1);
			break;

		default: 
			break;
		}
		pthread_mutex_unlock(&sd->mut);
	}

	return NULL;
}

int gcount;

void *cacat(void *param)
{
	while (1) {
		printf("%d\n", gcount);
		gcount = 0;
		usleep(1000000);
	}
	return NULL;
}

int main(void)
{
	struct sdata sd;
	sd.fd = open(DEVNAME, O_RDWR);
	if (sd.fd < 0)
		die("cannot open device " DEVNAME);

	if ((sd.sfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		die("socket");

	memset(&sd.me, 0, sizeof(struct sockaddr_in));
	sd.me.sin_family = AF_INET;
	sd.me.sin_port = htons(PORT);
	sd.me.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(sd.sfd, (struct sockaddr*)&sd.me, sizeof(sd.me)) < 0)
		die("bind");

	pthread_t thr;
	pthread_mutex_init(&sd.mut, NULL);
	pthread_create(&thr, NULL, run, &sd);

	pthread_t cthr;
	pthread_create(&cthr, NULL, cacat, NULL);

	char buf[SENDBUF];
	while (1) {
		pthread_mutex_lock(&sd.mut);
		int count = read(sd.fd, buf, SENDBUF);
		gcount += count / 8;
		// printf("count %d\n", count);
		sendto(sd.sfd, buf, count, 0, (struct sockaddr*)&sd.other, sizeof(sd.other));
		pthread_mutex_unlock(&sd.mut);
	}

	close(sd.fd);
	return 0;
}
