#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/time.h>

#include <linux/kthread.h>
#include <linux/delay.h>

#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/spi/spi.h>
#include <linux/slab.h>

#include <linux/fs.h>
#include <asm/uaccess.h>

#include "bcm2835spi.h"
 
 
#define DRIVER_AUTHOR "Igor <hardware.coder@gmail.com>"
#define DRIVER_DESC   "Tnterrupt Test"
 
// we want GPIO_17 (pin 11 on P5 pinout raspberry pi rev. 2 board)
// to generate interrupt
#define GPIO_ANY_GPIO                17
#define GPIO_CS 5
 
// text below will be seen in 'cat /proc/interrupt' command
#define GPIO_ANY_GPIO_DESC           "Some gpio pin description"
 
// below is optional, used in more complex code, in our case, this could be
// NULL
#define GPIO_ANY_GPIO_DEVICE_DESC    "some_device"

/*define command bits, first 3 bits count*/
#define CMD_CH_READ_DIR 0b00010000
#define CMD_CH_READ_REG 0b00110000 /*1 MUL must by 1 */
#define CMD_RG_READ     0b01000000
#define CMD_RG_WRITE    0b01100000
#define CMD_RESET       0b11000000 /* 0b110xxxxx,  x=rest don't care */
#define CMD_MUL			0b00010000

#define CONFIG0 0x00 /* 0 SPIRST MUXMOD BYPAS CLKENB CHOP STAT 0 */ /* 0000001010 */
#define CONFIG1 0x01 /* IDLMOD DLY2 DLY1 DLY0 SBSC1 SBSC0 DRATE1 DRATE0 */
#define MUXSCH  0x02 /* AINP3 AINP2 AINP1 AINP0 AINN3 AINN2 AINN1 AINN0 */
#define MUXDIF  0x03 /* DIFF7 DIFF6 DIFF5 DIFF4 DIFF3 DIFF2 DIFF1 DIFF0 */
#define MUXSG0  0x04 /* AIN7 AIN6 AIN5 AIN4 AIN3 AIN2 AIN1 AIN0 */ /* 11111111 */
#define MUXSG1  0x05 /* AIN15 AIN14 AIN13 AIN12 AIN11 AIN10 AIN9 AIN8 */
#define SYSRED  0x06 /* 0 0 REF GAIN TEMP VCC 0 OFFSET */
#define GPIOC   0x07 /* CIO7 CIO6 CIO5 CIO4 CIO3 CIO2 CIO1 CIO0 */
#define GPIOD   0x08 /* DIO7 DIO6 DIO5 DIO4 DIO3 DIO2 DIO1 DIO0 */
#define ID      0x09 /* ID7 ID6 ID5 ID4 ID3 ID2 ID1 ID0 */

static uint8_t read_reg(uint8_t reg)
{
	char buf[] = { reg | CMD_RG_READ, 0 };
	bcm2835_spi_transfernb(buf, buf, 2);
	return buf[1];
}

static void write_reg(uint8_t add, uint8_t val)
{
	char buf[] = { add | CMD_RG_WRITE, val };
	bcm2835_spi_transfernb(buf, buf, 2);
}

static void reset(void)
{
	bcm2835_spi_transfer(CMD_RESET);
	msleep(10);
}

static void change_channels(uint8_t m1, uint8_t m0)
{
	write_reg(MUXSG0, m0);
	msleep(10);

	write_reg(MUXSG1, m1);
	msleep(10);
}

static void init_ads(void)
{
	int i;

	reset();
	write_reg(CONFIG0, 0b00001010);
	write_reg(CONFIG1, 0b10000010);
	
	write_reg(MUXSCH, 1);
	change_channels(0b01000000, 0b000000000);
	for (i = 0; i < 8; i++)
		printk("reg %x %x\n", i, read_reg(i));
	msleep(10);
}
 
/****************************************************************************/
/* Interrupts variables block                                               */
/****************************************************************************/
static short int irq_any_gpio = 0;
static uint8_t *bfull, *bempty;
static uint32_t idx;

struct timeval reftm;

#define BUFF_SIZE 1000000
#define HALF_SIZE 2000

wait_queue_head_t thwq;
static atomic_t wqcond;
static spinlock_t slock;

/****************************************************************************/
/* IRQ handler - fired on interrupt                                         */
/****************************************************************************/
static irqreturn_t r_irq_handler(int irq, void *dev_id, struct pt_regs *regs) 
{ 
	struct timeval tm;
	uint32_t elapsed;
	//unsigned long flags;
	//local_irq_save(flags);
	spin_lock(&slock);

	if (idx == BUFF_SIZE) {
		// printk("shit reached\n");
		spin_unlock(&slock);
		return IRQ_HANDLED;
	}
 
 	bempty[idx] = 0;
	bcm2835_spi_transfernb(bempty + idx, bempty + idx, 4);
	idx += 4;

	do_gettimeofday(&tm);
	elapsed = (tm.tv_sec - reftm.tv_sec) * 1000000;
	elapsed += tm.tv_usec - reftm.tv_usec;
	*(uint32_t*)(bempty + idx) = elapsed;
	idx += 4;

	if (idx >= HALF_SIZE) {
		atomic_set(&wqcond, 1);
		wake_up_interruptible(&thwq);
	}

	// gpio_set_value(26, 1);
	// gpio_set_value(26, 0);
 
	//local_irq_restore(flags);
	spin_unlock(&slock);
	return IRQ_HANDLED;
}
 
/****************************************************************************/
/* This function configures interrupts.                                     */
/****************************************************************************/
void r_int_config(void) 
{ 
	spin_lock_init(&slock);
   if (gpio_request(GPIO_ANY_GPIO, GPIO_ANY_GPIO_DESC)) {
	  printk("GPIO request faiure: %s\n", GPIO_ANY_GPIO_DESC);
	  return;
   }
   if ( (irq_any_gpio = gpio_to_irq(GPIO_ANY_GPIO)) < 0 ) {
	  printk("GPIO to IRQ mapping faiure %s\n", GPIO_ANY_GPIO_DESC);
	  return;
   }
 
   printk(KERN_NOTICE "Mapped int %d\n", irq_any_gpio);
 
   if (request_irq(irq_any_gpio,
				   (irq_handler_t ) r_irq_handler,
				   IRQF_TRIGGER_FALLING,
				   GPIO_ANY_GPIO_DESC,
				   GPIO_ANY_GPIO_DEVICE_DESC)) {
	  printk("Irq Request failure\n");
	  return;
   }
}

static int opened = 0;
static uint32_t tocopy;

static void pause_data(unsigned long *flags)
{
	spin_lock_irqsave(&slock, *flags);
	gpio_set_value(GPIO_CS, 1);
	gpio_set_value(GPIO_CS, 0);
	// msleep(100);
}

static void resume_data(unsigned long flags)
{
	//tocopy = idx = 0;
	//wqcond = 0;
	spin_unlock_irqrestore(&slock, flags);
	// msleep(100);
}

static int ads1258_open(struct inode* inode, struct file* file)
{
	unsigned long flags;

	if (opened) {
		printk("already opened !\n");
		return -EBUSY;
	}
	
	printk("opened\n");
	spin_lock_irqsave(&slock, flags);
	do_gettimeofday(&reftm);
	resume_data(flags);
	opened = 1;
	return 0;
}

static int ads1258_release(struct inode* inode, struct file* file)
{
	opened = 0;
	printk("closed\n");
	return 0;
}

static int ads1258_read(struct file* file, char* buffer, size_t len, loff_t* offset)
{
	unsigned long flags;
	uint32_t minlen;

	/*if (len < 12) {
		int ret;

		pause_data(&flags);
		ret = do_regs(CMD_RG_READ, buffer, len - 1) + 1;
		*offset = 0;
		resume_data(flags);
		return 3;
	}*/

	if (!tocopy) {
		uint8_t *manbuf;

		wait_event_interruptible(thwq, atomic_read(&wqcond) == 1);
		spin_lock_irqsave(&slock, flags);

		
		manbuf = bempty;
		bempty = bfull;
		bfull = manbuf;

		tocopy = idx;
		*offset = 0;
		idx = 0;

		atomic_set(&wqcond, 0);
		spin_unlock_irqrestore(&slock, flags);
	}

	minlen = len < tocopy ? len : tocopy;
	(void)copy_to_user(buffer, bfull + *offset, minlen);

	*offset += minlen;
	tocopy -= minlen;
	return minlen;
}

static long ads1258_ioctl(struct file *file, unsigned int cmd, unsigned long param)
{
	unsigned long flags;
	uint8_t ret = 0;
	
	pause_data(&flags);
	if (cmd == 0xA1)
		ret = read_reg(param);
	else
		write_reg(cmd, param);
	resume_data(flags);	
	return ret;
}

static struct file_operations fops = {
	.read = ads1258_read,
	.open = ads1258_open,
	.unlocked_ioctl = ads1258_ioctl,
	.release = ads1258_release,
};
 
/****************************************************************************/
/* This function releases interrupts.                                       */
/****************************************************************************/
void r_int_release(void) 
{ 
   free_irq(irq_any_gpio, GPIO_ANY_GPIO_DEVICE_DESC);
   gpio_free(GPIO_ANY_GPIO);
}

/****************************************************************************/
/* Module init / cleanup block.                                             */
/****************************************************************************/
static int major;
#define DEVICE_NAME "ads1258"

int r_init(void) 
{
	printk(KERN_NOTICE "Hello !\n");
	major = register_chrdev(0, DEVICE_NAME, &fops);
	bfull = kzalloc(BUFF_SIZE, GFP_KERNEL);
	bempty = kzalloc(BUFF_SIZE, GFP_KERNEL);
	init_waitqueue_head(&thwq);
	gpio_direction_output(26, 0);
	gpio_direction_output(GPIO_CS, 0);
   
	bcm2835_init();   
	bcm2835_spi_begin();
	bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);      // The default
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);                   // The default
	bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_64);    // ~ 4 MHz
	bcm2835_spi_chipSelect(BCM2835_SPI_CS0);                      // The default
	bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW);      // the default
  
  	init_ads();
	do_gettimeofday(&reftm);
	r_int_config();
 
	return 0;
}
 
void r_cleanup(void) 
{
   printk(KERN_NOTICE "Goodbye\n");
   r_int_release();
   bcm2835_spi_end();
   bcm2835_close();

   kfree(bfull);
   kfree(bempty);
   unregister_chrdev(major, DEVICE_NAME);
}

module_init(r_init);
module_exit(r_cleanup);
 
 
/****************************************************************************/
/* Module licensing/description block.                                      */
/****************************************************************************/
MODULE_LICENSE("GPL");
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);

