/*********************************************************************
 *
 *  Main Application Entry Point and TCP/IP Stack Demo
 *  Module for Microchip TCP/IP Stack
 *   -Demonstrates how to call and use the Microchip TCP/IP stack
 *   -Reference: Microchip TCP/IP Stack Help (TCPIP Stack Help.chm)1150

 *
 *********************************************************************
 * FileName:        MainDemo.c
 * Dependencies:    TCPIP.h
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.11b or higher
 *                  Microchip C30 v3.24 or higher
 *                  Microchip C18 v3.36 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2010 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *      ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *      used in conjunction with a Microchip ethernet controller for
 *      the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 * File Description:
 * Change History:
 * Rev   Description
 * ----  -----------------------------------------
 * 1.0   Initial release
 * V5.36 ---- STACK_USE_MPFS support has been removed 
 ********************************************************************/
/*
 * This macro uniquely defines this file as the main entry point.
 * There should only be one such definition in the entire project,
 * and this file must define the AppConfig variable as described below.
 */
#define THIS_IS_STACK_APPLICATION

// Include all headers for any enabled TCPIP Stack functions
#include "TCPIP Stack/TCPIP.h"

// Include functions specific to this stack application
#include "MainDemo.h"

// Used for Wi-Fi assertions
#define WF_MODULE_NUMBER   WF_MODULE_MAIN_DEMO

// Declare AppConfig structure and some other supporting stack variables
APP_CONFIG AppConfig;
static unsigned short wOriginalAppConfigChecksum;    // Checksum of the ROM defaults for AppConfig
BYTE AN0String[8];


// Private helper functions.
// These may or may not be present in all applications.
static void InitAppConfig(void);
static void InitializeBoard(void);

//
// PIC18 Interrupt Service Routines
// 
// NOTE: Several PICs, including the PIC18F4620 revision A3 have a RETFIE FAST/MOVFF bug
// The interruptlow keyword is used to work around the bug when using C18
    
#if defined(__C32__)
    void _general_exception_handler(unsigned cause, unsigned status)
    {
        Nop();
        Nop();
    }
#endif

#include <p32xxxx.h>

void spi_init ()
{
    char dummy;

    TRISDbits.TRISD10 = 0; // sck output
    TRISCbits.TRISC4 = 1; // sdi input
    TRISDbits.TRISD0 = 0; // sdo output
    PORTDbits.RD0 = 0;

    SPI1CONbits.ON      = 0;        // disable SPI to reset any previous state
    dummy               = SPI1BUF;  // clear receive buffer
    SPI1BRG             = 9;        // PBCLK / 2 / baud - 1
    SPI1CONbits.MSTEN   = 1;        // enable master mode
    SPI1CONbits.CKE     = 1;        // set clock-to-data timing
    SPI1CONbits.ON      = 1;        // turn SPI on
}

inline char spi_put_get_char (char c)
{
    while (!SPI1STATbits.SPITBE);  // wait until SPI transmission complete
    SPI1BUF = c;                   // send data to slave
    while (!SPI1STATbits.SPIRBF);  // wait until SPI transmission complete
    return SPI1BUF;
}

/*define command bits, first 3 bits count*/
#define CMD_CH_READ_DIR 0b00010000
#define CMD_CH_READ_REG 0b00110000 /*1 MUL must by 1 */
#define CMD_RG_READ     0b01000000
#define CMD_RG_WRITE    0b01100000
#define CMD_RESET       0b11000000 /* 0b110xxxxx,  x=rest don't care */

#define CONFIG0 0x00 /* 0 SPIRST MUXMOD BYPAS CLKENB CHOP STAT 0 */ /* 0000001010 */
#define CONFIG1 0x01 /* IDLMOD DLY2 DLY1 DLY0 SBSC1 SBSC0 DRATE1 DRATE0 */
#define MUXSCH  0x02 /* AINP3 AINP2 AINP1 AINP0 AINN3 AINN2 AINN1 AINN0 */
#define MUXDIF  0x03 /* DIFF7 DIFF6 DIFF5 DIFF4 DIFF3 DIFF2 DIFF1 DIFF0 */
#define MUXSG0  0x04 /* AIN7 AIN6 AIN5 AIN4 AIN3 AIN2 AIN1 AIN0 */ /* 11111111 */
#define MUXSG1  0x05 /* AIN15 AIN14 AIN13 AIN12 AIN11 AIN10 AIN9 AIN8 */
#define SYSRED  0x06 /* 0 0 REF GAIN TEMP VCC 0 OFFSET */
#define GPIOC   0x07 /* CIO7 CIO6 CIO5 CIO4 CIO3 CIO2 CIO1 CIO0 */
#define GPIOD   0x08 /* DIO7 DIO6 DIO5 DIO4 DIO3 DIO2 DIO1 DIO0 */
#define ID      0x09 /* ID7 ID6 ID5 ID4 ID3 ID2 ID1 ID0 */

void reset(void)
{
    spi_put_get_char(CMD_RESET);
    DelayMs(10);
}

inline BYTE read_reg(BYTE reg)
{
    BYTE ret;
    spi_put_get_char(CMD_RG_READ | reg);
    ret = spi_put_get_char(0x00);
    return ret;
}

inline void write_reg(BYTE add, BYTE value)
{
    spi_put_get_char(CMD_RG_WRITE | add);
    spi_put_get_char(value);
}

void change_channels(BYTE m1, BYTE m0)
{
    spi_put_get_char(CMD_RG_WRITE | MUXSG0);
    spi_put_get_char(m0);
    DelayMs(10);

    spi_put_get_char(CMD_RG_WRITE | MUXSG1);
    spi_put_get_char(m1);
    DelayMs(10);
}

void init_ads(void)
{
    reset();
    write_reg(CONFIG0, 0b00011010);
    write_reg(CONFIG1, 0b10000011);
    change_channels(0b00001000, 0b00000000);
    DelayMs(10);
    spi_put_get_char(CMD_CH_READ_REG);
    DelayMs(10);
}

DWORD oldt;
inline void read_pack(BYTE* buff, int len)
{
    int i;
    for (i = 0; i < len; i += 8)
    {
        *(DWORD*)(buff + i) = (TickGet() - oldt) / (TICKS_PER_SECOND / 100000);
        spi_put_get_char(CMD_CH_READ_REG);
        buff[i + 4] = spi_put_get_char(0x00);
        buff[i + 7] = spi_put_get_char(0x00);
        buff[i + 6] = spi_put_get_char(0x00);
        buff[i + 5] = spi_put_get_char(0x00);
        if (!(buff[i + 4] & 0b10000000)) i -= 8;
    }
}

inline BYTE get_checksum(BYTE* buff, int len)
{
    int i; BYTE b = 0;
    for (i = 0; i < len - 1; i++)
        b ^= buff[i];
    return b;
}

#define SYNC 0xA0
#define READ 0xA1
#define WRITE 0xA2
#define LCMD 4
inline BYTE* interp_cmd(BYTE* buf)
{
    static BYTE resp[LCMD] = { SYNC, 1, 1, 1 };
    switch (buf[0])
    {
        case READ:
            resp[1] = buf[1];
            resp[2] = read_reg(buf[1]);
            resp[3] = get_checksum(resp, LCMD);
            return resp;
            break;

        case WRITE:
            write_reg(buf[1], buf[2]);
            return NULL;
            break;
        default:
            return NULL;
    }
}

inline void routine(TCP_SOCKET sockfd)
{
    int i;
    static BYTE toSend[2000];
    static BYTE toRecv[100];
    WORD incount;

    incount = TCPIsGetReady(sockfd);	// Get TCP RX FIFO byte count
    if (incount >= LCMD)
    {
        TCPGetArray(sockfd, toRecv, incount);
        if (incount % LCMD) incount -= incount % LCMD;

        BYTE* resp;
        for (i = 0; i < incount; i += LCMD)
        {
            resp = interp_cmd(toRecv + i);
            if (resp)
            {
                LED0_IO ^= 1;
                TCPPutArray(sockfd, resp, LCMD);
                TCPFlush(sockfd);
                spi_put_get_char(0x00);
            }
        }
        oldt = TickGet();
    }

    read_pack(toSend, 2000);
    TCPPutArray(sockfd, toSend, sizeof(toSend));
}

int main(void)
{

    // Initialize application specific hardware
    InitializeBoard();
    TickInit();
    InitAppConfig();
    StackInit();

    spi_init();
    init_ads();

    TCP_SOCKET sockfd = TCPOpen(0, TCP_OPEN_SERVER, 12000, TCP_PURPOSE_GENERIC_TCP_SERVER);

    while(1)
    {
        StackTask();
        routine(sockfd);
    }
}


/****************************************************************************
  Function:
    static void InitializeBoard(void)

  Description:
    This routine initializes the hardware.  It is a generic initialization
    routine for many of the Microchip development boards, using definitions
    in HardwareProfile.h to determine specific initialization.

  Precondition:
    None

  Parameters:
    None - None

  Returns:
    None

  Remarks:
    None
  ***************************************************************************/
static void InitializeBoard(void)
{    
    // LEDs
    LED0_TRIS = 0;
    LED1_TRIS = 0;
    LED2_TRIS = 0;
    LED3_TRIS = 0;
    LED4_TRIS = 0;
    LED5_TRIS = 0;
    LED6_TRIS = 0;
    LED7_TRIS = 0;
    LED_PUT(0x00);


    #if defined(__PIC32MX__)
    {
        // Enable multi-vectored interrupts
        INTEnableSystemMultiVectoredInt();
        
        // Enable optimal performance
        SYSTEMConfigPerformance(GetSystemClock());
        mOSCSetPBDIV(OSC_PB_DIV_1);                // Use 1:1 CPU Core:Peripheral clocks
        
        // Disable JTAG port so we get our I/O pins back, but first
        // wait 50ms so if you want to reprogram the part with 
        // JTAG, you'll still have a tiny window before JTAG goes away.
        // The PIC32 Starter Kit debuggers use JTAG and therefore must not 
        // disable JTAG.
        DelayMs(50);
        #if !defined(__MPLAB_DEBUGGER_PIC32MXSK) && !defined(__MPLAB_DEBUGGER_FS2)
            DDPCONbits.JTAGEN = 0;
        #endif
        LED_PUT(0x00);                // Turn the LEDs off
        
        CNPUESET = 0x00098000;        // Turn on weak pull ups on CN15, CN16, CN19 (RD5, RD7, RD13), which is connected to buttons on PIC32 Starter Kit boards
    }
    #endif

    // ADC
    AD1CON1 = 0x84E4;            // Turn on, auto sample start, auto-convert, 12 bit mode (on parts with a 12bit A/D)
    AD1CON2 = 0x0404;            // AVdd, AVss, int every 2 conversions, MUXA only, scan
    AD1CON3 = 0x1003;            // 16 Tad auto-sample, Tad = 3*Tcy
    #if defined(__32MX460F512L__) || defined(__32MX795F512L__)    // PIC32MX460F512L and PIC32MX795F512L PIMs has different pinout to accomodate USB module
        AD1CSSL = 1<<2;                // Scan pot
    #else
        AD1CSSL = 1<<5;                // Scan pot
    #endif
}

/*********************************************************************
 * Function:        void InitAppConfig(void)
 *
 * PreCondition:    MPFSInit() is already called.
 *
 * Input:           None
 *
 * Output:          Write/Read non-volatile config variables.
 *
 * Side Effects:    None
 *
 * Overview:        None
 *
 * Note:            None
 ********************************************************************/
// MAC Address Serialization using a MPLAB PM3 Programmer and 
// Serialized Quick Turn Programming (SQTP). 
// The advantage of using SQTP for programming the MAC Address is it
// allows you to auto-increment the MAC address without recompiling 
// the code for each unit.  To use SQTP, the MAC address must be fixed
// at a specific location in program memory.  Uncomment these two pragmas
// that locate the MAC address at 0x1FFF0.  Syntax below is for MPLAB C 
// Compiler for PIC18 MCUs. Syntax will vary for other compilers.
//#pragma romdata MACROM=0x1FFF0
static ROM BYTE SerializedMACAddress[6] = {MY_DEFAULT_MAC_BYTE1, MY_DEFAULT_MAC_BYTE2, MY_DEFAULT_MAC_BYTE3, MY_DEFAULT_MAC_BYTE4, MY_DEFAULT_MAC_BYTE5, MY_DEFAULT_MAC_BYTE6};
//#pragma romdata

static void InitAppConfig(void)
{
#if defined(EEPROM_CS_TRIS) || defined(SPIFLASH_CS_TRIS)
    unsigned char vNeedToSaveDefaults = 0;
#endif
    
    while(1)
    {
        // Start out zeroing all AppConfig bytes to ensure all fields are 
        // deterministic for checksum generation
        memset((void*)&AppConfig, 0x00, sizeof(AppConfig));
        
        AppConfig.Flags.bIsDHCPEnabled = TRUE;
        AppConfig.Flags.bInConfigMode = TRUE;
        memcpypgm2ram((void*)&AppConfig.MyMACAddr, (ROM void*)SerializedMACAddress, sizeof(AppConfig.MyMACAddr));
//        {
//            _prog_addressT MACAddressAddress;
//            MACAddressAddress.next = 0x157F8;
//            _memcpy_p2d24((char*)&AppConfig.MyMACAddr, MACAddressAddress, sizeof(AppConfig.MyMACAddr));
//        }
        AppConfig.MyIPAddr.Val = MY_DEFAULT_IP_ADDR_BYTE1 | MY_DEFAULT_IP_ADDR_BYTE2<<8ul | MY_DEFAULT_IP_ADDR_BYTE3<<16ul | MY_DEFAULT_IP_ADDR_BYTE4<<24ul;
        AppConfig.DefaultIPAddr.Val = AppConfig.MyIPAddr.Val;
        AppConfig.MyMask.Val = MY_DEFAULT_MASK_BYTE1 | MY_DEFAULT_MASK_BYTE2<<8ul | MY_DEFAULT_MASK_BYTE3<<16ul | MY_DEFAULT_MASK_BYTE4<<24ul;
        AppConfig.DefaultMask.Val = AppConfig.MyMask.Val;
        AppConfig.MyGateway.Val = MY_DEFAULT_GATE_BYTE1 | MY_DEFAULT_GATE_BYTE2<<8ul | MY_DEFAULT_GATE_BYTE3<<16ul | MY_DEFAULT_GATE_BYTE4<<24ul;
        AppConfig.PrimaryDNSServer.Val = MY_DEFAULT_PRIMARY_DNS_BYTE1 | MY_DEFAULT_PRIMARY_DNS_BYTE2<<8ul  | MY_DEFAULT_PRIMARY_DNS_BYTE3<<16ul  | MY_DEFAULT_PRIMARY_DNS_BYTE4<<24ul;
        AppConfig.SecondaryDNSServer.Val = MY_DEFAULT_SECONDARY_DNS_BYTE1 | MY_DEFAULT_SECONDARY_DNS_BYTE2<<8ul  | MY_DEFAULT_SECONDARY_DNS_BYTE3<<16ul  | MY_DEFAULT_SECONDARY_DNS_BYTE4<<24ul;
    
    
    
        // Compute the checksum of the AppConfig defaults as loaded from ROM
        wOriginalAppConfigChecksum = CalcIPChecksum((BYTE*)&AppConfig, sizeof(AppConfig));

        break;
    }
}
