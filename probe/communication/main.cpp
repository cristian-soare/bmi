#include <unistd.h>

#include <iostream>
#include <fstream>
#include <cstdio>

#include "TCPClient.h"
#include "CountBuff.h"
#include "CircularBuffer.h"
#include "Interpreter.h"

using namespace std;

const char ip[] = "192.168.0.3";
const int port = 12000;

void interpreterPerformance()
{
	// 100 kb as it has been fortold in Interpreter.cpp :: run comments
	CircularBuffer* cb = new CircularBuffer(100000, new ofstream("data.out", ios::binary));
	TCPClient* tc = new TCPClient(ip, port, cb);
	Interpreter* interp = new Interpreter(cb, tc);

	tc->start();
	interp->start();

	for (int i = 0; i < 10; i++) {
		printf("%d: %d / %d\n", i, cb->getCount(), cb->maxCap);
		usleep(SEC);
	}

	interp->stop();
	tc->stop();
	
	delete interp;
}

void requestTest()
{
	// 100 kb as it has been fortold in Interpreter.cpp :: run comments
	CircularBuffer* cb = new CircularBuffer(100000, new ofstream("data.out", ios::binary));
	TCPClient* tc = new TCPClient(ip, port, cb);
	Interpreter* interp = new Interpreter(cb, tc);

	tc->start();
	interp->start();

	for (int i = 0; i < 10; i++) {
		printf("incerc sa aflu %d => ", i);
		printf("%X\n", interp->readRegister(i));
		usleep(SEC);
	}

	interp->stop();
	tc->stop();
	
	delete interp;
}

void linkPerformance()
{
	// performance testing
	CountBuff* cb = new CountBuff();
	TCPClient* tc = new TCPClient(ip, port, cb);

	tc->start();
	cb->analyse(cout, 10);

	delete tc;
	delete cb;
}

int main()
{
	linkPerformance();
	// interpreterPerformance();
	// requestTest();
	return 0;
}
