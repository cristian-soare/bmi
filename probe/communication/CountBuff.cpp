#include "CountBuff.h"

#include <ostream>
#include <unistd.h>

using namespace std;

CountBuff::CountBuff()
: count(0) {}

bool CountBuff::add(byte* buff, int len)
{
	count += len;
	return true;
}

void CountBuff::analyse(ostream& out, int seconds)
{
	unsigned int old = 0;
	for (int i = 0; i < seconds; i++) {
		usleep(1000000);
		out << i << ": " << count - old << endl;
		old = count;
	}
}

int CountBuff::getCount()
{
	return count;
}
