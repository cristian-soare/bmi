#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <pthread.h>
#include "typedefs.h"

class Buffer;
class TCPClient;

class Interpreter
{
private:
	bool running;
	pthread_t thid;
	Buffer* buffer;
	TCPClient* client;
	int count;

	static const int dataLen = 5;
	static const int propLen = 4;

	static const byte SYNC = 0xA0;
	static const byte READ = 0xA1;
	static const byte WRITE = 0xA2;

	static bool isValidChecksum(const byte* b, int len);
	void sync();

public:
	Interpreter(Buffer* buffer, TCPClient* client);
	~Interpreter();

	void start(bool newStart = true);
	void stop();
	int getCount();
	byte readRegister(byte add);
	void writeRegister(byte add, byte val);
	static void* run(void* param);
};

#endif
