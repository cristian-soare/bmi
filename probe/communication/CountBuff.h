#ifndef COUNT_BUFF_H
#define COUNT_BUFF_H

#include "Buffer.h"

#include <iosfwd>

class CountBuff : public Buffer
{
private:
	unsigned int count;

public:
	CountBuff();

	bool add(byte* buff, int len);
	void analyse(std::ostream& out, int seconds);
	int getCount();
};

#endif