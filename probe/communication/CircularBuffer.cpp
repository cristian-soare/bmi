#include "CircularBuffer.h"

#include <cstring>
#include <fstream>

using namespace std;

#define SAFE(mutex, instruction) \
	pthread_mutex_lock(&mutex); \
	instruction; \
	pthread_mutex_unlock(&mutex)

CircularBuffer::CircularBuffer(int cap, ofstream* out)
: out(out), maxCap(cap)
{
	buffer = new byte[cap];
	left = right = 0;
	count = 0;
	pthread_mutex_init(&countMutex, NULL);
}

CircularBuffer::~CircularBuffer()
{
	delete[] buffer;
	if (out) out->close();
	delete out;
	pthread_mutex_destroy(&countMutex);
}

bool CircularBuffer::isEmpty()
{
	return count == 0;
}

bool CircularBuffer::isFull()
{
	return count == maxCap;
}

int CircularBuffer::getCount()
{
	return count;
}

bool CircularBuffer::add(byte* b, int len)
{
	if (len > maxCap - count)
		return false;
	SAFE(countMutex, count += len);

	int continous = maxCap - right;
	if (len <= continous) {
		continous = 0;
	} else {
		memcpy(buffer + right, b, continous);
		right = 0;
		len -= continous;
	}

	memcpy(buffer + right, b + continous, len);
	right = (right + len) % maxCap;
	return true;
}

bool CircularBuffer::align(byte header, int len, bool (*isValid)(const byte* b, int len))
{
	int passCount = 0;

	while (count - passCount >= len) {
		if (buffer[left] == header && (*isValid)(buffer + left, len)) {
			SAFE(countMutex, count -= passCount);
			return true;
		}

		passCount++;
		left = (left + 1) % maxCap;
	}

	SAFE(countMutex, count -= passCount);
	return false;
}

void CircularBuffer::advance(int len)
{
	SAFE(countMutex, count -= len);
	left = (left + len) % maxCap;
}

void CircularBuffer::write(int len)
{
	if (out == NULL) return;
	if (len > count) len = count;

	int continous = maxCap - left;
	if (len <= continous) {
		continous = 0;
	} else {
		out->write((char*)buffer + left, continous);
		len -= continous;
	}

	out->write((char*)buffer + (left + continous) % maxCap, len);
}
