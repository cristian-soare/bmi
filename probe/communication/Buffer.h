#ifndef GEN_BUFFER_H
#define GEN_BUFFER_H

#include "typedefs.h"

class Buffer
{
public:
	virtual ~Buffer() {}
	virtual bool add(byte* buff, int len) = 0;
	virtual int getCount() = 0;
	virtual void advance(int len) {}
	virtual bool align(byte header, int len, bool (*isValid)(const byte* b, int len)) { return false; }
	virtual byte operator[](int x) { return 0; }
	virtual const byte* getBuffer() { return 0; }
	virtual void write(int len) {}
};

#endif
