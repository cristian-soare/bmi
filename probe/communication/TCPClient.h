#ifndef TCP_CLIENT_H
#define	TCP_CLIENT_H

#include <arpa/inet.h> 

#include "typedefs.h"

class Buffer;

class TCPClient
{
private:
	Buffer* buff;
	int sockfd;
	sockaddr_in servaddr;
	
	pthread_t thid;
	bool running;
	static void* run(void* param);

public:
	TCPClient(const char* ip, const int port, Buffer* buff);
	~TCPClient();

	void start();
	void stop();
	void sendBuffer(byte* buff, int len);
};

#endif
