#include "TCPClient.h"
#include "Buffer.h"

#include <cstring>
#include <string>
#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>

using namespace std;

TCPClient::TCPClient(const char* ip, const int port, Buffer* buff)
{
	this->buff = buff;
	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(port);
	inet_aton(ip, &servaddr.sin_addr);
}

TCPClient::~TCPClient()
{
	
}

void TCPClient::start()
{
	if (running) return;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	connect(sockfd, (sockaddr*)&servaddr, sizeof(servaddr));
	running = true;
	pthread_create(&thid, NULL, run, (void*)this);
}

#include <iostream>

void* TCPClient::run(void* param)
{
	const int localbuflen = 10000;
	byte buf[localbuflen];
	int count;
	TCPClient* t = (TCPClient*)param;

	while (t->running) {
		count = recv(t->sockfd, buf, localbuflen, 0);
		if (count <= 0)
			std::cout << "fuck" << std::endl;
		t->buff->add(buf, count);
	}

	return NULL;
}

void TCPClient::stop()
{
	if (!running) return;
	running = false;
	pthread_join(thid, NULL);
	close(sockfd);
}

void TCPClient::sendBuffer(byte* buff, int len)
{
	send(sockfd, buff, len, 0);
}
