#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include "Buffer.h"

#include <iosfwd>
#include <pthread.h>

class CircularBuffer : public Buffer
{
private:
	byte* buffer;
	int left, right, frozenRight;
	int count;
	pthread_mutex_t countMutex;
	std::ofstream* out;

public:
	const int maxCap;

	CircularBuffer(int cap, std::ofstream* out = NULL);
	~CircularBuffer();

	bool isEmpty();
	bool isFull();
	int getCount();

	bool add(byte* b, int len);
	bool align(byte header, int len, bool (*isValid)(const byte* b, int len));
	void advance(int len);

	byte operator[](int x);
	void write(int len);
};

inline byte CircularBuffer::operator[](int x)
{
	return buffer[(left + x) % maxCap];
}

#endif