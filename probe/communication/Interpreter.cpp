#include "Interpreter.h"

#include "Buffer.h"
#include "TCPClient.h"

#include <unistd.h>
#include <fstream>

using namespace std;

Interpreter::Interpreter(Buffer* buffer, TCPClient* client)
: running(false), buffer(buffer), client(client), count(0) {}

Interpreter::~Interpreter() 
{
	delete client;
	delete buffer;
}

void Interpreter::start(bool newStart)
{
	if (running) return;
	if (newStart) readRegister(0);

	running = true;
	pthread_create(&thid, NULL, run, (void*)this);
}

void* Interpreter::run(void* param)
{
	Interpreter* t = (Interpreter*)param;
	int nb;

	while (t->running) {
		nb = t->buffer->getCount();
		nb -= nb % t->dataLen;

		t->buffer->write(nb);
		t->buffer->advance(nb);
		t->count += nb;

		// remember to set big enough circular buffer (4.7 mbps)
		// 1/100 s ~ 47 kb => 100 kb buffer
		usleep(SEC / 100);
	}

	return NULL;
}

bool Interpreter::isValidChecksum(const byte* buff, int len)
{
	byte b = 0;
	for (int i = 0; i < len; i++)
		b ^= buff[i];
	return b == 0;
}

void Interpreter::sync()
{
	while (buffer->align(SYNC, propLen, isValidChecksum) == false)
		usleep(SEC / 1000);
}

void Interpreter::stop()
{
	if (!running) return;
	running = false;
	pthread_join(thid, NULL);
}

byte Interpreter::readRegister(byte add)
{
	byte cmd[] = { READ, add, 0, 0 };
	bool wasRunning = running;

	stop();
	client->sendBuffer(cmd, propLen);
	sync();

	byte val = (*buffer)[2];
	buffer->advance(propLen);

	if (wasRunning) start(false);
	return val;
}

void Interpreter::writeRegister(byte add, byte val)
{
	byte cmd[] = { WRITE, add, val, 0 };
	client->sendBuffer(cmd, propLen);
}

int Interpreter::getCount()
{
	return count;
}
