#include "parsetree.h"

#include "parserexceptions.h"
#include "operator.h"

ParseTree::ParseTree(float val)
	: rootValue(new float(val))
	, variable(false)
	, oper(NULL)
	, left(NULL)
	, right(NULL) {}

ParseTree::ParseTree(float* val)
	: rootValue(val)
	, variable(true)
	, oper(NULL)
	, left(NULL)
	, right(NULL) {}

ParseTree::ParseTree(Operator* op, ParseTree* l, ParseTree* r)
	: rootValue(new float())
	, variable(false)
	, oper(op)
	, left(l)
	, right(r)
{
	int n = countOperands();
	if (op->isBinary() && n == 2) return;
	if (!op->isBinary() && n == 1) return;

	delete rootValue; delete oper;
	delete left; delete right;
	throw SyntacticEx(SyntacticEx::INV_OP);
}

ParseTree::~ParseTree()
{
	if (!variable) delete rootValue;
	delete oper;
	delete left;
	delete right;
}

float ParseTree::compute()
{
	if (oper) {
		float r = right->compute();
		if (left) {
			float l = left->compute();
			*rootValue = oper->calc(l, r);
		} else {
			*rootValue = oper->calc(r);
		}
	}
	return *rootValue;
}

int ParseTree::countOperands()
{
	int n = 0;
	if (left) n++;
	if (right) n++;
	return n;
}
