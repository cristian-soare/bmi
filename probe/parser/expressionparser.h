#ifndef EXPRESSIONPARSER_H
#define EXPRESSIONPARSER_H

#include "stringsplitter.h"
#include <stack>

class ParseTree;
class Operator;

struct Variable {
	std::string name;
	float* val;

	Variable(std::string& name, float* val)
		: name(name)
		, val(val) {}
};

struct ParsedExpr {
	ParseTree* pt;
	std::vector<Variable> vars;

	ParsedExpr();
	~ParsedExpr();
};

class ExpressionParser
{
public:
	ExpressionParser();
	ParsedExpr* eval(const std::string& expr);

private:
	int index, paranthesis;
	StringSplitter ss;
	std::vector<std::string> delim;
	std::vector<Variable> vars;

	void processOperator(std::stack<ParseTree*>& res, std::stack<Operator*>& opers);
	ParseTree* parse();
	bool isValidOper(std::string& s);
	bool isValidVar(std::string& s);
	float* varDeclared(std::string& s);

	template <typename T>
	void freeMem(std::stack<T*>& v);
};

#endif
