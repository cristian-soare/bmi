#include <fstream>
#include <iostream>
#include <algorithm>

#include "expressionparser.h"
#include "parsetree.h"
#include "parserexceptions.h"

using namespace std;

int main()
{
	ExpressionParser asd;
	string s;

	ifstream f("date.in");
	getline(f, s);
	f.close();

	ParsedExpr* pe = NULL;
	
	try {
		pe = asd.eval(s);
		// *pe->vars[0].val = 10;
		// cout << pe->vars.size() << endl;
		cout << pe->pt->compute() << endl;

		for (unsigned int i = 0; i < pe->vars.size(); i++)
			delete pe->vars[i].val;
		delete pe;
	} catch (ParserAbstractEx e) {
		cout << e.what() << endl;
	}

	return 0;
}
