#ifndef PARSEREXCEPTIONS_H
#define PARSEREXCEPTIONS_H

#include <exception>
#include <string>

class ParserAbstractEx : public std::exception
{
public:
	ParserAbstractEx(const std::string& msg) throw()
		: msg(msg) {}

	virtual const char* what() const throw()
	{
		return msg.c_str();
	}

private:
	const std::string& msg;
};

class EvaluatorEx : public ParserAbstractEx
{
public:
	static const std::string NEG_SQRT;
	static const std::string NEG_LOG;
	static const std::string ZERO_LOG;
	static const std::string DIV_ZERO;

	EvaluatorEx(const std::string& msg) throw()
		: ParserAbstractEx(msg) {}
};

class SyntacticEx : public ParserAbstractEx
{
public:
	static const std::string INV_OP;
	static const std::string INV_PAR;
	static const std::string INV_EXP;

	SyntacticEx(const std::string& msg) throw()
		: ParserAbstractEx(msg) {}
};

#endif
