#ifndef PARSETREE_H
#define PARSETREE_H

class Operator;

class ParseTree
{
public:
	ParseTree(float val);
	ParseTree(float* val);
	ParseTree(Operator* op, ParseTree* l, ParseTree* r);
	~ParseTree();

	float compute();

private:
	float* rootValue;
	bool variable;
	Operator* const oper;
	ParseTree* const left;
	ParseTree* const right;

	int countOperands();
};

#endif