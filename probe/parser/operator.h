#ifndef OPERATOR_H
#define OPERATOR_H

#include <string>
#include <iosfwd>

class Operator
{
public:
	static const int PLUS = 0;
	static const int MINUS_BINARY = 1;
	static const int PROD = 2;
	static const int DIV = 3;
	static const int MINUS_UNARY = 4;
	static const int POW = 5;
	static const int LOG = 6;
	static const int SQRT = 7;
	static const int SIN = 8;
	static const int COS = 9;

	static const std::string operators[];

	Operator(const std::string& tip);

	float calc(float a, float b);
	float calc(float a);
	bool operator<(const Operator& o);
	bool isBinary();
	void unaryMinus();
	int getType();
	std::ostream& operator<<(std::ostream& out);

private:
	int type;
};

#endif