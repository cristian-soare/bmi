#include "operator.h"

#include "parserexceptions.h"

#include <string>
#include <cmath>

using namespace std;

const std::string Operator::operators[] = { "+", "-", "*", "/", "-", "^", "log", "sqrt", "sin", "cos" };

Operator::Operator(const std::string& tip)
{
	if (tip == "+") type = PLUS;
	else if (tip == "-") type = MINUS_BINARY;
	else if (tip == "*") type = PROD;
	else if (tip == "/") type = DIV;
	else if (tip == "-") type = MINUS_UNARY;
	else if (tip == "^") type = POW;
	else if (tip == "log") type = LOG;
	else if (tip == "sqrt") type = SQRT;
	else if (tip == "sin") type = SIN;
	else if (tip == "cos") type = COS;
	else throw SyntacticEx(SyntacticEx::INV_OP);
}

float Operator::calc(float a, float b)
{
	switch (type) {
		case PLUS: return a + b;
		case MINUS_BINARY: return a - b;
		case PROD: return a * b;
		case DIV:
			if (b == 0) 
				throw EvaluatorEx(EvaluatorEx::DIV_ZERO);
			return a / b;
		case POW: return pow(a, b);
		default: return 0;
	}
}

float Operator::calc(float a)
{
	switch (type) {
		case MINUS_UNARY: return -a;
		case LOG:
			if (a < 0) throw EvaluatorEx(EvaluatorEx::NEG_LOG);
			if (a == 0) throw EvaluatorEx(EvaluatorEx::ZERO_LOG);
			return log10(a);
		
		case SQRT:
			if (a < 0) EvaluatorEx(EvaluatorEx::NEG_SQRT);
			return sqrt(a);

		case SIN: return sin(a);
		case COS: return cos(a);
		default: return 0;
	}
}

bool Operator::operator<(const Operator& o)
{
	return type < o.type || (type == POW && o.type == POW);
}

bool Operator::isBinary()
{
	return type <= 3 || type == 5;
}

void Operator::unaryMinus()
{
	if (type == MINUS_BINARY)
		type = MINUS_UNARY;
}

int Operator::getType()
{
	return type;
}

ostream& Operator::operator<<(ostream& out)
{
	out << operators[type];
	return out;
}
