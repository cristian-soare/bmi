#include "parserexceptions.h"

using namespace std;

const string EvaluatorEx::NEG_SQRT = "negative value passed to square root";
const string EvaluatorEx::NEG_LOG = "negative value passed to logarithm";
const string EvaluatorEx::ZERO_LOG = "expression under logarithm evaluates to zero";
const string EvaluatorEx::DIV_ZERO = "division by zero";

const string SyntacticEx::INV_OP = "invalid operator or operand";
const string SyntacticEx::INV_PAR = "invalid parantheses";
const string SyntacticEx::INV_EXP = "invalid expression";
