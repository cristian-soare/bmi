#include "expressionparser.h"

#include "parserexceptions.h"
#include "operator.h"
#include "parsetree.h"

#include <cstdlib>
#include <cctype>

using namespace std;

ParsedExpr::ParsedExpr()
	: pt(NULL) {}

ParsedExpr::~ParsedExpr()
{
	delete pt;
}

ExpressionParser::ExpressionParser()
{
	for (int i = 0; i < 10; i++)
		delim.push_back(Operator::operators[i]);
	delim.push_back("("); delim.push_back(")");
}

ParsedExpr* ExpressionParser::eval(const std::string& expr)
{
	index = paranthesis = 0;

	ss.setDelim(delim);
	ss.setString(expr);	

	ParsedExpr* pe = new ParsedExpr();

	try {
		pe->pt = parse();
		pe->vars = vars;

		if (paranthesis)
			throw SyntacticEx(SyntacticEx::INV_PAR);
	} catch (ParserAbstractEx e) {
		delete pe;
		for (unsigned int i = 0; i < vars.size(); i++) 
			delete vars[i].val;
		throw e;
	}

	return pe;
}

void ExpressionParser::processOperator(stack<ParseTree*>& res, stack<Operator*>& opers)
{
	if (res.empty() || opers.empty() || (opers.top()->isBinary() && res.size() == 1)) {
		freeMem<Operator>(opers); freeMem<ParseTree>(res);
		throw SyntacticEx(SyntacticEx::INV_EXP);
	}

	Operator* o = opers.top(); opers.pop();
	ParseTree* r = res.top(); res.pop();
	ParseTree* l = NULL;

	if (o->isBinary()) {
		l = res.top();
		res.pop();
	}

	try {
		res.push(new ParseTree(o, l, r));
	} catch (ParserAbstractEx e) {
		freeMem<Operator>(opers); freeMem<ParseTree>(res);
		throw e;
	}
}

ParseTree* ExpressionParser::parse()
{
	vars.clear();
	stack<Operator*> opers;
	stack<ParseTree*> results;

	try {
		string s;
		while (ss.read(s)) {
			if (s == "(") {
				++index;
				++paranthesis;
				results.push(parse());
	 			continue;
			}

			if (s == ")") {
				--paranthesis;
				if (paranthesis < 0)
					throw SyntacticEx(SyntacticEx::INV_PAR);
				break;
			}

			float x = atof(s.c_str());
			if (x) {
				results.push(new ParseTree(x));
			} else if (isValidOper(s)) {
				Operator* op = new Operator(s);

				if (opers.empty() && results.empty())
					op->unaryMinus();

				while (true) {
					if (opers.empty() || *opers.top() < *op) {
						opers.push(op);
						break;
					}
					processOperator(results, opers);
				}
			} else if (isValidVar(s)) {
				float* foundVar = varDeclared(s);

				if (!foundVar) {
					vars.push_back(Variable(s, new float()));
					foundVar = vars.back().val;
				}

				results.push(new ParseTree(foundVar));
			} else {
				throw SyntacticEx(SyntacticEx::INV_EXP);
			}
		}

		while (!opers.empty())
			processOperator(results, opers);

		if (results.size() != 1)
			throw SyntacticEx(SyntacticEx::INV_EXP);
	} catch (ParserAbstractEx e) {
		freeMem<Operator>(opers); freeMem<ParseTree>(results);
		throw e;
	}

	return results.top();
}

bool ExpressionParser::isValidOper(string& s)
{
	for (unsigned int i = 0; i < delim.size(); i++)
		if (delim[i] == s)
			return true;
	return false;
}

bool ExpressionParser::isValidVar(string& s)
{
	for (unsigned int i = 0; i < s.size(); i++)
		if (!isdigit(s[i]) && !isalpha(s[i]) && s[i] != '_')
			return false;
	return true;
}

float* ExpressionParser::varDeclared(string& s)
{
	for (unsigned int i = 0; i < vars.size(); i++)
		if (s == vars[i].name)
			return vars[i].val;
	return NULL;
}

template <typename T>
void ExpressionParser::freeMem(stack<T*>& v)
{
	while (!v.empty()) {
		delete v.top();
		v.pop();
	}
}
