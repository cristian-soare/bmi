function gen(len, freqs)
	y = zeros(1, len);
	for i = 1 : length(freqs)
		y += genf(len, freqs(i))(1 : len);
	end
	plot(y);

	fid = fopen('data.txt', 'wt');
	fprintf(fid, '%d\n', len);
	fprintf(fid, '%f\n', y);
	fclose(fid);
end

function y = genf(len, freq)
	if len < freq * 2
		y = zeros(1, len);
		return;
	end

	rev = 2*pi * freq;
	x = 0:(2*pi / (len / freq)):rev;
	y = sin(x);
end
