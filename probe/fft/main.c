#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define SWAP(a, b) cop = (a); (a) = (b); (b) = cop
#define POW2(a) ((a) * (a))

void bit_reversal(float* data, int nn)
{
	int i, j, cop;
	int n, m;

	n = nn * 2;
	j = 0;

    for (i = 0; i < nn; i += 2) {
        if (j > i) {
            //swap the real part
            SWAP(data[j], data[i]);
            //swap the complex part
            SWAP(data[j + 1], data[i + 1]);
            // checks if the changes occurs in the first half
            // and use the mirrored effect on the second half
            if (j / 2 < n / 4) {
                //swap the real part
                SWAP(data[n - (i + 2)], data[n - (j + 2)]);
                //swap the complex part
                SWAP(data[n - (i + 2) + 1], data[n - (j + 2) + 1]);
            }
        }
        m = n / 2;
        while (m >= 2 && j >= m) {
            j -= m;
            m /= 2;
        }
        j += m;
    }
}

void fft(float* data, int nn)
{
	int n, m, mmax, istep, i, j;
	double wtemp, wr, wpr, wpi, wi, theta;
	float tempr, tempi;

	n = nn * 2;
	bit_reversal(data, nn);

	mmax = 2;
	while (n > mmax) {
		istep = mmax * 2;
		theta = 2 * M_PI / mmax;
		wtemp = sin(0.5 * theta);
		
		wpr = -2.0 * wtemp * wtemp;
		wpi = sin(theta);
		
		wr = 1.0;
		wi = 0.0;

		for (m = 1; m < mmax; m += 2) {

			// #pragma omp parallel for private(i, j, tempr, tempi)
			for (i = m; i <= n; i += istep) {
				j = i + mmax;
				tempr = wr * data[j - 1] - wi * data[j];
                tempi = wr * data[j] + wi * data[j - 1];
                data[j - 1] = data[i - 1] - tempr;
                data[j] = data[i] - tempi;
                data[i - 1] += tempr;
                data[i] += tempi;
			}

			wr = (wtemp = wr) * wpr - wi * wpi + wr;
            wi = wi * wpr + wtemp * wpi + wi;
		}
		mmax = istep;
	}
}

int main(void)
{
	int len, i;

	FILE* f = fopen("data.txt", "r");
	fscanf(f, "%d", &len);
	float* data = calloc(len * 2, sizeof(float));
	for (i = 0; i < len * 2; i += 2)
		fscanf(f, "%f", &data[i]);
	fclose(f); f = NULL;
	
	fft(data, len);

	f = fopen("fftout.txt", "w");
	fprintf(f, "%d\n", len / 2);
	for (i = 0; i < len; i += 2)
		fprintf(f, "%f\n", sqrt(POW2(data[i]) + POW2(data[i + 1])));
	fclose(f);

	free(data);
	return 0;
}
