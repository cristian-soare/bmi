function ret = getsignal(filename)

	[A, M, P] = imread(filename);
	len = length(A(1,:));

	ret = [];
	for i = 1 : len
		ret = [ret; find(A(:,i) == 0)(1)];
	end

	[pth, name, ext] = fileparts(filename);
	dataname = strcat(name, '.in');
	fid = fopen(dataname, 'w');
	fprintf(fid, '%g ', ret);
	fclose(fid);

end