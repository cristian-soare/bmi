#ifndef GUIS_H
#define GUIS_H

#include <QDialog>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <tuple>

namespace Ui
{
class GraphSettingsGui;
}

class FilterGraphPlugin;

///////////////////// GraphSettingsGui //////////////////////////
/////////////////////////////////////////////////////////////////

class GraphSettingsGui : public QDialog
{
	Q_OBJECT

public:
	GraphSettingsGui(QWidget* parent);
	~GraphSettingsGui();

	void exec(FilterGraphPlugin* fgp);
	std::string filterType(int idx);
	int filterType(const std::string& s);

private slots:
	void accept();
	void on_filterType_currentIndexChanged(int index);

private:
	Ui::GraphSettingsGui* ui;
	FilterGraphPlugin* fgp;
};

#endif
