#include "guis.h"
#include "ui_graph.h"

#include "filterplugin.h"

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpinBox>
#include <QSpacerItem>
#include <QLabel>
#include <string>

using namespace std;

//////////////////// GraphSettingsGui //////////////////////////////////
////////////////////////////////////////////////////////////////////////

GraphSettingsGui::GraphSettingsGui(QWidget* parent)
	: QDialog(parent)
	, ui(new Ui::GraphSettingsGui)
{
	ui->setupUi(this);

	ui->filterType->addItems(QStringList({"Band pass", "Band stop", "Low pass", "High pass"}));
	connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
	connect(ui->filterType, SIGNAL(currentIndexChanged(int)), this, SLOT(on_filterType_currentIndexChanged(int)));
}

GraphSettingsGui::~GraphSettingsGui()
{

}

string GraphSettingsGui::filterType(int idx)
{
	return ui->filterType->itemText(idx).toStdString();
}

int GraphSettingsGui::filterType(const string& s)
{
	return ui->filterType->findText(QString::fromStdString(s));
}

void GraphSettingsGui::exec(FilterGraphPlugin* fgp)
{
	this->fgp = fgp;
	ui->samples->setValue(fgp->sampleCount);
	ui->order->setValue(fgp->order);
	ui->sps->setValue(fgp->sps);
	ui->useSPS->setChecked(fgp->useSPS);
	ui->centerFreq->setValue(fgp->centerFreq);
	ui->bwidth->setValue(fgp->bwidth);
	ui->filterType->setCurrentIndex(fgp->ftype);
	QDialog::exec();
}

void GraphSettingsGui::accept()
{
	fgp->sampleCount = ui->samples->value();
	fgp->order = ui->order->value();
	fgp->sps = ui->useSPS->isChecked() ? ui->sps->value() : 0;
	fgp->useSPS = ui->useSPS->isChecked();
	fgp->centerFreq = ui->centerFreq->value();
	fgp->bwidth = ui->bwidth->value();
	fgp->ftype = ui->filterType->currentIndex();
	QDialog::accept();
}

void GraphSettingsGui::on_filterType_currentIndexChanged(int index)
{
	ui->bwidth->setEnabled(index < 2);
	ui->cfreq->setText(index < 2 ? "Center freq:" : "Cutoff freq");
}
