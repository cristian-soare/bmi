QT += core gui widgets printsupport

TARGET = filter
TEMPLATE = lib
CONFIG += debug

INCLUDEPATH += ../commons

LIBS += -lDSPFilters
SOURCES += filterplugin.cpp guis.cpp
HEADERS += guis.h filterplugin.h ../commons/generalbuffer.h ../commons/signalbuffer.h
FORMS += graph.ui

QMAKE_CXXFLAGS += -std=c++14
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast
