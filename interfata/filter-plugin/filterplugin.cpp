#include "filterplugin.h"

#include <QMessageBox>
#include <QHBoxLayout>
#include <QLabel>

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <cstring>

#include <QDialog>
#include <QWidget>

using namespace std;

void IFilter::setupFreq(int order, double centFreq, double freqBand)
{
	iorder = order;
	icentFreq = centFreq;
	ifreqBand = freqBand;
	setup();
}

void IFilter::setupSPS(int sps)
{
	isps = sps;
	setup();
}

void FBandPass::setup()
{
	Dsp::SimpleFilter<Dsp::Butterworth::BandPass<12>, 1>::setup(iorder, isps, icentFreq, ifreqBand);
}

void FBandPass::process(int sampleCount, double* const* v)
{
	Dsp::SimpleFilter<Dsp::Butterworth::BandPass<12>, 1>::process(sampleCount, v);
}

void FBandStop::setup()
{
	Dsp::SimpleFilter<Dsp::Butterworth::BandStop<12>, 1>::setup(iorder, isps, icentFreq, ifreqBand);
}

void FBandStop::process(int sampleCount, double* const* v)
{
	Dsp::SimpleFilter<Dsp::Butterworth::BandStop<12>, 1>::process(sampleCount, v);
}

void FLowPass::setup()
{
	Dsp::SimpleFilter<Dsp::Butterworth::LowPass<12>, 1>::setup(iorder, isps, icentFreq);
}

void FLowPass::process(int sampleCount, double* const* v)
{
	Dsp::SimpleFilter<Dsp::Butterworth::LowPass<12>, 1>::process(sampleCount, v);
}

void FHighPass::setup()
{
	Dsp::SimpleFilter<Dsp::Butterworth::HighPass<12>, 1>::setup(iorder, isps, icentFreq);
}

void FHighPass::process(int sampleCount, double* const* v)
{
	Dsp::SimpleFilter<Dsp::Butterworth::HighPass<12>, 1>::process(sampleCount, v);
}

//////////////////////////////// FilterPlugin ///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

FilterPlugin::FilterPlugin(QWidget* p)
	: IPlugin(p)
	, graphGui(p)
{
	layout = new QHBoxLayout(p);
	setLayout(layout);
	layout->setAlignment(Qt::AlignCenter);
	layout->addWidget(new QLabel("BMI Filter Plugin\n(C)2015 Cristian Soare"));
	layout->setSizeConstraint(QLayout::SetFixedSize);
}

FilterPlugin::~FilterPlugin()
{
	// delete layout;
	// delete label;
}

static FilterPlugin* globalPlugin;

extern "C" IPlugin* initPlugin(QWidget* parent)
{
	if (!globalPlugin)
		globalPlugin = new FilterPlugin(parent);
	return globalPlugin;
}

extern "C" void destroyPlugin()
{
	delete globalPlugin;
	globalPlugin = NULL;
}

QString FilterPlugin::getName()
{
	return QString("Filter plugin v 0.1");
}

IPluginGraph* FilterPlugin::initGraphPlugin()
{
	plugins.push_back(new FilterGraphPlugin(this));
	return plugins.back();
}

////////////////////////////// Graph plugin ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

FilterGraphPlugin::FilterGraphPlugin(FilterPlugin* plot)
	: parent(plot)
{
	filled = sps = 0;
	intime = new double[512];
	inval = new double[512];
}

FilterGraphPlugin::~FilterGraphPlugin()
{
	delete[] intime;
	delete[] inval;
	delete f;
}

void FilterGraphPlugin::show()
{
	parent->graphGui.exec(this);
	update();
}

pair<vector<pair<string, SignalBuffer::Domain>>, vector<pair<string, SignalBuffer::Domain>>> FilterGraphPlugin::getIOProp()
{
	vector<pair<string, SignalBuffer::Domain>> ins{make_pair("input", SignalBuffer::Time)};
	vector<pair<string, SignalBuffer::Domain>> outs{make_pair("output", SignalBuffer::Time)};
	return make_pair(ins, outs);
}

SignalBuffer*& FilterGraphPlugin::operator[](const std::string& s)
{
	return s == "input" ? input : output;
}

static IFilter* newFilter(int type)
{
	switch (type) {
	case 0: return new FBandPass();
	case 1: return new FBandStop();
	case 2: return new FLowPass();
	case 3: return new FHighPass();
	default: return nullptr;
	}
}

void FilterGraphPlugin::update()
{
	delete intime; intime = new double[sampleCount];
	delete inval; inval = new double[sampleCount];

	delete f;
	f = newFilter(ftype);
	f->setupFreq(order, centerFreq, bwidth);
	if (sps)
		f->setupSPS(sps);
}

void FilterGraphPlugin::reset()
{
	filled = 0;
	if (!useSPS)
		sps = 0;
}

void FilterGraphPlugin::run()
{
	int i = 0, size = input->getSize();
	SignalBuffer& v = *input;

	while (i < size) {
		for (; i < size && filled < sampleCount; i++, filled++) {
			intime[filled] = v[i].first;
			inval[filled] = v[i].second;
		}

		if (filled == sampleCount) {
			if (!sps) {
				if (!useSPS) {
					double sum = 0;
					for (int j = 1; j < filled; j++)
						sum += intime[j] - intime[j - 1];
					sps = 1. / (sum / (filled - 1));
				}

				f->setupSPS(sps);
			}

			f->process(sampleCount, &inval);

			for (int j = 0; j < sampleCount; j++)
				output->add(make_pair(intime[j], inval[j]));
			filled = 0;
		}
	}
}

void FilterGraphPlugin::save(pugi::xml_node node)
{
	node.append_attribute("filterType") = parent->graphGui.filterType(ftype).c_str();
	node.append_attribute("samples") = sampleCount;
	node.append_attribute("order") = order;
	node.append_attribute("sps") = sps;
	node.append_attribute("useSPS") = useSPS;
	node.append_attribute("centerFreq") = centerFreq;
	node.append_attribute("bwidth") = bwidth;
}

void FilterGraphPlugin::load(pugi::xml_node node)
{
	ftype = parent->graphGui.filterType(node.attribute("filterType").value());
	sampleCount = node.attribute("samples").as_int();
	order = node.attribute("order").as_int();
	sps = node.attribute("sps").as_int();
	useSPS = node.attribute("useSPS").as_bool();
	centerFreq = node.attribute("centerFreq").as_double();
	bwidth = node.attribute("bwidth").as_double();
	update();
}
