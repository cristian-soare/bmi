#ifndef FilterPLUGIN_H
#define FilterPLUGIN_H

#include "classinterfaces.h"
#include "guis.h"

#include <DspFilters/Butterworth.h>

class QDialog;
class QWidget;
class QLabel;
class QHBoxLayout;
template <typename T> class CircularBuffer;

class FilterGraphPlugin;
class FilterPlugin;

class IFilter
{
public:
	virtual ~IFilter() {}

	void setupFreq(int order, double icentFreq, double ifreqBand);
	void setupSPS(int sps);
	virtual void process(int sampleCount, double* const* v) = 0;

protected:
	virtual void setup() = 0;

	int iorder, isps = 1;
	double icentFreq, ifreqBand;
};

class FBandPass : public Dsp::SimpleFilter<Dsp::Butterworth::BandPass<12>, 1>, public IFilter
{
public:
	void process(int sampleCount, double* const* v);

private:
	void setup();
};

class FBandStop : public Dsp::SimpleFilter<Dsp::Butterworth::BandStop<12>, 1>, public IFilter
{
public:
	void process(int sampleCount, double* const* v);

private:
	void setup();
};

class FLowPass : public Dsp::SimpleFilter<Dsp::Butterworth::LowPass<12>, 1>, public IFilter
{
public:
	void process(int sampleCount, double* const* v);

private:
	void setup();
};

class FHighPass : public Dsp::SimpleFilter<Dsp::Butterworth::HighPass<12>, 1>, public IFilter
{
public:
	void process(int sampleCount, double* const* v);

private:
	void setup();
};

////////////////////// FilterGraphPlugin ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

class FilterGraphPlugin : public IPluginGraph
{
	friend class GraphSettingsGui;

public:
	FilterGraphPlugin(FilterPlugin* parent);
	~FilterGraphPlugin();

	void show();
	void reset();

    std::pair<
        std::vector<std::pair<std::string, SignalBuffer::Domain>>,
        std::vector<std::pair<std::string, SignalBuffer::Domain>>> getIOProp();
    SignalBuffer*& operator[](const std::string& s);
	void run();

	void save(pugi::xml_node node);
	void load(pugi::xml_node node);

private:
	void update();

	IFilter* f = nullptr;

	SignalBuffer *input, *output;
	double *intime, *inval;

	int filled, order, sps, sampleCount, ftype;
	bool useSPS;
	double centerFreq, bwidth;
	FilterPlugin* parent;
};

/////////////////// FilterPlugin ////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

class FilterPlugin : public IPlugin
{
public:
	FilterPlugin(QWidget* p);
	~FilterPlugin();

	QString getName();
	IPluginGraph* initGraphPlugin();

	GraphSettingsGui graphGui;

private:
	QHBoxLayout* layout;
	std::vector<FilterGraphPlugin*> plugins;
};

#endif
