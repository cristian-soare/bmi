#include "guis.h"
#include "ui_config.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QLabel>

#include "edfplugin.h"

using namespace std;

ConfigGui::ConfigGui(QWidget* widget, EDFPlugin* ainput)
	: QDialog(widget)
	, ui(new Ui::ConfigGui)
	, ainput(ainput)
{
	ui->setupUi(this);

	connect(ui->button, SIGNAL(clicked()), this, SLOT(browse()));
	connect(ui->okButton, SIGNAL(clicked()), this, SLOT(okClick()));
}

ConfigGui::~ConfigGui()
{

}

void ConfigGui::browse()
{
	ui->path->setText(QFileDialog::getOpenFileName(
		this, "File path", ui->path->text(), "*.ogg *.wav"));
	ainput->loadFile(ui->path->text().toStdString());
}

void ConfigGui::okClick()
{
	close();
}

void ConfigGui::setInfo(const string& info)
{
	ui->details->setText(QString::fromStdString(info));
}
