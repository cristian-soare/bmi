QT += core gui widgets printsupport

TARGET = edfplugin
TEMPLATE = lib
CONFIG += debug

LIBS += -lpugixml
SOURCES += guis.cpp edflib.c \
    edfplugin.cpp
HEADERS += guis.h edflib.h \
    edfplugin.h
FORMS += config.ui

QMAKE_CXXFLAGS += -std=c++14
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast
