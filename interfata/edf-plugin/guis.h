#ifndef GUIS_H
#define GUIS_H

#include <QDialog>
#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QSlider>
#include <string>

class EDFPlugin;

namespace Ui
{
class ConfigGui;
}

class ConfigGui : public QDialog
{
	Q_OBJECT

public:
	ConfigGui(QWidget* parent, EDFPlugin* ainput);
	~ConfigGui();

	void setInfo(const std::string& info);

private slots:
	void browse();
	void okClick();

private:
	Ui::ConfigGui* ui;

	EDFPlugin* ainput;
};

#endif
