#ifndef AUDIO_H
#define AUDIO_H

#include <vector>
#include <string>
#include <thread>
#include <mutex>
#include <atomic>
#include <pugixml.hpp>
#include <edflib.h>

#include "guis.h"

struct sample
{
    int channel;
    double x, y;
};

class DataManagerInterface
{
public:
    virtual void addData(sample* smp, int len) = 0;
    virtual void notifyStop() = 0;

protected:
	virtual ~DataManagerInterface() {}
};

class InputPluginInterface
{
public:
	virtual bool start() = 0;
	virtual void stop() = 0;

    virtual void setDataManager(DataManagerInterface* dmanager) = 0;
    virtual void setParents(QWidget* settings, QWidget* control) { (void)settings; (void)control; }

    virtual void showSettings() {}
    virtual void showConfig() {}

    virtual unsigned int getByteCount() { return 0; }
    virtual float getUsage() { return 0; }
    virtual const std::vector<std::string>& getRegNames() = 0;

    virtual void save(pugi::xml_node node) { (void)node; }
    virtual void load(pugi::xml_node node) { (void)node; }

protected:
	virtual ~InputPluginInterface() {}
};

class EDFPlugin : public InputPluginInterface
{
public:
	EDFPlugin();
	~EDFPlugin();

	bool start();
	void stop();

	void loadFile(const std::string& path);

	void setDataManager(DataManagerInterface* dmanager);
	void setParents(QWidget* settings, QWidget* control);

	void showConfig();

	const std::vector<std::string>& getRegNames();

	void save(pugi::xml_node node);
	void load(pugi::xml_node node);

private:
	void run();

	std::thread thr;
	std::atomic<bool> ongoing;
	unsigned int progress;

	std::vector<std::string> regNames;
	DataManagerInterface* dmanager;
	ConfigGui gui;

	std::string path;
	edf_hdr_struct edfhdr;
};

#endif
