#include "edfplugin.h"
#include "ui_config.h"

#include <iostream>
#include <chrono>

using namespace std;

static EDFPlugin* globalPlugin;

extern "C" InputPluginInterface* initPlugin()
{
	if (!globalPlugin)
		globalPlugin = new EDFPlugin();
	return globalPlugin;
}

extern "C" void destroyPlugin()
{
	delete globalPlugin;
	globalPlugin = 0;
}

EDFPlugin::EDFPlugin()
	: gui(NULL, this)
{
	ongoing = false;
	edfhdr.handle = 0;

	regNames = { "Fc5", "Fc3", "Fc1", "Fcz", "Fc2", "Fc4", "Fc6", "C5", "C3", "C1",
						  "Cz", "C2", "C4", "C6", "Cp5", "Cp3", "Cp1", "Cpz", "Cp2", "Cp4",
						  "Cp6", "Fp1", "Fpz", "Fp2", "Af7", "Af3", "Afz", "Af4", "Af8", "F7",
						  "F5", "F3", "F1", "Fz", "F2", "F4", "F6", "F8", "Ft7", "Ft8", "T7",
						  "T8", "T9", "T10", "Tp7", "Tp8", "P7", "P5", "P3", "P1", "Pz", "P2",
						  "P4", "P6", "P8", "Po7", "Po3", "Poz", "Po4", "Po8", "O1", "Oz", "O2", "Iz", };
}

EDFPlugin::~EDFPlugin()
{
	if (thr.joinable())
		thr.join();
}

bool EDFPlugin::start()
{
	if (ongoing)
		return true;
	if (thr.joinable())
		thr.join();

	ongoing = true;
	thr = std::thread(&EDFPlugin::run, this);
	return true;
}

void EDFPlugin::run()
{
	int nsamp = 16; // edfhdr.signalparam[0].smp_in_file / (edfhdr.file_duration / 1000000);
	sample smp[44100];
	progress = 0;

	cout << "length " << edfhdr.file_duration << endl;

	for (int channel = 0; channel < edfhdr.edfsignals; channel++)
		edfrewind(edfhdr.handle, channel);

	while (ongoing) {
		unsigned int oldProgress = progress;
		for (int channel = 0; channel < edfhdr.edfsignals; channel++) {
			double buff[200];
			edfread_physical_samples(edfhdr.handle, channel, nsamp, buff);

			progress = oldProgress;
			for (int count = 0; count < nsamp; count++, progress++) {
				int ccount = channel * nsamp + count;

				smp[ccount].channel = channel;
				smp[ccount].x = (double)progress / 160;
				smp[ccount].y = buff[count];
			}
		}

		dmanager->addData(smp, nsamp * edfhdr.edfsignals);
		this_thread::sleep_for(chrono::milliseconds(100));
	}
}

void EDFPlugin::stop()
{
	if (!ongoing)
		return;

	ongoing = false;
	thr.join();
}

void EDFPlugin::setDataManager(DataManagerInterface* dmanager)
{
	this->dmanager = dmanager;
}

void EDFPlugin::setParents(QWidget* settings, QWidget* control)
{
	(void)settings;
	gui.setParent(control); gui.setWindowFlags(Qt::Window);
}

void EDFPlugin::showConfig()
{
	gui.show();
}

const vector<string>& EDFPlugin::getRegNames()
{
	return regNames;
}

void EDFPlugin::save(pugi::xml_node node)
{
	node.append_attribute("edfPath") = path.c_str();
}

void EDFPlugin::load(pugi::xml_node node)
{
	loadFile(node.attribute("edfPath").value());
}

void EDFPlugin::loadFile(const string& path)
{
	this->path = path;
	if (edfhdr.handle)
		edfclose_file(edfhdr.handle);
	edfopen_file_readonly(path.c_str(), &edfhdr, EDFLIB_READ_ALL_ANNOTATIONS);

	string info = "Loaded file\n" + to_string(edfhdr.edfsignals) + "channels\n";
	gui.setInfo(info);
}
