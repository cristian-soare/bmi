QT += core gui widgets printsupport

TARGET = audio
TEMPLATE = lib
CONFIG += debug

LIBS += -lpugixml -lsfml-audio
SOURCES += audio.cpp guis.cpp
HEADERS += guis.h
FORMS += config.ui

QMAKE_CXXFLAGS += -std=c++14
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast
