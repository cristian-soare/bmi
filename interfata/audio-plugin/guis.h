#ifndef GUIS_H
#define GUIS_H

#include <QDialog>
#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QSlider>
#include <string>

class AudioInput;

namespace Ui
{
class ConfigGui;
}

class ConfigGui : public QDialog
{
	Q_OBJECT

public:
	ConfigGui(QWidget* parent, AudioInput* ainput);
	~ConfigGui();

	void setSliderMaximum(int val);
	void slide(int val);

private slots:
	void browse();
	void okClick();

	void onSliderPressed();
	void onSliderChanged(int val);

private:
	Ui::ConfigGui* ui;

	AudioInput* ainput;
	bool pressed;
};

#endif
