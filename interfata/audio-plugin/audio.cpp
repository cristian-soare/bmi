#include "audio.h"
#include "ui_config.h"

#include <iostream>
#include <chrono>

using namespace std;

static AudioInput* globalPlugin;

extern "C" InputPluginInterface* initPlugin()
{
	if (!globalPlugin)
		globalPlugin = new AudioInput();
	return globalPlugin;
}

extern "C" void destroyPlugin()
{
	delete globalPlugin;
	globalPlugin = 0;
}

AudioInput::AudioInput()
{
	regNames.push_back("left");
	regNames.push_back("right");
	gui = new ConfigGui(NULL, this);
	ongoing = false;
}

AudioInput::~AudioInput()
{
	if (thr.joinable())
		thr.join();
	delete gui;
}

bool AudioInput::start()
{
	sound.setBuffer(sbuffer);
	sound.setLoop(false);

	if (ongoing)
		return true;
	if (thr.joinable())
		thr.join();

	ongoing = true;
	thr = std::thread(&AudioInput::run, this);
	return true;
}

void AudioInput::setOffset(unsigned int sec)
{
	lock_guard<mutex> guard(mut);
	oldProgress = progress = sec * sbuffer.getSampleRate();
	sound.setPlayingOffset(sf::seconds(sec));
}

void AudioInput::run()
{
	double nsamp = sbuffer.getSampleRate() / 1000.;

	oldProgress = progress = 0;
	sample smp[44100];

	sound.play();
	while (ongoing) {
		lock_guard<mutex> guard(mut);
		unsigned int limidx = sound.getPlayingOffset().asMilliseconds() * nsamp;

		if (sound.getStatus() == sf::SoundSource::Stopped) {
			ongoing = false;
			// dmanager->notifyStop();
			// notStop();
			break;
		}

		unsigned int count;
		for (count = 0; progress < limidx; progress++, count++)
			for (unsigned int channel = 0; channel < sbuffer.getChannelCount(); channel++) {
				int ccount = count * sbuffer.getChannelCount() + channel;

				smp[ccount].channel = channel;
				smp[ccount].x = (double)progress / sbuffer.getSampleRate();
				smp[ccount].y = sbuffer.getSamples()[progress * sbuffer.getChannelCount() + channel];
			}
		dmanager->addData(smp, count * sbuffer.getChannelCount());

		if (progress - oldProgress > sbuffer.getSampleRate()) {
			oldProgress = progress;
			gui->slide(progress / sbuffer.getSampleRate());
		}

		this_thread::sleep_for(chrono::milliseconds(1));
	}
	sound.stop();
}

void AudioInput::stop()
{
	if (!ongoing)
		return;

	ongoing = false;
	thr.join();
}

void AudioInput::setNotifyStop(std::function<void()> f)
{
	this->notStop = f;
}

void AudioInput::setDataManager(DataManagerInterface* dmanager)
{
	this->dmanager = dmanager;
}

void AudioInput::setParents(QWidget* settings, QWidget* control)
{
	(void)settings;
	gui->setParent(control); gui->setWindowFlags(Qt::Window);
}

void AudioInput::showConfig()
{
	gui->show();
}

const vector<string>& AudioInput::getRegNames()
{
	return regNames;
}

void AudioInput::save(pugi::xml_node node)
{
	node.append_attribute("musicPath") = path.c_str();
}

void AudioInput::load(pugi::xml_node node)
{
	path = node.attribute("musicPath").value();
	sbuffer.loadFromFile(path);
	gui->setSliderMaximum(sbuffer.getDuration().asSeconds());
}

void AudioInput::loadFile(const string& path)
{
	sbuffer.loadFromFile(path);
	cout << sbuffer.getSampleRate() << endl;
}
