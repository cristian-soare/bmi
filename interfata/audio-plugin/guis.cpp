#include "guis.h"
#include "ui_config.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QLabel>

#include "audio.h"

using namespace std;

ConfigGui::ConfigGui(QWidget* widget, AudioInput* ainput)
	: QDialog(widget)
	, ui(new Ui::ConfigGui)
	, ainput(ainput)
{
	ui->setupUi(this);

	connect(ui->button, SIGNAL(clicked()), this, SLOT(browse()));
	connect(ui->okButton, SIGNAL(clicked()), this, SLOT(okClick()));

	ui->slider->setTracking(false);
	connect(ui->slider, SIGNAL(valueChanged(int)), this, SLOT(onSliderChanged(int)));
	connect(ui->slider, SIGNAL(sliderPressed()), this, SLOT(onSliderPressed()));
	pressed = false;
}

ConfigGui::~ConfigGui()
{

}

void ConfigGui::browse()
{
	ui->path->setText(QFileDialog::getOpenFileName(
		this, "File path", ui->path->text(), "*.ogg *.wav"));
}

void ConfigGui::okClick()
{
	ainput->path = ui->path->text().toStdString();
	ainput->loadFile(ainput->path);

	ui->slider->setMinimum(0);
	ui->slider->setMaximum(ainput->sbuffer.getDuration().asSeconds());
	ui->slider->setValue(0);
	close();
}

void ConfigGui::onSliderPressed()
{
	pressed = true;
}

void ConfigGui::slide(int val)
{
	if (!pressed)
		ui->slider->setValue(val);
}

void ConfigGui::onSliderChanged(int val)
{
	if (pressed) {
		pressed = false;
		ainput->setOffset(val);
	}
}

void ConfigGui::setSliderMaximum(int val)
{
	ui->slider->setMaximum(val);
}

