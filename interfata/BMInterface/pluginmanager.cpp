#include "pluginmanager.h"

#include "classinterfaces.h"
#include "graphmanager.h"
#include "usefulfunctions.h"
#include "datamanager.h"
#include "settingsform.h"
#include <mainwindow.h>

#include <dlfcn.h>
#include <stdexcept>

using namespace std;

InputPluginInterface* InputPluginInterface::instance;
void* InputPluginInterface::handle;
string InputPluginInterface::path;

#include <iostream>

InputPluginInterface* InputPluginInterface::getInstance(const string& path)
{
//    auto notStop = bind(&MainWindow::notifyStopExternal, MainWindow::getInstance());

    if (!path.empty()) {
        if (InputPluginInterface::path != path) {
            destroyInstance();

            InputPluginInterface* (*initPlugin)();
            handle = dlopen(path.c_str(), RTLD_LAZY);
            if (!handle)
                throw runtime_error(dlerror());

            dlerror();
            initPlugin = (InputPluginInterface* (*)())dlsym(handle, "initPlugin");
            if (dlerror())
                throw runtime_error("Cannot initialize input plugin\n");

            instance = initPlugin();
            if (!instance)
                throw runtime_error("Bad input plugin");

            DataManager::destroyInstance();
            instance->setDataManager(DataManager::getInstance());
//            instance->setNotifyStop(notStop);
            InputPluginInterface::path = path;
            DataChannels::getChannelInstance()->init(instance->getRegNames());
        } else {
            instance->setDataManager(DataManager::getInstance());
//            instance->setNotifyStop(notStop);
        }
    }
    return instance;
}

void InputPluginInterface::destroyInstance()
{
    if (!instance)
        return;

    ((void (*)())dlsym(handle, "destroyPlugin"))();
//    dlclose(handle);
}

string InputPluginInterface::getPath()
{
    return path;
}

PluginManager* PluginManager::instance;

PluginManager::PluginManager() {}

PluginManager::~PluginManager()
{
//    for (unsigned int i = 0; i < plugins.size(); i++)
//        dlclose(handles[i]);
	cout << handles.size() << endl;
}

PluginManager* PluginManager::getInstance()
{
    if (!instance)
        instance = new PluginManager();
    return instance;
}

void PluginManager::destroyInstance()
{
    delete instance;
    instance = NULL;
}

QString PluginManager::addPlugin(const QString& path, QWidget* guiParent)
{
    void* handle;
    IPlugin* (*initPlugin)(QWidget* guiParent);

	handle = dlopen(path.toLocal8Bit(), RTLD_LAZY);
    if (!handle)
        throw runtime_error(dlerror());

    dlerror();
    initPlugin = (IPlugin* (*)(QWidget*))dlsym(handle, "initPlugin");
    if (dlerror())
        throw runtime_error("Cannot initialize plugin\n");

    IPlugin* plugin = initPlugin(guiParent);
    if (!plugin)
        throw runtime_error("Bad plugin\n");

    bool found = false;
    for (unsigned int i = 0; i < plugins.size() && !found; i++)
        if (plugins[i]->getName() == plugin->getName())
            found = true;

    if (!found) {
        plugins.push_back(plugin);
        handles.push_back(handle);
        pluginPaths.push_back(path);
    } else {
        ((void (*)())dlsym(handle, "destroyPlugin"))();
        dlclose(handle);
        throw runtime_error("Plugin already loaded\n");
    }

    return plugin->getName();
}

void PluginManager::removePlugin(int x)
{
    if (x < 0 || x >= (int)plugins.size())
        return;

    ((void (*)())dlsym(handles[x], "destroyPlugin"))();
//    dlclose(handles[x]);

    plugins.erase(plugins.begin() + x);
    handles.erase(handles.begin() + x);
    pluginPaths.erase(pluginPaths.begin() + x);
}

IPlugin* PluginManager::getPlugin(int x)
{
    if (x < 0 || x >= (int)plugins.size())
        return NULL;
    return plugins[x];
}

IPlugin* PluginManager::getPlugin(QString name)
{
    for (unsigned int i = 0; i < plugins.size(); i++)
        if (plugins[i]->getName() == name)
            return plugins[i];
    return NULL;
}

const vector<QString>& PluginManager::getPluginPaths()
{
    return pluginPaths;
}

const vector<IPlugin*>& PluginManager::getPlugins()
{
    return plugins;
}
