#ifndef GRAPHSETTINGS_H
#define GRAPHSETTINGS_H

#include <QString>
#include <vector>
#include <utility>
#include <memory>
#include <shared_mutex>
#include <thread>

#include <qcustomplot.h>
#include <datamanager.h>
#include <pluginmanager.h>

#include <generalbuffer.h>

class QSpinBox;
class QComboBox;
class QCheckBox;
class QLabel;
class QHBoxLayout;
class QBoxLayout;
class ParsedExpr;

class IPlugin;
class IPluginGraph;

#include <usefulfunctions.h>

/////////////////////////// SignalPlottable ////////////////////////////
/////////////////////////////////////////////////////////////////////////

class SignalPlottable : public ThreadTask
{
public:
	SignalPlottable(DataEntry* dentry);
	~SignalPlottable();

	void setSharedMutex(std::shared_timed_mutex* smutex);
	void setPlot();

protected:
	DataEntry dentry;

	std::shared_timed_mutex* smutex;
	bool plotOk;
};

/////////////////////// SignalGraph //////////////////////////////////
//////////////////////////////////////////////////////////////////////

class SignalGraph : public QCPGraph, public SignalPlottable
{
public:
    SignalGraph(QString color, QCPAxis* x, QCPAxis* y, DataEntry* dentry);
    ~SignalGraph();

protected:
    void run();

	const float& density;

private:
    void timeRun();
    void frequencyRun();
};

/////////////////////// SignalBars ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

class SignalBars : public QCPBars, public SignalPlottable
{
public:
	SignalBars(QString color, QCPAxis* x, QCPAxis* y, DataEntry* dentry);
	~SignalBars();

	void swapAxis();

private:
	void run();
};

///////////////////////// SignalPlot /////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

class SignalPlot : public QCustomPlot
{
public:
    SignalPlot();
    virtual ~SignalPlot();

	bool addPlottable(QCPAbstractPlottable* graph);
    virtual void replot() = 0;

protected:
    const float& timeRange;

    bool writing;
    std::shared_timed_mutex smutex;
};

class TimePlot : public SignalPlot
{
public:
    TimePlot();
    ~TimePlot();

    void replot();
};

class FrequencyPlot : public SignalPlot
{
public:
    FrequencyPlot(std::pair<float, float> freqs, QCPAxis::ScaleType stype = QCPAxis::stLinear);
    ~FrequencyPlot();

    void replot();

private:
    std::pair<float, float> freqs;
};

class BarPlot : public SignalPlot
{
public:
	BarPlot(QVector<QString> names, bool vertical = true);
    ~BarPlot();

	void replot();

private:
	float count;
	QCPAxis *channelAxis, *valueAxis;
};

struct PlotRow {
    std::vector<SignalPlot*> plot;
    QHBoxLayout* box;

    PlotRow(std::vector<SignalPlot*> plot, bool tied);
    ~PlotRow();
};

///////////////////////// GraphManager /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

class GraphManager
{
public:
    static GraphManager* getInstance();
    static void destroyInstance();

    void setPlots(std::vector<PlotRow*> nrows);
    void setGui(QBoxLayout* box, int offset);

    void setTimeRange(float timeRange);
    void setDensity(float density);
    const float& getTimeRange();
    const float& getDensity();

    void clearPlots();
    void replot();

    void start();
    void stop();

    std::vector<SignalPlot*> plot;

private:
    GraphManager();
    ~GraphManager();

    static GraphManager* instance;

    std::vector<PlotRow*> rows;
    QBoxLayout* box;
    float timeRange, density;
};

#endif // GRAPHSETTINGS_H
