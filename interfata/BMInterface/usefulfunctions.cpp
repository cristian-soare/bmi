#include <usefulfunctions.h>

using namespace std;

ThreadTask::ThreadTask()
    : ongoing(false) {}

void ThreadTask::start()
{
    if (ongoing)
        return;
    if (thr.joinable())
        thr.join();

    ongoing = true;
    thr = thread(&ThreadTask::run, this);
}

void ThreadTask::stop()
{
    ongoing = false;
    if (thr.joinable())
        thr.join();
}
