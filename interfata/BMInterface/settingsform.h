 #ifndef SETTINGSFORM_H
#define SETTINGSFORM_H

#include <QString>
#include <QDialog>
#include <QHBoxLayout>
#include <QCheckBox>

#include <string>
#include <vector>
#include <iosfwd>
#include <memory>

#include <pugixml.hpp>
#include "graphmanager.h"
#include "classinterfaces.h"
#include <domainsettings.h>
#include <singledomainsettings.h>

#include <diagram/view.h>
#include <treedisplay.h>

namespace Ui {
class SettingsForm;
}

class QPushButton;
class PluginPlotInterface;
class IPluginGraph;

class DataChannels
{
public:
    static DataChannels* getChannelInstance();
    static DataChannels* getPluginInstance();

    void init(const std::vector<std::string>& channels);
    bool addChannel(const std::string& name, int domain = 0);
    void removeChannel(const std::string& name);

    std::pair<int, int> getDomain(const std::string& name);

    void addBox(QComboBox* box, int domain = 0);
    void removeBox(QComboBox* box);

private:
    DataChannels(int n);
    ~DataChannels();

    static DataChannels* channelInstance;
    static DataChannels* pluginInstance;

    int n;
    std::vector<std::vector<std::string>> channels;
    std::vector<std::vector<QComboBox*>> channelBoxes;
};

///////////////////// GraphSettings ////////////////////////////////////
////////////////////////////////////////////////////////////////////////

class GraphSettings : public QHBoxLayout
{
    Q_OBJECT

public:
    GraphSettings(QBoxLayout* parent, int addPoint);
    ~GraphSettings();

	QCPAbstractPlottable* getProperties(QCPAxis* x, QCPAxis* y);
    void setDomain(SignalBuffer::Domain domain);

    void readConfig(pugi::xml_node node);
    void saveConfig(pugi::xml_node node);
    static QStringList colorList;

private:
    QBoxLayout* parent;
    QLabel* separator;
    QComboBox* collour;
    QComboBox* channel;

    int getColorIndex(QString s);
};

//////////////////// GeneralRowSettings //////////////////////////////////

class GeneralRowSettings : public QBoxLayout
{
    Q_OBJECT

public:
    GeneralRowSettings(QBoxLayout* parent, int addPoint, Direction dir);
    ~GeneralRowSettings();

    virtual void readConfig(pugi::xml_node node) = 0;
    virtual void saveConfig(pugi::xml_node node) = 0;

protected slots:
    void on_numCols_vchanged(int num);

protected:
    QBoxLayout* parent;
    QHBoxLayout* header;

    QSpinBox* numCols;
    std::vector<QBoxLayout*> graphSet;
    virtual QBoxLayout* getNext(int pos) = 0;
};

class RowPlotsSettings : public GeneralRowSettings
{
    Q_OBJECT

public:
    RowPlotsSettings(QBoxLayout* parent, int addPoint);
    ~RowPlotsSettings();

    PlotRow* getPlotRow();
    void readConfig(pugi::xml_node node);
    void saveConfig(pugi::xml_node node);

private:
    QBoxLayout* getNext(int pos);

    QCheckBox* tieYCheck;
    QLabel* label;
};

class PlotSettings : public GeneralRowSettings
{
    Q_OBJECT

public:
    PlotSettings(QBoxLayout* parent, int addPoint);
    ~PlotSettings();

    SignalPlot* getPlot();
    void readConfig(pugi::xml_node node);
    void saveConfig(pugi::xml_node node);

private slots:
    void onTypeChanged(int x);
    void onSettingsClicked(bool ok);

private:
    QBoxLayout* getNext(int pos);

    QComboBox* typeCombo;
    QPushButton* prop;

    DomainSettings domainSettings;
	SingleDomainSettings sdomainSettings;
};

/////////////////////////// DataModifier ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

class DataModifier : public QHBoxLayout
{
    Q_OBJECT

public:
    DataModifier(QWidget* parent);
    ~DataModifier();

    bool isChecked();
    DataNode* getNode();

	void saveConfig(pugi::xml_node node);
	void loadPlugin(pugi::xml_node node);
	void loadInputs(pugi::xml_node node);

private slots:
    void onSettingsClicked(bool ok);
    void onPluginChanged(int idx);
    void onInputChanged(int idx);
    void onChannelChanged(QString idx);
    void onOutputChanged(int idx);
    void onLabelChanged(QString s);

private:
	void resetBoxes();
	void adaptBoxes();

    QCheckBox* checkBox;
    QComboBox *inputCombo, *channelCombo;
    QComboBox* pluginCombo;
    QPushButton* settingsButton;
    QComboBox* outputCombo;
    QLineEdit* olabel;

    std::shared_ptr<IPluginGraph> plugin;
    std::map<std::string, std::string> ins, outs;
};

/////////////////////////// SettingsForm ////////////////////////////////////////

class MainWindow;

class SettingsForm : public QDialog
{
    Q_OBJECT

public:
    static SettingsForm* getInstance(MainWindow* parent = NULL);
    static void destroyInstance();

    // general settings
    int bufferLength(); QString oldBufferLength;
    bool bufferChanged(bool reset = true);
    unsigned int watchdogInterval();
    void show();

private slots:
    void on_spinBox_valueChanged(int arg1);

    void on_browsePluginButton_clicked();
    void on_addPluginButton_clicked();
    void on_removePluginButton_clicked();
    void on_pluginListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_browseInPlugin_clicked();
    void on_inputSettingsButton_clicked();

    void on_addDataPath_clicked();
    void on_removeDataPath_clicked();

	void on_buttonBox_clicked(QAbstractButton *button);

private:
    explicit SettingsForm(MainWindow *parent);
    ~SettingsForm();

    static SettingsForm* instance;
    Ui::SettingsForm *ui;
    std::vector<RowPlotsSettings*> rowSet;
    std::vector<DataModifier*> dataModifier;
    MainWindow* parent;

    View* view;
    TreeDisplay* disp;

    void readConfig();
    void updateGraphs();
    bool loadPlugin(QString path);
    bool okNewConfig();
};

#endif // SETTINGSFORM_H
