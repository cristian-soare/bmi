#ifndef SINGLEDOMAINSETTINGS_H
#define SINGLEDOMAINSETTINGS_H

#include <QDialog>
#include <QVector>
#include <QString>
#include <pugixml.hpp>

namespace Ui {
class SingleDomainSettings;
}

class SingleDomainSettings : public QDialog
{
	Q_OBJECT

public:
	explicit SingleDomainSettings(QWidget *parent = 0);
	~SingleDomainSettings();

	QVector<QString> getNames();
	bool isVertical();

	void saveConfig(pugi::xml_node node);
	void readConfig(pugi::xml_node node);

private slots:
	void on_buttonBox_accepted();
	void on_buttonBox_rejected();

private:
	Ui::SingleDomainSettings *ui;
	QString text;
	bool vertical;
};

#endif // SINGLEDOMAINSETTINGS_H
