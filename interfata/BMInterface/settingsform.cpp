#include "settingsform.h"
#include "ui_settingsform.h"

#include "mainwindow.h"
#include "classinterfaces.h"
#include "pluginmanager.h"
#include "usefulfunctions.h"
#include "datamanager.h"

#include <diagram/view.h>
#include <diagram/diagramscene.h>

#include <QFileDialog>
#include <QComboBox>
#include <QCheckBox>
#include <qcustomplot.h>

#include <fstream>
#include <string>
#include <algorithm>

#include <unistd.h>
#include <pwd.h>
#include <dlfcn.h>
#include <stdexcept>

using namespace std;

DataChannels* DataChannels::channelInstance;
DataChannels* DataChannels::pluginInstance;

DataChannels* DataChannels::getChannelInstance()
{
    if (!channelInstance)
		channelInstance = new DataChannels(3);
    return channelInstance;
}

DataChannels* DataChannels::getPluginInstance()
{
    if (!pluginInstance)
        pluginInstance = new DataChannels(1);
    return pluginInstance;
}

#include <iostream>

DataChannels::DataChannels(int n)
    : n(n)
    , channels(vector<vector<string>>(n))
    , channelBoxes(vector<vector<QComboBox*>>(n))
{

}

DataChannels::~DataChannels()
{

}

void DataChannels::init(const vector<string>& newChannels)
{
    channels.clear();
    for (auto i : channelBoxes)
        for (auto j : i)
            j->clear();

    channels = vector<vector<string>>(n);
    for (const string& s : newChannels)
        addChannel(s, DataEntry::Time);
}

pair<int, int> DataChannels::getDomain(const string& name)
{
    for (unsigned int i = 0; i < channels.size(); i++) {
        unsigned int found = find(channels[i].begin(), channels[i].end(), name) - channels[i].begin();

        if (found != channels[i].size())
            return make_pair(i, found);
    }
    return make_pair(-1, -1);
}

bool DataChannels::addChannel(const string& name, int domain)
{
    if (name.empty() || getDomain(name).first >= 0)
        return false;

    channels[domain].push_back(name);
    for (QComboBox* cb : channelBoxes[domain])
        cb->addItem(QString::fromStdString(name));
    return true;
}

void DataChannels::removeChannel(const string& name)
{
    pair<int, int> found = getDomain(name);
    if (found.first < 0)
        return;

    channels[found.first].erase(channels[found.first].begin() + found.second);
    for (QComboBox* cb : channelBoxes[found.first])
        cb->removeItem(found.second);
}

void DataChannels::addBox(QComboBox* box, int domain)
{
    channelBoxes[domain].push_back(box);
    for (const string& s : channels[domain])
        box->addItem(QString::fromStdString(s));
}

void DataChannels::removeBox(QComboBox* box)
{
    box->clear();
    for (unsigned int i = 0; i < channelBoxes.size(); i++) {
        vector<QComboBox*>::iterator found = find(channelBoxes[i].begin(), channelBoxes[i].end(), box);
        if (found != channelBoxes[i].end()) {
            channelBoxes[i].erase(found);
            break;
        }
    }
}

///////////////////// GraphSettings ////////////////////////////////////
////////////////////////////////////////////////////////////////////////

QStringList GraphSettings::colorList;

GraphSettings::GraphSettings(QBoxLayout* parent, int addPoint)
    : parent(parent)
{
    separator = new QLabel("||");
    addWidget(separator);

    channel = new QComboBox();
    DataChannels::getChannelInstance()->addBox(channel);

    channel->setMaxVisibleItems(10);
    channel->setStyleSheet("combobox-popup: 0;");
    addWidget(channel);

    collour = new QComboBox();
    int idx = 0, size = collour->style()->pixelMetric(QStyle::PM_SmallIconSize);
    QPixmap pixmap(size, size);
    foreach (QString col, colorList) {
        collour->addItem("", QColor(idx));
        pixmap.fill(QColor(col));
        collour->setItemData(idx, pixmap, Qt::DecorationRole);
        idx++;
    }
    addWidget(collour);

    parent->insertLayout(addPoint, this);
    addItem(new QSpacerItem(10, 10, QSizePolicy::Expanding));
}

int GraphSettings::getColorIndex(QString s)
{
    for (int i = 0; i < colorList.size(); i++)
        if (colorList[i] == s)
            return i;
    return 0;
}

void GraphSettings::setDomain(SignalBuffer::Domain domain)
{
    DataChannels::getChannelInstance()->removeBox(channel);
    DataChannels::getChannelInstance()->addBox(channel, domain);
}

void GraphSettings::readConfig(pugi::xml_node node)
{
    channel->setCurrentText(node.attribute("channel").value());

    int idx = getColorIndex(QString::fromLocal8Bit(node.attribute("color").value()));
    collour->setCurrentIndex(idx);
}

void GraphSettings::saveConfig(pugi::xml_node node)
{
    node.set_name("Graph");
	node.append_attribute("channel") = static_cast<const char*>(channel->currentText().toLocal8Bit());
	node.append_attribute("color") = static_cast<const char*>(colorList[collour->currentIndex()].toLocal8Bit());
}

GraphSettings::~GraphSettings()
{
    parent->removeItem(this);
    removeWidget(channel); DataChannels::getChannelInstance()->removeBox(channel); delete channel;
    removeWidget(collour); delete collour;
    removeWidget(separator); delete separator;
}

QCPAbstractPlottable* GraphSettings::getProperties(QCPAxis* x, QCPAxis* y)
{
//	if (channel->currentText().isEmpty())
//		throw runtime_error("All graph inputs have not beed set.");
//	DataEntry* dentry = DataManager::getInstance()->getEntry(channel->currentText().toStdString());
//	if (dentry->domain != DataEntry::Single)
//		return new SignalGraph(colorList[collour->currentIndex()], x, y, dentry);
//	return new SignalBars(colorList[collour->currentIndex()], x, y, dentry);
}

//////////////////// GeneralRowSettings //////////////////////////////////
//////////////////////////////////////////////////////////////////////////

GeneralRowSettings::GeneralRowSettings(QBoxLayout* parent, int addPoint, Direction dir)
    : QBoxLayout(dir)
    , parent(parent)
{
    numCols = new QSpinBox();
    numCols->setMinimum(1);
    numCols->setMaximum(4);
    numCols->setValue(1);
    connect(numCols, SIGNAL(valueChanged(int)), this, SLOT(on_numCols_vchanged(int)));

    header = new QHBoxLayout();
    addLayout(header);
    header->addWidget(numCols);
    parent->insertLayout(addPoint, this);
}

GeneralRowSettings::~GeneralRowSettings()
{
    parent->removeItem(this);
    removeWidget(numCols);
    delete numCols;

    removeItem(header);
    delete header;

    while (!graphSet.empty()) {
        delete graphSet[graphSet.size() - 1];
        graphSet.pop_back();
    }
}

void GeneralRowSettings::on_numCols_vchanged(int num)
{
    if (num != (int)graphSet.size()) {
        if (num < (int)graphSet.size())
            while (num != (int)graphSet.size()) {
                delete graphSet[graphSet.size() - 1];
                graphSet.pop_back();
            }
        else
            while (num != (int)graphSet.size())
                graphSet.push_back(getNext(graphSet.size() + 2));
    }
}

//////////////////// GeneralRowSettings //////////////////////////////////
////////////////////  RowPlotsSettings  //////////////////////////////////

RowPlotsSettings::RowPlotsSettings(QBoxLayout* parent, int addPoint)
    : GeneralRowSettings(parent, addPoint, QBoxLayout::TopToBottom)
{
    label = new QLabel("Number of plots on row: ");
    header->insertWidget(0, label);

    tieYCheck = new QCheckBox("Tie Y axis range");
    header->addWidget(tieYCheck);
    graphSet.push_back(getNext(1));
}

RowPlotsSettings::~RowPlotsSettings()
{
    header->removeWidget(label); delete label;
    header->removeWidget(tieYCheck); delete tieYCheck;
}

QBoxLayout* RowPlotsSettings::getNext(int pos)
{
    return new PlotSettings(this, pos);
}

PlotRow* RowPlotsSettings::getPlotRow()
{
    vector<SignalPlot*> v(graphSet.size());
	for (unsigned int i = 0; i < graphSet.size(); i++)
		v[i] = static_cast<PlotSettings*>(graphSet[i])->getPlot();
    return new PlotRow(v, tieYCheck->isChecked());
}

void RowPlotsSettings::readConfig(pugi::xml_node node)
{
    tieYCheck->setChecked(node.attribute("tieYAxis").as_bool());
    numCols->setValue(distance(node.children().begin(), node.children().end()));
    int i = 0;
    for (pugi::xml_node c = node.first_child(); c; c = c.next_sibling(), i++)
		static_cast<PlotSettings*>(graphSet[i])->readConfig(c);
}

void RowPlotsSettings::saveConfig(pugi::xml_node node)
{
    node.set_name("PlotsRow");
    node.append_attribute("tieYAxis") = tieYCheck->isChecked();

    for (unsigned int i = 0; i < graphSet.size(); i++)
		static_cast<PlotSettings*>(graphSet[i])->saveConfig(node.append_child());
}

//////////////////// GeneralRowSettings //////////////////////////////////
////////////////////    PlotSettings    //////////////////////////////////

PlotSettings::PlotSettings(QBoxLayout* parent, int addPoint)
    : GeneralRowSettings(parent, addPoint, QBoxLayout::LeftToRight)
{
    insertWidget(1, typeCombo = new QComboBox());
    typeCombo->addItem("Time");
    typeCombo->addItem("Frequency");
	typeCombo->addItem("Single");
    connect(typeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onTypeChanged(int)));

    insertWidget(2, prop = new QPushButton());
    prop->setIcon(QIcon(":/prefix/icons/gear-icon.png"));
    prop->setMaximumSize(QSize(30, 30));
    connect(prop, SIGNAL(clicked(bool)), this, SLOT(onSettingsClicked(bool)));

    graphSet.push_back(getNext(3));
    addItem(new QSpacerItem(10, 10, QSizePolicy::Expanding));
}

PlotSettings::~PlotSettings()
{
    removeWidget(typeCombo); delete typeCombo;
    removeWidget(prop); delete prop;
}

void PlotSettings::onTypeChanged(int x)
{
    for (auto i : graphSet)
        static_cast<GraphSettings*>(i)->setDomain(static_cast<SignalBuffer::Domain>(x));
    prop->setEnabled(typeCombo->currentIndex());
    prop->setVisible(typeCombo->currentIndex());
}

void PlotSettings::onSettingsClicked(bool ok)
{
    (void)ok;
	switch (typeCombo->currentIndex()) {
	case 1: domainSettings.show(); break;
	case 2: sdomainSettings.show(); break;
	default: break;
	}
}

QBoxLayout* PlotSettings::getNext(int pos)
{
    return new GraphSettings(this, pos);
}

SignalPlot* PlotSettings::getPlot()
{
    SignalPlot* plot = nullptr;
	switch (typeCombo->currentIndex()) {
	case 0:
		plot = new TimePlot();
		break;
	case 1:
		plot = new FrequencyPlot(domainSettings.getFreq(), domainSettings.getScale());
		break;
	case 2:
		plot = new BarPlot(sdomainSettings.getNames(), sdomainSettings.isVertical());
		break;
	}
    plot->legend->setVisible(true);

	for (auto sg : graphSet)
		plot->addPlottable(static_cast<GraphSettings*>(sg)->getProperties(plot->xAxis, plot->yAxis));

	if (typeCombo->currentIndex() == 2 && !sdomainSettings.isVertical())
		for (int i = 0; i < plot->plottableCount(); i++)
			dynamic_cast<SignalBars*>(plot->plottable(i))->swapAxis();

    return plot;
}

void PlotSettings::readConfig(pugi::xml_node node)
{
	numCols->setValue(distance(node.children("Graph").begin(), node.children("Graph").end()));

	typeCombo->setCurrentText(node.attribute("type").value());
	switch (typeCombo->currentIndex()) {
	case 1: domainSettings.readConfig(node); break;
	case 2: sdomainSettings.readConfig(node); break;
	default: break;
	}

    int i = 0;
	for (auto c : node.children("Graph"))
		static_cast<GraphSettings*>(graphSet[i++])->readConfig(c);
}

void PlotSettings::saveConfig(pugi::xml_node node)
{
    node.set_name("Plot");

	node.append_attribute("type") = static_cast<const char*>(typeCombo->currentText().toLocal8Bit());
	switch (typeCombo->currentIndex()) {
	case 1: domainSettings.saveConfig(node); break;
	case 2: sdomainSettings.saveConfig(node); break;
	default: break;
	}

    for (unsigned int i = 0; i < graphSet.size(); i++)
		static_cast<GraphSettings*>(graphSet[i])->saveConfig(node.append_child());
}

/////////////////////////// DataModifier ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

static QPalette getPalette(bool ok)
{
    QPalette palette;
    if (!ok) {
        palette.setColor(QPalette::Background, Qt::red);
        palette.setColor(QPalette::Base, Qt::red);
    }
    return palette;
}

DataModifier::DataModifier(QWidget* parent)
    : QHBoxLayout(parent)
{
    addWidget(checkBox = new QCheckBox(parent));
    addWidget(inputCombo = new QComboBox(parent));
    addWidget(channelCombo = new QComboBox(parent));
    addWidget(pluginCombo = new QComboBox(parent));
    addWidget(settingsButton = new QPushButton(parent));
    addWidget(outputCombo = new QComboBox(parent));
    addWidget(olabel = new QLineEdit(parent));

    settingsButton->setIcon(QIcon(":/prefix/icons/gear-icon.png"));
    settingsButton->setMaximumHeight(30); settingsButton->setMaximumWidth(30);
    connect(settingsButton, SIGNAL(clicked(bool)), this, SLOT(onSettingsClicked(bool)));

	connect(pluginCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onPluginChanged(int)));
    connect(inputCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onInputChanged(int)));
	connect(channelCombo, SIGNAL(currentTextChanged(QString)), this, SLOT(onChannelChanged(QString)));
	connect(outputCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(onOutputChanged(int)));

    olabel->setMinimumWidth(100);
    olabel->setPalette(getPalette(false));
    connect(olabel, SIGNAL(textChanged(QString)), this, SLOT(onLabelChanged(QString)));

    DataChannels::getPluginInstance()->addBox(pluginCombo);
}

DataModifier::~DataModifier()
{
	for (auto& p : outs)
		DataChannels::getChannelInstance()->removeChannel(p.second);
    DataChannels::getChannelInstance()->removeBox(channelCombo);
    DataChannels::getPluginInstance()->removeBox(pluginCombo);

    removeWidget(checkBox); delete checkBox;
    removeWidget(inputCombo); delete inputCombo;
    removeWidget(channelCombo); delete channelCombo;
    removeWidget(pluginCombo); delete pluginCombo;
    removeWidget(settingsButton); delete settingsButton;
    removeWidget(outputCombo); delete outputCombo;
    removeWidget(olabel); delete olabel;
}

bool DataModifier::isChecked()
{
    return checkBox->isChecked();
}

static bool checkSameIO(map<string, string>& imap, vector<pair<string, DataEntry::Domain>>& ivec)
{
	if (imap.size() != ivec.size())
		return false;
	for (const auto& i : ivec)
		if (imap.find(i.first) == imap.end())
			return false;
	return true;
}

void DataModifier::onSettingsClicked(bool ok)
{
    (void)ok;
    plugin->show();
	adaptBoxes();
}

void DataModifier::adaptBoxes()
{
	vector<pair<string, DataEntry::Domain>> ivec = plugin->getIOProp().first, ovec = plugin->getIOProp().second;
	if (!checkSameIO(ins, ivec) || !checkSameIO(outs, ovec))
		resetBoxes();
}

void DataModifier::resetBoxes()
{
	for (pair<string, string> p : outs)
		DataChannels::getChannelInstance()->removeChannel(p.second);

	ins.clear();
	outs.clear();

	inputCombo->clear();
	for (const auto& i : plugin->getIOProp().first) {
		inputCombo->addItem(QString::fromStdString(i.first));
		ins[i.first];
	}
	outputCombo->clear();
	for (const auto& i : plugin->getIOProp().second) {
		outputCombo->addItem(QString::fromStdString(i.first));
		outs[i.first];
	}
}

void DataModifier::onPluginChanged(int idx)
{
    if (idx < 0)
        return;
    plugin = shared_ptr<IPluginGraph>(PluginManager::getInstance()->getPlugin(idx)->initGraphPlugin());
	resetBoxes();
}

void DataModifier::onInputChanged(int idx)
{
	if (idx < 0)
		return;
	pair<string, SignalBuffer::Domain> prop = plugin->getIOProp().first[idx];
	const string oldChannel = ins[prop.first];
	DataChannels::getChannelInstance()->removeBox(channelCombo);
	DataChannels::getChannelInstance()->addBox(channelCombo, prop.second);
	channelCombo->setCurrentText(QString::fromStdString(oldChannel));
}

void DataModifier::onChannelChanged(QString s)
{
    ins[inputCombo->currentText().toStdString()] = s.toStdString();
}

void DataModifier::onOutputChanged(int idx)
{
	if (idx < 0)
		return;
    pair<string, SignalBuffer::Domain> prop = plugin->getIOProp().second[idx];
    olabel->setText(QString::fromStdString(outs[prop.first]));
}

void DataModifier::onLabelChanged(QString s)
{
    SignalBuffer::Domain domain = plugin->getIOProp().second[outputCombo->currentIndex()].second;
    DataChannels::getChannelInstance()->removeChannel(outs[outputCombo->currentText().toStdString()]);
    bool valid = DataChannels::getChannelInstance()->addChannel(s.toStdString(), domain);

    outs[outputCombo->currentText().toStdString()] = valid ? s.toStdString() : "";
    olabel->setPalette(getPalette(valid));
}

DataNode* DataModifier::getNode()
{
//	DataManager::getInstance()->markPluginUsed(pluginCombo->currentText());
//	for (auto i : ins)
//		if (i.second.empty())
//			throw runtime_error("Invalid or unset input for data node");
//	for (auto i : outs)
//		if (i.second.empty())
//			throw runtime_error("Invalid or unset output for data node");
//    return new DataNode(plugin, ins, outs);
}

void DataModifier::saveConfig(pugi::xml_node node)
{
	node.append_attribute("plugin") = static_cast<const char*>(pluginCombo->currentText().toLocal8Bit());
	plugin->save(node);
	for (auto& i : ins) {
		pugi::xml_node n = node.append_child("input");
		n.append_attribute("name") = i.first.c_str();
		n.text().set(i.second.c_str());
	}
	for (auto& i : outs) {
		pugi::xml_node n = node.append_child("output");
		n.append_attribute("name") = i.first.c_str();
		n.text().set(i.second.c_str());
	}
}

void DataModifier::loadPlugin(pugi::xml_node node)
{
	pluginCombo->setCurrentText(QString::fromLocal8Bit(node.attribute("plugin").value()));
	plugin->load(node);
	adaptBoxes();
	for (auto i : node.children("output")) {
		outputCombo->setCurrentText(QString::fromLocal8Bit(i.attribute("name").value()));
		olabel->setText(QString::fromLocal8Bit(i.text().as_string()));
	}
}

void DataModifier::loadInputs(pugi::xml_node node)
{
	for (auto i : node.children("input")) {
		inputCombo->setCurrentText(QString::fromLocal8Bit(i.attribute("name").value()));
		channelCombo->setCurrentText(QString::fromLocal8Bit(i.text().as_string()));
	}
}

/////////////////////////// SettingsForm ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

SettingsForm* SettingsForm::instance;

SettingsForm::SettingsForm(MainWindow* parent) :
    QDialog(parent),
    ui(new Ui::SettingsForm),
    parent(parent)
{
    ui->setupUi(this);

    // general tab
    ui->bufLenLineEdit->setText(QString::number(100000));

    // graphs tab
    GraphSettings::colorList << "red" << "green" << "blue" << "yellow" << "brown" << "black";

    ui->tabDiagram->layout()->addWidget(view = new View("proba"));
    ui->tabDisplay->layout()->addWidget(disp = new TreeDisplay(static_cast<DiagramScene*>((view->view()->scene()))));

    readConfig();
    okNewConfig();
}

SettingsForm::~SettingsForm()
{
    while (rowSet.size()) {
        delete rowSet[rowSet.size() - 1];
        rowSet.pop_back();
    }
	for (auto i : dataModifier)
		delete i;
    delete ui;
}

SettingsForm* SettingsForm::getInstance(MainWindow* parent)
{
    if (!instance)
        instance = new SettingsForm(parent ? parent : MainWindow::getInstance());
    return instance;
}

void SettingsForm::destroyInstance()
{
    delete instance;
    instance = NULL;
}

static string getConfigDir()
{
    passwd* pass = getpwuid(getuid());
    string file(pass->pw_name);
    file = "/home/" + file + "/.bmirc";
    return file;
}

void SettingsForm::readConfig()
{
    pugi::xml_document doc;
    if (doc.load_file(getConfigDir().c_str()).status != pugi::status_ok)
        return;

    pugi::xml_node root = doc.child("bmi");
    ui->bufLenLineEdit->setText(QString::fromLocal8Bit(root.attribute("bufferLength").value()));
    ui->wdogSpin->setValue(root.attribute("watchdog").as_int());
    parent->load(root);

    for (pugi::xml_node node = root.child("Plugin"); node; node = node.next_sibling("Plugin")) {
        loadPlugin(node.attribute("path").value());
        PluginManager::getInstance()->getPlugin(node.attribute("name").value())->load(node);
    }

    static_cast<DiagramScene*>(view->view()->scene())->load(root.child("Diagram"));
    disp->load(root.child("TreeDisplay"));
//	for (auto node : root.children("Data")) {
//		on_addDataPath_clicked();
//		dataModifier.back()->loadPlugin(node);
//	}
//	int i = 0;
//	for (auto node : root.children("Data"))
//		dataModifier[i++]->loadInputs(node);

//    ui->spinBox->setValue(distance(root.children("PlotsRow").begin(), root.children("PlotsRow").end()));
//	i = 0;
//    for (pugi::xml_node c = root.child("PlotsRow"); c; c = c.next_sibling("PlotsRow"), i++)
//        rowSet[i]->readConfig(c);
}

void SettingsForm::updateGraphs()
{
    vector<PlotRow*> plots(rowSet.size(), nullptr);
    vector<DataNode*> dnodes(dataModifier.size(), nullptr);

    view->registerDataNodes();
//	for (unsigned int i = 0; i < dataModifier.size(); i++)
//		dnodes[i] = dataModifier[i]->getNode();
//	for (unsigned int i = 0; i < rowSet.size(); i++)
//		plots[i] = rowSet[i]->getPlotRow();
//	for (auto i : dnodes)
//		i->completeInputs();

    DataManager::commitReplacement(true);
    DataManager::getInstance()->setWatchDogInterval(ui->wdogSpin->value() * 1000);
    GraphManager::getInstance()->setPlots(plots);
    MainWindow::getInstance()->setSplitter(disp->getDisplay().front());
//    for (auto i : dnodes)
//        DataManager::getInstance()->addNode(i);
    parent->addGraphs();
}


bool SettingsForm::okNewConfig()
{
    try {
        updateGraphs();
    } catch (const runtime_error& e) {
        QMessageBox::warning(this, "Error", QString::fromUtf8(e.what()));
        return false;
    }

    if (ui->bufLenLineEdit->text().toInt() == 0) {
        QMessageBox::warning(this, "Error", "Invalid buffer length");
        return false;
    }

    oldBufferLength = ui->bufLenLineEdit->text();

//    try {
//        InputPluginInterface::getInstance(ui->inpluginLineEdit->text().toStdString())->setParents(this, parent);
//    } catch (std::exception& e) {
//        QMessageBox::warning(this, "Error",QString::fromUtf8(e.what()));
//        return false;
//    }

    return true;
}

int SettingsForm::bufferLength() { return ui->bufLenLineEdit->text().toInt(); }
unsigned int SettingsForm::watchdogInterval() { return ui->wdogSpin->value(); }

bool SettingsForm::bufferChanged(bool reset)
{
    bool ok = ui->bufLenLineEdit->isModified();
    if (reset) {
        ui->bufLenLineEdit->setModified(false);
    }
    return ok;
}

void SettingsForm::on_spinBox_valueChanged(int num)
{
    if (num != (int)rowSet.size()) {
        if (num < (int)rowSet.size())
            while (num != (int)rowSet.size())  {
                delete rowSet[rowSet.size() - 1];
                rowSet.pop_back();
            }
        else
            while (num != (int)rowSet.size())
                rowSet.push_back(new RowPlotsSettings(ui->verticalGraph, rowSet.size() + 1));
    }
}

///////////////// Plugin Manager ////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

void SettingsForm::on_browsePluginButton_clicked()
{
    ui->pluginPathLineEdit->setText(
                QFileDialog::getOpenFileName(
                    this, "Plugin", ui->pluginPathLineEdit->text()));
}

bool SettingsForm::loadPlugin(QString path)
{
    try {
        QString pname = PluginManager::getInstance()->addPlugin(path, this);
        ui->pluginListWidget->addItem(pname);
        DataChannels::getPluginInstance()->addChannel(pname.toStdString());
        return true;
    } catch (const exception& ex) {
        QMessageBox::warning(this, "Error", QString::fromLocal8Bit(ex.what()) + path);
        return false;
    }
}

void SettingsForm::on_addPluginButton_clicked()
{
    loadPlugin(ui->pluginPathLineEdit->text());
}

void SettingsForm::on_removePluginButton_clicked()
{
    int x = ui->pluginListWidget->currentRow();
	if (x < 0)
		return;

	if (DataManager::getInstance()->isPluginUsed(ui->pluginListWidget->currentItem()->text())) {
        QMessageBox::warning(this, "Error", "The plugin is being used !");
        return;
    }

    PluginManager::getInstance()->removePlugin(x);
    DataChannels::getPluginInstance()->removeChannel(ui->pluginListWidget->currentItem()->text().toStdString());
    ui->pluginListWidget->takeItem(x);
}

void SettingsForm::on_pluginListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    PluginManager::getInstance()->getPlugin(item->text())->show();
}

void SettingsForm::on_browseInPlugin_clicked()
{
    ui->inpluginLineEdit->setText(
                QFileDialog::getOpenFileName(
                    this, "Input plugin", ui->inpluginLineEdit->text()));
    InputPluginInterface::getInstance(ui->inpluginLineEdit->text().toStdString());
}

void SettingsForm::on_inputSettingsButton_clicked()
{
    if (InputPluginInterface::getInstance())
        InputPluginInterface::getInstance()->showSettings();
}

void SettingsForm::on_addDataPath_clicked()
{
    dataModifier.push_back(new DataModifier(this));
    int x = ui->tabData->layout()->count();
	static_cast<QVBoxLayout*>(ui->tabData->layout())->insertLayout(x - 1, dataModifier.back());
}

void SettingsForm::on_removeDataPath_clicked()
{
    for (vector<DataModifier*>::iterator i = dataModifier.begin(); i != dataModifier.end();)
        if ((*i)->isChecked()) {
            ui->tabData->layout()->removeItem(*i);
            delete *i;
            i = dataModifier.erase(i);
        } else {
            i++;
        }
}

void SettingsForm::on_buttonBox_clicked(QAbstractButton *button)
{
	QDialogButtonBox::StandardButton bflag = ui->buttonBox->standardButton(button);
	switch (bflag) {
	case QDialogButtonBox::Save: {
		if (!okNewConfig())
			return;

		pugi::xml_document doc;
		pugi::xml_node root = doc.append_child("bmi");
		root.append_attribute("bufferLength") = static_cast<const char*>(ui->bufLenLineEdit->text().toLocal8Bit());
		root.append_attribute("watchdog") = ui->wdogSpin->value();
		parent->save(root);

		const vector<QString>& paths = PluginManager::getInstance()->getPluginPaths();
		const vector<IPlugin*>& plugins = PluginManager::getInstance()->getPlugins();
		for (unsigned int i = 0; i < paths.size(); i++) {
			pugi::xml_node node = root.append_child("Plugin");
			node.append_attribute("name") = static_cast<const char*>(plugins[i]->getName().toLocal8Bit());
			node.append_attribute("path") = static_cast<const char*>(paths[i].toLocal8Bit());
            plugins[i]->save(node);
		}

        auto dchild = root.append_child("Diagram");
        static_cast<DiagramScene*>(view->view()->scene())->save(dchild);

        auto tchild = root.append_child("TreeDisplay");
        disp->save(tchild);
//		for (auto i : dataModifier)
//			i->saveConfig(root.append_child("Data"));

//		for (unsigned int i = 0; i < rowSet.size(); i++)
//			rowSet[i]->saveConfig(root.append_child());

		doc.save_file(getConfigDir().c_str());
	}
		break;

	case QDialogButtonBox::Ok:
	case QDialogButtonBox::Apply:
		if (okNewConfig() && bflag == QDialogButtonBox::Ok)
			close();
		break;

	case QDialogButtonBox::Cancel:
		ui->bufLenLineEdit->setText(oldBufferLength);
        DataManager::getInstance()->commitReplacement(false);
		close();
		break;

	default:
		break;
	}
}

void SettingsForm::show()
{
    DataManager::getInstance()->formReplacement();
    QDialog::show();
}
