#-------------------------------------------------
#
# Project created by QtCreator 2014-07-15T17:12:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport opengl

TARGET = BMInterface
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++14
CONFIG += debug

LIBS += -ldl -lpugixml -lqcustomplot

INCLUDEPATH += ../commons

SOURCES += main.cpp\
        mainwindow.cpp \
    settingsform.cpp \
    graphmanager.cpp \
    pluginmanager.cpp \
    datamanager.cpp \
    usefulfunctions.cpp \
    domainsettings.cpp \
    singledomainsettings.cpp \
    diagram/chip.cpp \
    diagram/diagramscene.cpp \
    diagram/view.cpp \
    treedisplay.cpp

HEADERS  += mainwindow.h \
    settingsform.h \
    communication/typedefs.h \
    classinterfaces.h \
    graphmanager.h \
    pluginmanager.h \
    usefulfunctions.h \
    datamanager.h \
    domainsettings.h \
    singledomainsettings.h \
    ../commons/signalbuffer.h \
    diagram/chip.h \
    diagram/diagramscene.h \
    diagram/view.h \
    treedisplay.h

FORMS    += mainwindow.ui \
    settingsform.ui \
    domainsettings.ui \
    singledomainsettings.ui

DEPENDPATH += communication/

RESOURCES += \
    iconsRes.qrc
