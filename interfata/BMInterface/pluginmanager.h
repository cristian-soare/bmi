#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <vector>
#include <utility>
#include <tuple>
#include <QString>
#include <QDialog>
#include <pugixml.hpp>

#include <datamanager.h>

class IPlugin;
class SignalGraph;
class SignalPlot;
class QWidget;

///////////////////////////// InputPluginInterface ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

class InputPluginInterface
{
public:
    static InputPluginInterface* getInstance(const std::string& path = "");
    static void destroyInstance();
    static std::string getPath();

    virtual bool start() = 0;
    virtual void stop() = 0;

    virtual void setNotifyStop(std::function<void()> f) = 0;
    virtual void setDataManager(DataManagerInterface* dmanager) = 0;
    virtual void setParents(QWidget* settings, QWidget* control) { (void)settings; (void)control; }

    virtual void showSettings() {}
    virtual void showConfig() {}

    virtual unsigned int getByteCount() { return 0; }
    virtual float getUsage() { return 0; }
    virtual const std::vector<std::string>& getRegNames() = 0;

    virtual void save(pugi::xml_node node) { (void)node; }
    virtual void load(pugi::xml_node node) { (void)node; }

protected:
    virtual ~InputPluginInterface() {}

private:
    static InputPluginInterface* instance;
    static void* handle;
    static std::string path;
};

//////////////////////////// PluginManager ////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

class PluginManager
{
public:
    static PluginManager* getInstance();
    static void destroyInstance();

    QString addPlugin(const QString& path, QWidget* guiParent);
    void removePlugin(int x);

    IPlugin* getPlugin(int x);
    IPlugin* getPlugin(QString name);

    const std::vector<QString>& getPluginPaths();
    const std::vector<IPlugin*>& getPlugins();
    void removePluginInstances();

private:
    PluginManager();
    ~PluginManager();

    static PluginManager* instance;

    std::vector<IPlugin*> plugins;
    std::vector<void*> handles;
    std::vector<QString> pluginPaths;
};

class IPluginGraph
{
public:
    virtual ~IPluginGraph() {}

    virtual void show() = 0;
    virtual void reset() {}

    virtual void start() {}
    virtual void stop() {}
    virtual void run() = 0;

    virtual std::pair<
        std::vector<std::pair<std::string, SignalBuffer::Domain>>,
        std::vector<std::pair<std::string, SignalBuffer::Domain>>> getIOProp() = 0;
    virtual SignalBuffer*& operator[](const std::string& s) = 0;
    virtual std::vector<std::pair<std::string, QWidget*>> getWidgets() {
        return std::vector<std::pair<std::string, QWidget*>>(); }
    virtual void setNotifyStop(std::function<void()> f) { this->notStop = f; }

    virtual void save(pugi::xml_node node) { (void)node; }
    virtual void load(pugi::xml_node node) { (void)node; }

protected:
    std::function<void()> notStop;
};

class IPlugin : public QDialog
{
protected:
    IPlugin(QWidget* parent) : QDialog(parent) {}
    virtual ~IPlugin() {}

public:
    virtual QString getName() = 0;
    virtual IPluginGraph* initGraphPlugin() = 0;

    virtual void save(pugi::xml_node node) { (void)node; }
    virtual void load(pugi::xml_node node) { (void)node; }
};

#endif // PLUGINMANAGER_H
