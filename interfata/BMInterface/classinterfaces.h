#ifndef DESTINATIONGRAPH_H
#define DESTINATIONGRAPH_H

#include <string>
#include <vector>
#include <QDialog>

class QCustomPlot;

struct sample {
    int channel;
    double x, y;
};

class Configurer
{
public:
    virtual std::string getRegisterConfig() = 0;
};

#endif // DESTINATIONGRAPH_H
