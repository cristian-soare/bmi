#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qcustomplot.h>
#include "settingsform.h"
#include "graphmanager.h"
#include "pluginmanager.h"
#include "datamanager.h"

#include <QSplitter>
#include <QVBoxLayout>

#include <iostream>

using namespace std;

MainWindow* MainWindow::instance;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    cbuffer = NULL; client = NULL; interp = NULL;
    instance = this;

    timer = new QTimer();
    timer->setInterval(33);

    statusTimer = new QTimer();
    statusTimer->setInterval(300);

    SettingsForm::getInstance(this);

    connect(timer, SIGNAL(timeout()), this, SLOT(run()));
    connect(statusTimer, SIGNAL(timeout()), this, SLOT(statusFunc()));
    connect(this, SIGNAL(stopGuiSig(bool)), this, SLOT(stopGuiSlot(bool)));
}

MainWindow::~MainWindow()
{
    if (timer->isActive()) {
        GraphManager::getInstance()->stop();
        InputPluginInterface::getInstance()->stop();
        stopGuiSlot();
    }

    delete timer;
    SettingsForm::destroyInstance();
    GraphManager::destroyInstance();
	InputPluginInterface::destroyInstance();
    DataManager::destroyInstance();
    PluginManager::destroyInstance();
    delete ui;
}

MainWindow* MainWindow::getInstance()
{
    if (!instance)
        new MainWindow();
    return instance;
}

void MainWindow::destroyInstance()
{
    delete instance;
    instance = NULL;
}

void MainWindow::notifyStop()
{
    if (!stoppable)
        return;

    stoppable = false;
    std::thread([this](){ emit this->stopGuiSig(true); }).detach();
}

void MainWindow::stopGuiSlot(bool error)
{
    DataManager::getInstance()->stop();
    timer->stop();
    statusTimer->stop();

    statusBar()->showMessage("Idle");
    ui->actionStart->setIcon(QIcon(":/prefix/icons/Play-icon.png"));
    ui->actionStart->setText("Start");

    if (error) {
        QString errmsg = "Communication with the device has been lost !";
        QMessageBox::critical(this, "Error", errmsg);
    }
}

void MainWindow::run()
{
    GraphManager::getInstance()->replot();
}

void MainWindow::statusFunc()
{
    QString msg;
    static int count = 0;
////    uint diff = InputPluginInterface::getInstance()->getByteCount() - count;

//    msg = "Transfer: " + QString::number((float)diff / 300.) + "kB/s | ";
//    msg += "Buffer usage: " + QString::number(InputPluginInterface::getInstance()->getUsage()) + "%";
//    count += diff;
    statusBar()->showMessage(msg);
}

void MainWindow::on_actionStart_triggered()
{
    if (!timer->isActive()) {
//        if (!InputPluginInterface::getInstance()) {
//            QMessageBox::warning(this, "Error", "No input device plugin added !");
//            return;
//        }

//        if (!InputPluginInterface::getInstance()->start()) {
//            QMessageBox::warning(this, "Error", "Cannot connect to the device !");
//            return;
//        }
//        GraphManager::getInstance()->start();

        timer->start();
        statusTimer->start();

        ui->actionStart->setIcon(QIcon(":/prefix/icons/Stop-icon.png"));
        ui->actionStart->setText("Stop");
        stoppable = true;
        DataManager::getInstance()->start();
    } else {
//        DataManager::getInstance()->stop();
//        GraphManager::getInstance()->stop();
//        InputPluginInterface::getInstance()->stop();
        stopGuiSlot();
    }
}

void MainWindow::on_timeRangeHorizontalSlider_valueChanged(int value)
{
    float timeRange = (float)value / 1000.;
//    ui->timeRangeLabel->setText(QString::number(timeRange) + " s");
    GraphManager::getInstance()->setTimeRange(timeRange);
}

void MainWindow::on_refreshRateHorizontalSlider_valueChanged(int value)
{
    timer->setInterval(1000 / value);
//    ui->refreshLabel->setText(QString::number(value) + " Hz");
}

void MainWindow::on_actionSettings_triggered()
{
    if (!timer->isActive())
        SettingsForm::getInstance()->show();
}

void MainWindow::addGraphs()
{
    GraphManager::getInstance()->setGui(ui->verticalLayout, 0);
}

void MainWindow::setSplitter(QSplitter* splitter)
{
    QLayoutItem* widget;
    while ((widget = ui->verticalLayout->takeAt(0)) != nullptr)
        ui->verticalLayout->removeWidget(widget->widget());
    ui->verticalLayout->addWidget(splitter);
}

void MainWindow::on_actionADS_Configuration_triggered()
{
    if (InputPluginInterface::getInstance())
        InputPluginInterface::getInstance()->showConfig();
    else
        QMessageBox::warning(this, "Error", "No input device plugin added !");
}

void MainWindow::on_densityHorizontalSlider_valueChanged(int value)
{
//    ui->densityValueLabel->setText(QString::number(value) + " p/s");
    GraphManager::getInstance()->setDensity(1. / value);
}

void MainWindow::save(pugi::xml_node node)
{
//    node.append_attribute("timeRange") = ui->timeRangeHorizontalSlider->value();
//    node.append_attribute("refreshRate") = ui->refreshRateHorizontalSlider->value();
//    node.append_attribute("density") = ui->densityHorizontalSlider->value();
}

void MainWindow::load(pugi::xml_node node)
{
//    ui->timeRangeHorizontalSlider->setValue(node.attribute("timeRange").as_int());
//    ui->refreshRateHorizontalSlider->setValue(node.attribute("refreshRate").as_int());
//    ui->densityHorizontalSlider->setValue(node.attribute("density").as_int());
}
