#ifndef DOMAINSETTINGS_H
#define DOMAINSETTINGS_H

#include <QDialog>
#include <utility>
#include <qcustomplot.h>
#include <pugixml.hpp>

namespace Ui {
class DomainSettings;
}

class DomainSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DomainSettings(QWidget *parent = 0);
    ~DomainSettings();

    std::pair<float, float> getFreq();
    QCPAxis::ScaleType getScale();

	void saveConfig(pugi::xml_node node);
	void readConfig(pugi::xml_node node);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::DomainSettings *ui;
    std::pair<float, float> freqs;
    QCPAxis::ScaleType stype;
};

#endif // DOMAINSETTINGS_H
