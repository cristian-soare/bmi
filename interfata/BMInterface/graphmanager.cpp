#include "graphmanager.h"
#include "classinterfaces.h"

#include "datamanager.h"
#include "pluginmanager.h"
#include <generalbuffer.h>

#include <qcustomplot.h>
#include <QHBoxLayout>
#include <QBoxLayout>
#include <mutex>

using namespace std;

SignalPlottable::SignalPlottable(DataEntry* dentry)
	: dentry(*dentry)
	, plotOk(false) {}

SignalPlottable::~SignalPlottable() {}

void SignalPlottable::setSharedMutex(shared_timed_mutex* smutex)
{
	this->smutex = smutex;
}

void SignalPlottable::setPlot()
{
	plotOk = true;
}

SignalGraph::SignalGraph(QString color, QCPAxis* x, QCPAxis* y, DataEntry* dentry)
	: QCPGraph(x, y)
	, SignalPlottable(dentry)
	, density(GraphManager::getInstance()->getDensity())
{
    setPen(QPen(QColor(color)));
    setAntialiasedFill(false);
    setName(QString::fromStdString(dentry->name));

    if (dentry->domain == DataEntry::Frequency)
        setLineStyle(QCPGraph::lsImpulse);
}

SignalGraph::~SignalGraph() {}

void SignalGraph::run()
{
    switch (dentry.domain) {
    case DataEntry::Time: timeRun(); break;
    case DataEntry::Frequency: frequencyRun(); break;
	default: break;
    }
}

void SignalGraph::timeRun()
{
    while (ongoing) {
        int size = dentry.beginRead();
        shared_lock<shared_timed_mutex> lock(*smutex);

        if (size) {
            double lastx = data()->count() > 0 ? data()->lastKey() : 0;

            for (int i = 0; i < size; i++)
                if (dentry[i].first - lastx > density) {
                    addData(dentry[i].first, dentry[i].second);
                    lastx = dentry[i].first;
                } else if (dentry[i].first - lastx < 0) {
                    data()->clear();
                    lastx = dentry[i].first;
                }
            dentry.endRead(size);
        } else {
            ongoing = false;
        }
    }
}

void SignalGraph::frequencyRun()
{
    while (ongoing) {
        int size = dentry.beginRead();
        shared_lock<shared_timed_mutex> lock(*smutex);

        if (!size) {
            ongoing = false;
            break;
        }

        if (plotOk && size) {
            data()->clear();
            for (int i = size - 2; i >= 0 && dentry[i].first < dentry[i + 1].first; i--)
                addData(dentry[i].first, dentry[i].second);
            plotOk = false;
			dentry.endRead();
        }
    }
}

SignalBars::SignalBars(QString color, QCPAxis* x, QCPAxis* y, DataEntry* dentry)
	: QCPBars(x, y)
	, SignalPlottable(dentry)
{
	QColor qc(color);
	setPen(QPen(qc));
	qc.setAlpha(qc.alpha() - 120);
	setBrush(qc);
	setAntialiasedFill(false);
	setName(QString::fromStdString(dentry->name));
}

SignalBars::~SignalBars() {}

void SignalBars::run()
{
	while (ongoing) {
		int size = dentry.beginRead();
		shared_lock<shared_timed_mutex> lock(*smutex);
		for (int i = 0; i < size; i++)
			data()->insert(dentry[i].first, QCPBarData(dentry[i].first, dentry[i].second));
		dentry.endRead();
	}
}

void SignalBars::swapAxis()
{
	QCPAxis* man = keyAxis();
	setKeyAxis(valueAxis());
	setValueAxis(man);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////// SignalPlot //////////////////////////////////////////////

SignalPlot::SignalPlot()
    : timeRange(GraphManager::getInstance()->getTimeRange()) {}

SignalPlot::~SignalPlot()
{

}

bool SignalPlot::addPlottable(QCPAbstractPlottable* sg)
{
	dynamic_cast<SignalPlottable*>(sg)->setSharedMutex(&smutex);
    return QCustomPlot::addPlottable(sg);
}

TimePlot::TimePlot()
    : SignalPlot() {}

TimePlot::~TimePlot() {}

void TimePlot::replot()
{
    unique_lock<shared_timed_mutex> lock(smutex);
    double minimum = 0;

	for (int i = 0; i < graphCount(); i++)
        if (graph(i)->data()->size() > 0)
            minimum = max(graph(i)->data()->lastKey(), minimum);

    xAxis->setRange(minimum - timeRange + 0.1, minimum - 0.1);

	for (int i = 0; i < graphCount(); i++)
        graph(i)->removeDataBefore(minimum - timeRange - 3);
    QCustomPlot::replot();
}

FrequencyPlot::FrequencyPlot(pair<float, float> freqs, QCPAxis::ScaleType stype)
	: freqs(freqs)
{
	xAxis->setScaleType(stype);
	xAxis->setScaleLogBase(10);
}

FrequencyPlot::~FrequencyPlot() {}

void FrequencyPlot::replot()
{
    unique_lock<shared_timed_mutex> lock(smutex);
	for (int i = 0; i < graphCount(); i++)
        static_cast<SignalGraph*>(graph(i))->setPlot();
    xAxis->setRange(freqs.first, freqs.second);
	yAxis->setRange(0, yAxis->range().upper - yAxis->range().lower);
    QCustomPlot::replot();
}

BarPlot::BarPlot(QVector<QString> names, bool vertical)
	: channelAxis(vertical ? xAxis : yAxis)
	, valueAxis(vertical ? yAxis : xAxis)
{
	channelAxis->setAutoTicks(false);
	channelAxis->setAutoTickLabels(false);

	QVector<double> ticks;
	for (int i = 0; i < names.size(); i++)
		ticks << i;
	channelAxis->setTickVector(ticks);
	channelAxis->setTickVectorLabels(names);

	count = names.size();
}

void BarPlot::replot()
{
	unique_lock<shared_timed_mutex> lock(smutex);
	channelAxis->setRange(-1, count);
	valueAxis->setRange(0, valueAxis->range().upper - valueAxis->range().lower);
	QCustomPlot::replot();
}

BarPlot::~BarPlot() {}

//////////////////////////// PlotRow ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

PlotRow::PlotRow(vector<SignalPlot*> plot, bool tied)
    : plot(plot)
{
    box = new QHBoxLayout();
    for (unsigned int i = 0; i < plot.size(); i++) {
        plot[i]->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom);
        plot[i]->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        box->addWidget(plot[i]);
    }

    if (tied)
        for (unsigned int i = 0; i < plot.size(); i++)
            for (unsigned int j = 0; j < plot.size(); j++)
                QObject::connect(plot[i]->yAxis, SIGNAL(rangeChanged(QCPRange)), plot[j]->yAxis, SLOT(setRange(QCPRange)));
}

PlotRow::~PlotRow()
{
    for (unsigned int i = 0; i < plot.size(); i++) {
        box->removeWidget(plot[i]);
        delete plot[i];
    }
    delete box;
}

//////////////////////////////// GraphManager ///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

GraphManager* GraphManager::instance;

GraphManager* GraphManager::getInstance()
{
    if (!instance)
        instance = new GraphManager();
    return instance;
}

void GraphManager::destroyInstance()
{
    delete instance;
    instance = NULL;
}

GraphManager::GraphManager()
{
    instance = NULL;
    timeRange = 1;
    density = 1. / 200.;
}

GraphManager::~GraphManager()
{
    setPlots(vector<PlotRow*>());
}

void GraphManager::setPlots(std::vector<PlotRow*> nrows)
{
	for (unsigned int i = 0; i < rows.size(); i++) {
		box->removeItem(rows[i]->box);
		delete rows[i];
	}
    rows = nrows;
}

void GraphManager::setGui(QBoxLayout* box, int offset)
{
    for (unsigned int i = 0; i < rows.size(); i++)
        box->insertLayout(offset + i, rows[i]->box);
    this->box = box;

    plot.clear();
    for (unsigned int i = 0; i < rows.size(); i++)
        for (unsigned int j = 0; j < rows[i]->plot.size(); j++)
            plot.push_back(rows[i]->plot[j]);
}

void GraphManager::setTimeRange(float timeRange) { this->timeRange = timeRange; }
void GraphManager::setDensity(float density) { this->density = density; }
const float& GraphManager::getTimeRange() { return timeRange; }
const float& GraphManager::getDensity() { return density; }

void GraphManager::clearPlots()
{
    for (unsigned int i = 0; i < plot.size(); i++)
		for (int j = 0; j < plot[i]->plottableCount(); j++)
			plot[i]->plottable(j)->clearData();
}

void GraphManager::start()
{
    clearPlots();
    DataManager::getInstance()->start();

    for (unsigned int i = 0; i < plot.size(); i++)
		for (int j = 0; j < plot[i]->plottableCount(); j++)
			dynamic_cast<SignalPlottable*>(plot[i]->plottable(j))->start();
}

void GraphManager::stop()
{
    DataManager::getInstance()->stop();
    for (unsigned int i = 0; i < plot.size(); i++)
		for (int j = 0; j < plot[i]->plottableCount(); j++)
			dynamic_cast<SignalPlottable*>(plot[i]->plottable(j))->stop();
}

void GraphManager::replot()
{
    for (unsigned int i = 0; i < plot.size(); i++)
        plot[i]->replot();
}
