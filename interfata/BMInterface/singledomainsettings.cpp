#include "singledomainsettings.h"
#include "ui_singledomainsettings.h"

SingleDomainSettings::SingleDomainSettings(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::SingleDomainSettings)
{
	ui->setupUi(this);
	vertical = true;
}

SingleDomainSettings::~SingleDomainSettings()
{
	delete ui;
}

void SingleDomainSettings::on_buttonBox_accepted()
{
	text = ui->plainTextEdit->toPlainText();
	vertical = ui->verticalCB->isChecked();
}

void SingleDomainSettings::on_buttonBox_rejected()
{
	ui->plainTextEdit->setPlainText(text);
	ui->verticalCB->setChecked(vertical);
}

QVector<QString> SingleDomainSettings::getNames()
{
	QStringList slist = text.split('\n');
	QVector<QString> names;

	for (QString s : slist)
		names << s;
	return names;
}

bool SingleDomainSettings::isVertical()
{
	return vertical;
}

void SingleDomainSettings::saveConfig(pugi::xml_node node)
{
	node.append_attribute("vertical") = vertical;
	for (auto i : getNames())
		node.append_child("label").text().set(static_cast<const char*>(i.toLocal8Bit()));
}

void SingleDomainSettings::readConfig(pugi::xml_node node)
{
	vertical = node.attribute("vertical").as_bool();
	text.clear();
	for (auto i : node.children("label"))
		text += QString::fromLocal8Bit(i.text().as_string()) + "\n";
	text = text.trimmed();
	on_buttonBox_rejected();
}
