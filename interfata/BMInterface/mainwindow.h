#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "classinterfaces.h"
#include <pugixml.hpp>

class QCustomPlot;
class SettingsForm;
class ADSConfig;

class DataSource;
class CircularBuffer;
class Interpreter;
class GraphManager;
class PluginManager;
class QSplitter;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    friend class SettingsForm;

public:
    static MainWindow* getInstance();
    static void destroyInstance();

    void notifyStop();

    void save(pugi::xml_node node);
    void load(pugi::xml_node node);

private slots:
    void run();
    void statusFunc();
    void on_timeRangeHorizontalSlider_valueChanged(int value);
    void on_actionStart_triggered();
    void on_refreshRateHorizontalSlider_valueChanged(int value);
    void on_actionSettings_triggered();
    void on_actionADS_Configuration_triggered();
    void on_densityHorizontalSlider_valueChanged(int value);
    void stopGuiSlot(bool error = false);

signals:
    void stopGuiSig(bool error);

private:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void addGraphs();
    void setSplitter(QSplitter* splitter);

    static MainWindow* instance;
    Ui::MainWindow *ui;
    QTimer *timer, *statusTimer;

    DataSource* client;
    CircularBuffer* cbuffer;
    Interpreter* interp;
    std::atomic<bool> stoppable;
};

#endif // MAINWINDOW_H
