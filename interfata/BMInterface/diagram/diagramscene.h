#ifndef DIAGRAMSCENE_H
#define DIAGRAMSCENE_H

#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QGraphicsView>
#include <memory>

#include <pugixml.hpp>

class QGraphicsLineItem;
class DataLine;
class Chip;

class DiagramScene : public QGraphicsScene
{
    Q_OBJECT

public:
    explicit DiagramScene(QObject* parent);
    explicit DiagramScene(const DiagramScene& dscene);

    void setLine(const std::shared_ptr<DataLine>& line);
    void setLine(bool persist = true);
    std::shared_ptr<DataLine> getLine();

    void addChip(Chip* chip);
    Chip* findChip(const QString& name);
    void selectedChip(Chip* chip);

    void registerDataNodes();
    std::map<std::string, QWidget*> getWidgets();

    void save(pugi::xml_node node);
    void load(pugi::xml_node node);

protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent);
    void keyPressEvent(QKeyEvent* keyEvent);

private:
    std::shared_ptr<DataLine> line;
    std::vector<Chip*> chips;
    Chip* selected = nullptr;
    QPointF scenePos;
};

#endif // DIAGRAMSCENE_H
