#include "domainsettings.h"
#include "ui_domainsettings.h"

using namespace std;

DomainSettings::DomainSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DomainSettings)
{
    ui->setupUi(this);
    freqs = make_pair(0, 2000);
    stype = QCPAxis::stLinear;
}

DomainSettings::~DomainSettings()
{
    delete ui;
}

void DomainSettings::on_buttonBox_accepted()
{
    freqs = make_pair(ui->lowerSpin->value(), ui->upperSpin->value());
    stype = ui->scaleBox->currentText() == "Linear" ? QCPAxis::stLinear : QCPAxis::stLogarithmic;
    close();
}

void DomainSettings::on_buttonBox_rejected()
{
    ui->lowerSpin->setValue(freqs.first);
    ui->upperSpin->setValue(freqs.second);
    ui->scaleBox->setCurrentIndex(stype == QCPAxis::stLinear ? 0 : 1);
    close();
}

pair<float, float> DomainSettings::getFreq()
{
    return freqs;
}

QCPAxis::ScaleType DomainSettings::getScale()
{
    return stype;
}

void DomainSettings::saveConfig(pugi::xml_node node)
{
	node.append_attribute("lowerFreq") = freqs.first;
	node.append_attribute("upperFreq") = freqs.second;
	node.append_attribute("scale") = stype == QCPAxis::stLinear ? "linear" : "log";
}

void DomainSettings::readConfig(pugi::xml_node node)
{
	freqs.first = node.attribute("lowerFreq").as_float();
	freqs.second = node.attribute("upperFreq").as_float();
	stype = string(node.attribute("scale").value()) == "linear" ? QCPAxis::stLinear : QCPAxis::stLogarithmic;
}
