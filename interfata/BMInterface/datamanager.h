#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <string>
#include <vector>
#include <utility>
#include <thread>

#include <signalbuffer.h>
#include <usefulfunctions.h>
#include "classinterfaces.h"

class ParsedExpr;
class SignalGraph;
class DataNode;
class DataWatchdog;
class IPluginGraph;

class DataEntry : public SignalBuffer
{
public:
    DataEntry(const std::string& name, Domain domain = Time);
    DataEntry(const DataEntry& dentry);
    virtual ~DataEntry();

    void add(double x, double y);
    void endWrite();
    void addDataNode(DataNode* dn);

private:
    std::shared_ptr<std::vector<DataNode*>> nodes;
};

class DataManager;
class DataNode : public ThreadTask
{
public:
    DataNode(std::shared_ptr<IPluginGraph> plugin);
    ~DataNode();

    void notify();
    void start();
    void stop();

    void completeInputs(DataEntry& dentry);
    DataEntry& getOut(bool side, int idx);

private:
    void run();
    int getWritten();

    std::vector<std::unique_ptr<DataEntry>> inputs, outputs;
    std::shared_ptr<IPluginGraph> plugin;
    std::map<std::string, std::string> inputNames;

    std::mutex mut;
    std::condition_variable cvar;
    bool written;
};

class DataManagerInterface
{
public:
    virtual void addData(sample* smp, int len) = 0;
    virtual void notifyStop() = 0;

protected:
    virtual ~DataManagerInterface() {}
};

class DataManager : public DataManagerInterface
{
public:
    static DataManager* getInstance();
    static void destroyInstance();

    static void formReplacement();
    static void commitReplacement(bool ok);

    void setWatchDogInterval(unsigned int interval);
    void addNode(DataNode* dn);

    void addWidget(const std::string& name, QWidget* widget);
    void removeWidget(const std::string& name);
    std::map<std::string, QWidget*> getWidgets();

	void markPluginUsed(const QString& plugin);
	bool isPluginUsed(const QString& plugin);

    void addData(sample* smp, int len);
    void notifyStop();

    void start();
    void stop();

private:
    DataManager();
    ~DataManager();

    static DataManager* instance, *oldInstance;
	std::map<QString, bool> usedPlugins;

    unsigned int channelCount;
    std::vector<std::unique_ptr<DataNode>> dnodes;
    std::map<std::string, QWidget*> widgets;
    std::unique_ptr<DataWatchdog> wdog;
};

#endif // DATAMANAGER_H

