#ifndef GUIS_H
#define GUIS_H

#include <QDialog>
#include <utility>
#include <string>

class ADSPlugin;
class QLineEdit;
class QPushButton;

class InputSettingsGui : public QDialog
{
	Q_OBJECT

public:
	InputSettingsGui();
	~InputSettingsGui();

	std::pair<std::string, int> getCon();
	float getVref();

	void setCon(std::pair<std::string, int> con);
	void setVref(float vreff);
	void setGuiParent(QWidget* parent);

private slots:
	void okButtonClicked();

private:
	std::string ips;
	int porti;
	float vreff;

	QLineEdit *ip, *port, *vref;
	QPushButton* okButton;
};

#endif
