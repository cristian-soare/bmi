#include "adsconfig.h"
#include "ui_adsconfig.h"

#include "interpreter.h"

#include <QPushButton>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QMessageBox>
#include <QTimer>
#include <QLabel>
#include <fstream>

using namespace std;

/////////////////////////// BitButton //////////////////////////////////

BitButton::BitButton(QString text, QBoxLayout* parent)
{
    recordedValue = "0";
    defaultColor = "transparent;"; changedColor = "red;";
    style = "border: 1px solid black; background-color: ";
    this->parent = parent;
    int butHeight = 23;

    nameBut = new QPushButton(text);
    nameBut->setFlat(true);
    nameBut->setStyleSheet(style + defaultColor);
    nameBut->setMinimumHeight(butHeight);
    connect(nameBut, SIGNAL(clicked()), this, SLOT(on_click()));

    valueBut = new QPushButton("0");
    valueBut->setFlat(true);
    valueBut->setStyleSheet(style + defaultColor);
    valueBut->setMinimumHeight(butHeight);
    connect(valueBut, SIGNAL(clicked()), this, SLOT(on_click()));

    setSpacing(0);
    addWidget(nameBut);
    addWidget(valueBut);

    parent->addLayout(this);
}

BitButton::~BitButton()
{
    removeWidget(nameBut); delete nameBut;
    removeWidget(valueBut); delete valueBut;
    parent->removeItem(this);
}

void BitButton::on_click()
{
    if (valueBut->text() == "0")
        valueBut->setText("1");
    else
        valueBut->setText("0");

    if (valueBut->text() != recordedValue)
        valueBut->setStyleSheet(style + changedColor);
    else
        valueBut->setStyleSheet(style + defaultColor);
    nameBut->setStyleSheet(valueBut->styleSheet());
}

byte BitButton::getBit(int shift)
{
    valueBut->setStyleSheet(style + defaultColor);
    nameBut->setStyleSheet(style + defaultColor);
    if (valueBut->text() == "1")
        return 1 << shift;
    return 0;
}

void BitButton::setBit(byte value)
{
    valueBut->setStyleSheet(style + defaultColor);
    nameBut->setStyleSheet(style + defaultColor);
    if (value)
        valueBut->setText("1");
    else
        valueBut->setText("0");
}

void BitButton::saveValue()
{
    nameBut->setStyleSheet(style);
    valueBut->setStyleSheet(style);
}

//////////////////////// ADS Register ////////////////////////////////////////////

const char* RegisterADS::warnings[2] = {
    "Communication with the device must be active for registers to be read",
    "Communication with the device must be active for registers to be set"
};

RegisterADS::RegisterADS(byte adr, QBoxLayout* parent, Interpreter* interp)
    : interp(interp)
    , address(adr)
    , parent(parent)
{
    box = new QHBoxLayout();
    box->setSpacing(0);

    static const QString regNames[] = {
        "CONFIG0", "CONFIG1", "MUXSCH", "MUXDIF", "MUXSG0",
        "MUXSG1", "SYSRED", "GPIOC", "GPIOD", "ID" };

    static const QString bitnames[10][8] = {
        { "0", "SPIRST", "MUXMOD", "BYPAS", "CLKENB", "CHOP", "STAT", "0" },
        { "IDLMOD", "DLY2", "DLY1", "DLY0", "SBCS1", "SBCS0", "DRATE1", "DRATE0" },
        { "AINP3", "AINP2", "AINP1", "AINP0", "AINN3", "AINN2", "AINN1", "AINN0" },
        { "DIFF7", "DIFF6", "DIFF5", "DIFF4", "DIFF3", "DIFF2", "DIFF1", "DIFF0" },
        { "AIN7", "AIN6", "AIN5", "AIN4", "AIN3", "AIN2", "AIN1", "AIN0" },
        { "AIN15", "AIN14", "AIN13", "AIN12", "AIN11", "AIN10", "AIN9", "AIN8" },
        { "0", "0", "REF", "GAIN", "TEMP", "VCC", "0", "OFFSET" },
        { "CIO7", "CIO6", "CIO5", "CIO4", "CIO3", "CIO2", "CIO1", "CIO0" },
        { "DIO7", "DIO6", "DIO5", "DIO4", "DIO3", "DIO2", "DIO1", "DIO0" },
        { "ID7", "ID6", "ID5", "ID4", "ID3", "ID2", "ID1", "ID0" }
    };

    buttons = new BitButton*[8];
    for (int i = 0; i < 8; i++)
        buttons[i] = new BitButton(bitnames[adr][i], box);

    controlLayout = new QVBoxLayout(); controlLayout->setSpacing(2);

    getButton = new QPushButton("Get"); controlLayout->addWidget(getButton);
    connect(getButton, SIGNAL(clicked()), this, SLOT(on_get_clicked()));

    setButton = new QPushButton("Set"); controlLayout->addWidget(setButton);
    connect(setButton, SIGNAL(clicked()), this, SLOT(on_set_clicked()));

    spacers.push_back(new QSpacerItem(20, 10));
    box->addItem(spacers[0]);
    box->addLayout(controlLayout);

    setLayout(box);
    setTitle(regNames[adr]);

    parent->addWidget(this);
}

RegisterADS::~RegisterADS()
{
    parent->removeWidget(this);
    for (int i = 0; i < 8; i++)
        delete buttons[i];
    delete[] buttons;

    for (unsigned int i = 0; i < spacers.size(); i++) {
        box->removeItem(spacers[i]);
        delete spacers[i];
    }

    controlLayout->removeWidget(getButton); delete getButton;
    controlLayout->removeWidget(setButton); delete setButton;
    box->removeItem(controlLayout); delete controlLayout;

    delete box;
}

void RegisterADS::on_get_clicked()
{
    try {
        getRegister();
    } catch (int e) {
        QMessageBox::warning(this, "Error", warnings[e]);
    }
}

byte RegisterADS::getRegister()
{
    if (!interp->isRunning())
        throw 0;
    byte val = interp->readRegister(address);
    for (int i = 0; i < 8; i++)
        buttons[7 - i]->setBit(val & (1 << i));
    return val;
}

void RegisterADS::on_set_clicked()
{
    byte val = 0;
    for (int i = 0; i < 8; i++)
        val |= buttons[i]->getBit(7 - i);

    try {
        setRegister(val, false);
    } catch (int e) {
        QMessageBox::warning(this, "Error", warnings[e]);
    }
}

void RegisterADS::setRegister(byte val, bool outside)
{
    if (!interp->isRunning())
        throw 1;
    if (outside)
        for (int i = 0; i < 8; i++)
            buttons[7 - i]->setBit(val & (1 << i));
    interp->writeRegister(address, val);
}

char* RegisterADS::getRegisterString()
{
    static char s[9];
    for (int i = 0; i < 8; i++)
        s[i] = buttons[i]->getBit(0) + '0';
    s[8] = 0;
    return s;
}

void RegisterADS::setRegisterString(char* s)
{
    for (int i = 0; i < 8; i++)
        buttons[i]->setBit(s[i] - '0');
}

///////////////// LEDS ///////////////////////////////

class ChannelLED : public QVBoxLayout
{
public:
    ChannelLED(QWidget* parent, const string& name)
        : QVBoxLayout(parent)
        , name(name)
    {
        addWidget(img = new QLabel());
        addWidget(txt = new QLabel());

        setState(0);
        update();
    }

    void setState(int state)
    {
        this->state = state;
        if (state == 1)
            ovfcount++;
    }

    void update()
    {
        img->setPixmap(QPixmap(QString(":/res/icons/") + colours[state] + QString(".png")));

        QString stxt = QString::fromStdString(name) + ": " + states[state] + " ";
        if (state == 1)
            stxt += "\n(" + QString::number(ovfcount) + ")";
        txt->setText(stxt);
        ovfcount = 0;
    }

private:
    static const QString colours[], states[];
    const string& name;
    QLabel *img, *txt;
    int state, ovfcount;
};

const QString ChannelLED::colours[] = {"red", "yellow", "green"};
const QString ChannelLED::states[] = {"?", "OVF", "OK"};

//////////////////////////// ADS Configuration Panel ////////////////////

ADSConfig::ADSConfig(QWidget *parent, Interpreter* interp, const vector<string>& regNames)
    : QDialog(parent)
    , ui(new Ui::ADSConfig)
{
    ui->setupUi(this);

    registers = new RegisterADS*[10];
    for (byte i = 0; i < 10; i += 2) {
        hlayouts.push_back(new QHBoxLayout());
        registers[i] = new RegisterADS(i, hlayouts[i / 2], interp);
        registers[i + 1] = new RegisterADS(i + 1, hlayouts[i / 2], interp);
        ui->t1vLayout->addLayout(hlayouts[i / 2]);
    }

    if (interp->isRunning())
        for (int i = 0; i < 10; i++)
            registers[i]->getRegister();

    for (unsigned int i = 0; i < 3; i++)
        for (unsigned int j = 0; j < 8; j++) {
            channelLEDs.push_back(new ChannelLED(NULL, regNames[i * 8 + j]));
            ui->ledGrid->addLayout(channelLEDs.back(), i, j);
        }
    ledTimer = new QTimer();
    ledTimer->setInterval(100);
    connect(ledTimer, SIGNAL(timeout()), this, SLOT(ledExpire()));
}

ADSConfig::~ADSConfig()
{
    for (int i = 0; i < 10; i++)
        delete registers[i];
    delete[] registers;

    for (int i = 0; i < 5; i++) {
        ui->t1vLayout->removeItem(hlayouts[i]);
        delete hlayouts[i];
    }

    delete ui;
}

void ADSConfig::ledExpire()
{
    for (auto i : channelLEDs) {
        i->update();
        i->setState(0);
    }
}

void ADSConfig::switchLeds(bool on)
{
    if (on) {
        ledTimer->start();
    } else {
        ledTimer->stop();

        for (auto i : channelLEDs) {
            i->setState(0);
            i->update();
        }
    }
}

void ADSConfig::markLed(int idx, bool ovf)
{
    if (idx >= 24)
        return;
    channelLEDs[idx]->setState(ovf ? 1 : 2);
}

void ADSConfig::changeRegister(byte addr, byte mod, byte ok)
{
    byte val;
    try { val = registers[addr]->getRegister(); } catch (int e) {
        QMessageBox::warning(this, "Error", RegisterADS::warnings[e]);
        throw e;
    }

    val &= ~mod;
    val |= ok;

    try { registers[addr]->setRegister(val); } catch (int e) {
        QMessageBox::warning(this, "Error", RegisterADS::warnings[e]);
        throw e;
    }
}
