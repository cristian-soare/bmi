#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <thread>

typedef unsigned char byte;

class UDPClient;
class DataManagerInterface;
class ADSConfig;

class Interpreter
{
private:
	void run();

	bool running;
	std::thread thid;
    UDPClient* client;
    DataManagerInterface* dest;
    ADSConfig* conf;
	int count;
	float vref;

    static const int dataLen = 8;
	static const int propLen = 4;

	static const byte READ = 0xA1;
	static const byte WRITE = 0xA2;

public:
    Interpreter(UDPClient* client, DataManagerInterface* grp);
	~Interpreter();

    void init(DataManagerInterface* dmanager);
    void init(float vref);
    void init(ADSConfig* conf);

	void start();
    void stop();
    bool isRunning();
	int getCount();

	byte readRegister(byte add);
	void writeRegister(byte add, byte val);
};

#endif
