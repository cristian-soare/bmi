QT += core gui widgets printsupport

TARGET = ads1258
TEMPLATE = lib

LIBS += -lpugixml
SOURCES += adsplugin.cpp udpclient.cpp interpreter.cpp guis.cpp adsconfig.cpp
HEADERS += guis.h adsconfig.h
FORMS += adsconfig.ui

QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast

RESOURCES += \
    iconsRes.qrc
