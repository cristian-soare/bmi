#ifndef ADSCONFIG_H
#define ADSCONFIG_H

#include <QDialog>
#include <QVBoxLayout>
#include <QGroupBox>
#include <vector>


class QHBoxLayout;
class QBoxLayout;
class Interpreter;

typedef unsigned char byte;

namespace Ui {
class ADSConfig;
}

//////////////////////// BitButton //////////////////////////////

class BitButton : public QVBoxLayout
{
    Q_OBJECT

public:
    BitButton(QString text, QBoxLayout* parent);
    ~BitButton();

    byte getBit(int shift);
    void setBit(byte val);
    void saveValue();

private slots:
    void on_click();

private:
    QString defaultColor, changedColor, style;
    QString recordedValue;
    QPushButton* nameBut;
    QPushButton* valueBut;
    QBoxLayout* parent;
};

//////////////////////// ADS Register ////////////////////////////////////////

class RegisterADS : public QGroupBox
{
    Q_OBJECT

public:
    RegisterADS(byte adr, QBoxLayout* parent, Interpreter* interp);
    ~RegisterADS();

    char* getRegisterString();
    void setRegisterString(char* s);
    byte getRegister();
    void setRegister(byte val, bool outside = true);

    static const byte CONFIG0 = 0x00;
    static const byte CONFIG1 = 0x01;
    static const byte MUXSCH = 0x02;
    static const byte MUXDIF = 0x03;
    static const byte MUXSG0 = 0x04;
    static const byte MUXSG1 = 0x05;
    static const byte SYSRED = 0x06;
    static const byte GPIOC = 0x07;
    static const byte GPIOD = 0x08;
    static const byte ID = 0x09;

    static const char* warnings[2];

private slots:
    void on_get_clicked();
    void on_set_clicked();

private:
    BitButton** buttons;
    QPushButton* getButton;
    QPushButton* setButton;

    Interpreter* interp;
    byte address;

    QBoxLayout* parent;
    std::vector<QSpacerItem*> spacers;
    QVBoxLayout* controlLayout;
    QHBoxLayout* box;
};

/////////////////////////////////// ADS Configuration Panel ///////////////////////////////////
class ChannelLED;

class ADSConfig : public QDialog
{
    Q_OBJECT

public:
    explicit ADSConfig(QWidget *parent, Interpreter* interp, const std::vector<std::string>& regNames);
    ~ADSConfig();

    void setConfigFile(std::string path);
    void switchLeds(bool on);
    void markLed(int idx, bool ovf);

private slots:
    void ledExpire();

private:
    Ui::ADSConfig *ui;
    RegisterADS** registers;
    std::vector<QHBoxLayout*> hlayouts;

    std::vector<ChannelLED*> channelLEDs;
    QTimer* ledTimer;

    void changeRegister(byte addr, byte mod, byte ok);
};

#endif // ADSCONFIG_H
