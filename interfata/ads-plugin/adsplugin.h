#ifndef ADSPLUGIN_H
#define ADSPLUGIN_H

#include <vector>
#include <string>
#include <pugixml.hpp>
#include <QWidget>

class UDPClient;
class Interpreter;
class InputSettingsGui;
class ADSConfig;
class ChannelLEDs;

struct sample
{
    int channel;
    double x, y;
};

class DataManagerInterface
{
public:
    virtual void addData(sample* smp, int len) = 0;
    virtual void notifyStop() = 0;

protected:
	virtual ~DataManagerInterface() {}
};

class InputPluginInterface
{
public:
    virtual bool start() = 0;
    virtual void stop() = 0;

    virtual void setDataManager(DataManagerInterface* dmanager) = 0;
    virtual void setParents(QWidget* settings, QWidget* control) { (void)settings; (void)control; }

    virtual void showSettings() {}
    virtual void showConfig() {}

    virtual unsigned int getByteCount() { return 0; }
    virtual float getUsage() { return 0; }
    virtual const std::vector<std::string>& getRegNames() = 0;

    virtual void save(pugi::xml_node node) { (void)node; }
    virtual void load(pugi::xml_node node) { (void)node; }

protected:
    virtual ~InputPluginInterface() {}
};

class ADSPlugin : public InputPluginInterface
{
    friend class InputSettingsGui;

public:
	ADSPlugin();
	~ADSPlugin();

	bool start();
	void stop();

    void setDataManager(DataManagerInterface* dmanager);
    void setParents(QWidget* settings, QWidget* control);

    void showSettings();
    void showConfig();

    unsigned int getByteCount();
    float getUsage();
    const std::vector<std::string>& getRegNames();

    void save(pugi::xml_node node);
    void load(pugi::xml_node node);

private:
	UDPClient* client;
	Interpreter* interp;

    ADSConfig* conf;
    InputSettingsGui* settingsGui;

    std::vector<std::string> regNames;
};

#endif
