#include "interpreter.h"

#include "udpclient.h"
#include "adsplugin.h"

#include <unistd.h>
#include <fstream>
#include <iostream>
#include <adsconfig.h>

using namespace std;

Interpreter::Interpreter(UDPClient* client, DataManagerInterface* dg)
    : running(false)
    , client(client)
    , dest(dg)
    , count(0)
{

}

Interpreter::~Interpreter()
{
    stop();
	delete client;
}

void Interpreter::init(DataManagerInterface* dmanager)
{
    this->dest = dmanager;
}

void Interpreter::init(float vref)
{
    this->vref = vref;
}

void Interpreter::init(ADSConfig* conf)
{
    this->conf = conf;
}

void Interpreter::start()
{
    cout << "intrat" << endl;
	if (running)
        return;
    if (thid.joinable())
        thid.join();

    cout << "starting" << endl;
	running = true;

	uint8_t temp[] = { 0xA3, 0 };
	client->send(temp, 2);
	thid = thread(&Interpreter::run, this);
}

bool Interpreter::isRunning()
{
    return running;
}

void Interpreter::run()
{
    static sample smp[125000];
    uint8_t buf[1000000];
    ofstream f("log.csv");

	while (running) {
        int nb = client->recv(buf, 1000000);
        if (nb <= 0)
            continue;

        int idx = 0;
        for (int i = 0; i < nb; i += dataLen) {
            smp[idx].channel = buf[i] & 0b00011111;
            if (smp[idx].channel >= 29)
                continue;

            conf->markLed(smp[idx].channel, buf[i] & 0x40);

            uint micros = *(uint*)(buf + i + 4);
            smp[idx].x = (double)micros / 1000000.0F;

            int val = 0;
            if (buf[i + 1] & 0x80)
                val |= 0xff << 24;
            val |= buf[i + 1] << 16;
            val |= buf[i + 2] << 8;
            val |= buf[i + 3];
            smp[idx].y = 1.06 * vref * val / ((1 << 23) - 1);

            f.precision(10);
            f << smp[idx].channel << ',' << smp[idx].x << ',' << smp[idx].y << endl;
            ++idx;
        }

        dest->addData(smp, idx);
		count += nb;
	}
}

void Interpreter::stop()
{
	running = false;
    if (thid.joinable())
        thid.join();
}

byte Interpreter::readRegister(byte add)
{
	byte cmd[2000] = { READ, add, 0 };
	bool wasRunning = running;

    stop();
	client->send(cmd, 3);
    while (client->recv(cmd, 2000) >= 12) {}

	byte val = cmd[1];
	if (wasRunning)
        start();
	return val;
}

void Interpreter::writeRegister(byte add, byte val)
{
	byte cmd[] = { WRITE, add, val };
	client->send(cmd, 3);
}

int Interpreter::getCount()
{
	return count;
}
