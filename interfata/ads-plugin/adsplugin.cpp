#include "adsplugin.h"

#include "udpclient.h"
#include "interpreter.h"
#include "adsconfig.h"
#include "guis.h"

#include <iostream>

using namespace std;

static ADSPlugin* globalPlugin;

extern "C" InputPluginInterface* initPlugin()
{
	if (!globalPlugin)
		globalPlugin = new ADSPlugin();
	return globalPlugin;
}

extern "C" void destroyPlugin()
{
	delete globalPlugin;
	globalPlugin = 0;
}

ADSPlugin::ADSPlugin()
{
	client = new UDPClient(300000);
	interp = new Interpreter(client, NULL);
	settingsGui = new InputSettingsGui();

	string tregs[] = {
	   "DIFF0", "DIFF1", "DIFF2", "DIFF3", "DIFF4", "DIFF5", "DIFF6", "DIFF7",

	   "AIN0", "AIN1", "AIN2", "AIN3", "AIN4", "AIN5", "AIN6", "AIN7",
	   "AIN8", "AIN9", "AIN10", "AIN11", "AIN12", "AIN13", "AIN14", "AIN15",

	   "OFFSET", "VCC", "TEMP", "GAIN", "REF"
	};
	regNames = vector<string>(tregs, tregs + 29);
}

ADSPlugin::~ADSPlugin()
{
	delete interp;
	delete settingsGui;
}

bool ADSPlugin::start()
{
	client->init(settingsGui->getCon().first.c_str(), settingsGui->getCon().second);
	interp->init(settingsGui->getVref());

	interp->start();
	conf->switchLeds(true);
	return true;
}

void ADSPlugin::stop()
{
	interp->stop();
	conf->switchLeds(false);
}

void ADSPlugin::setDataManager(DataManagerInterface* dmanager)
{
	interp->init(dmanager);
}

void ADSPlugin::setParents(QWidget* settings, QWidget* control)
{
	conf = new ADSConfig(control, interp, regNames);
	interp->init(conf);
}

void ADSPlugin::showSettings()
{
	settingsGui->show();
}

void ADSPlugin::showConfig()
{
	conf->show();
}

unsigned int ADSPlugin::getByteCount()
{
	return interp->getCount();
}

float ADSPlugin::getUsage()
{
	return 0;
}

const std::vector<std::string>& ADSPlugin::getRegNames()
{
	return regNames;
}

void ADSPlugin::save(pugi::xml_node node)
{
	pair<string, int> con = settingsGui->getCon();
	node.append_attribute("IP") = con.first.c_str();
	node.append_attribute("port") = settingsGui->getCon().second;
	node.append_attribute("vref") = settingsGui->getVref();
}

void ADSPlugin::load(pugi::xml_node node)
{
	settingsGui->setCon(make_pair(node.attribute("IP").value(), node.attribute("port").as_int()));
	settingsGui->setVref(node.attribute("vref").as_float());
}
