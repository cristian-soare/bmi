#ifndef UDPCLIENT_H
#define UDPCLIENT_H

#include <arpa/inet.h>
#include <string>
#include <cstdint>

class UDPClient
{
public:
	UDPClient(int timeout = 0);
	UDPClient(const std::string& ip, const int port, int timeout = 0);
	~UDPClient();

	void init(const std::string& ip, const int port);
	int send(const uint8_t* buf, int len);
	int recv(uint8_t* buf, int len);

private:
	void init(int timeout);

	int sockfd;
	socklen_t slen;
	sockaddr_in servaddr;
};

#endif
