#include "guis.h"

#include <QGridLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QLabel>
#include <QVBoxLayout>
#include <QPixmap>

using namespace std;

InputSettingsGui::InputSettingsGui()
{
	QGridLayout* layout = new QGridLayout();
	setLayout(layout);

	layout->addWidget(new QLabel("IP: "), 0, 0);
	layout->addWidget(ip = new QLineEdit(), 0, 1);

	layout->addWidget(new QLabel("Port: "), 1, 0);
	layout->addWidget(port = new QLineEdit(), 1, 1);

	layout->addWidget(new QLabel("Vref: "), 2, 0);
	layout->addWidget(vref = new QLineEdit(), 2, 1);

	layout->addWidget(okButton = new QPushButton("OK"), 4, 2);
	connect(okButton, SIGNAL(clicked()), this, SLOT(okButtonClicked()));
}

InputSettingsGui::~InputSettingsGui()
{

}

void InputSettingsGui::okButtonClicked()
{
	ips = ip->text().toStdString();
	porti = port->text().toInt();
	vreff = vref->text().toFloat();
	close();
}

pair<string, int> InputSettingsGui::getCon()
{
	return make_pair(ips, porti);
}

float InputSettingsGui::getVref()
{
	return vreff;
}

void InputSettingsGui::setCon(pair<string, int> con)
{
	ips = con.first;
	porti = con.second;

	ip->setText(QString::fromStdString(ips));
	port->setText(QString::number(porti));
}

void InputSettingsGui::setVref(float vreff)
{
	this->vreff = vreff;
	vref->setText(QString::number(vreff));
}

