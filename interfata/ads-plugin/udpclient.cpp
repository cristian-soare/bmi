#include "udpclient.h"

#include <sys/socket.h>
#include <cstring>
#include <unistd.h>

using namespace std;

UDPClient::UDPClient(int timeout)
{
	init(timeout);
}

UDPClient::UDPClient(const string& ip, const int port, int timeout)
{
	init(timeout);
	init(ip, port);
}

UDPClient::~UDPClient()
{
	close(sockfd);
}

void UDPClient::init(int timeout)
{
	slen = sizeof(servaddr);
	sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (timeout) {
		timeval tm;
		tm.tv_sec = 0;
		tm.tv_usec = timeout * 1000;
		setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tm, sizeof(tm));
	}
}

void UDPClient::init(const string& ip, const int port)
{
	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(port);
	inet_aton(ip.c_str(), &servaddr.sin_addr);
}

int UDPClient::send(const uint8_t* buf, int len)
{
	return sendto(sockfd, buf, len, 0, (struct sockaddr*)&servaddr, slen);
}

int UDPClient::recv(uint8_t* buf, int len)
{
	return recvfrom(sockfd, buf, len, 0, (struct sockaddr*)&servaddr, &slen);
}
