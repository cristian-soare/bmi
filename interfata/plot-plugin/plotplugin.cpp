#include <plotplugin.h>

#include <plotsettings.h>

#include <QMessageBox>
#include <QHBoxLayout>
#include <QLabel>
#include <qcustomplot.h>
#include <graphs.h>

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <cstring>

using namespace std;

ReplotTimer::ReplotTimer(SignalPlot *plot, mutex &mut)
    : plot(plot)
    , mut(mut)
{
    setInterval(33);
}

void ReplotTimer::timerEvent(QTimerEvent *event)
{
    (void)event;
    lock_guard<mutex> lock(mut);
    plot->replot();
}

//////////////////////////////// FFTPlugin ///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

PlotGraph::PlotGraph(PlotPlugin* parent)
	: parent(parent)
    , gui(make_unique<PlotSettings>(static_cast<QWidget*>(parent->parent())))
{

}

PlotGraph::~PlotGraph()
{

}

void PlotGraph::show()
{
    if (gui->exec() == QDialog::Accepted)
        update();
}

void PlotGraph::update()
{
    buffers.clear();
    for (const auto& i : gui->getGraphs())
        buffers[i.first] = nullptr;

    switch (gui->getDomain()) {
    case SignalBuffer::Time: plot = std::make_unique<TimePlot>(); break;
    case SignalBuffer::Frequency: plot = std::make_unique<FrequencyPlot>(); break;
//    case SignalBuffer::Single: plot = std::make_unique<BarPlot>(); break;
    default: break;
    }

    for (const auto& i : gui->getGraphs()) {
        switch (gui->getDomain()) {
        case SignalBuffer::Time:
        case SignalBuffer::Frequency:
            plot->addPlottable(new SignalGraph(i.second, plot->xAxis, plot->yAxis, QString::fromStdString(i.first), gui->getDomain(), buffers[i.first]));
            break;

        default:
            break;
        }
    }
}

void PlotGraph::reset()
{

}

void PlotGraph::start()
{
    timer = new ReplotTimer(plot.get(), mut);
    timer->start();
}

void PlotGraph::stop()
{
    timer->stop();
    delete timer;
}

void PlotGraph::run()
{
    mut.lock();
    for (int i = 0; i < plot->graphCount(); i++)
        dynamic_cast<SignalPlottable*>(plot->graph(i))->run();
    mut.unlock();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

pair<vector<pair<string, SignalBuffer::Domain>>, vector<pair<string, SignalBuffer::Domain>>> PlotGraph::getIOProp()
{
    vector<pair<string, SignalBuffer::Domain>> ins, outs;
    for (auto& i : buffers)
        ins.push_back(make_pair(i.first, domain));
    return make_pair(ins, outs);
}

SignalBuffer*& PlotGraph::operator[](const std::string& s)
{
    return buffers[s];
}

vector<pair<string, QWidget*>> PlotGraph::getWidgets()
{
    vector<pair<string, QWidget*>> ret;
    if (plot)
        ret.push_back(make_pair("Plot", plot.get()));
    return ret;
}

static const std::vector<std::string> domainNames {
    "Time", "Frequency", "Single"
};

void PlotGraph::save(pugi::xml_node node)
{
    node.append_attribute("domain") = domainNames[gui->getDomain()].c_str();

    for (const auto& i : gui->getGraphs()) {
        auto gchild = node.append_child("Graph");
        gchild.append_attribute("name") = i.first.c_str();
        gchild.append_attribute("colour") = i.second.name().toStdString().c_str();
    }
}

void PlotGraph::load(pugi::xml_node node)
{
    auto found = std::find(domainNames.begin(), domainNames.end(), node.attribute("domain").value());
    auto idx = found != domainNames.end() ? found - domainNames.begin() : 0;
    gui->setDomain(static_cast<SignalBuffer::Domain>(idx));

    std::vector<std::pair<std::string, QColor>> graphs;
    for (pugi::xml_node gchild = node.child("Graph"); gchild; gchild = gchild.next_sibling("Graph"))
        graphs.push_back(std::make_pair(gchild.attribute("name").value(), gchild.attribute("colour").value()));
    gui->setGraphs(graphs);

    update();
}

PlotPlugin* globalPlugin;

extern "C" PlotPlugin* initPlugin(QWidget* parent)
{
	if (!globalPlugin)
		globalPlugin = new PlotPlugin(parent);
	return globalPlugin;
}

extern "C" void destroyPlugin()
{
	delete globalPlugin;
	globalPlugin = nullptr;
}

PlotPlugin::PlotPlugin(QWidget* parent)
    : IPlugin(parent) {}

QString PlotPlugin::getName()
{
    return "Signal plot";
}

IPluginGraph* PlotPlugin::initGraphPlugin()
{
    return new PlotGraph(this);
}
