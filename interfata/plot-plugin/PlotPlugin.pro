QT += core gui widgets printsupport

TARGET = plot
TEMPLATE = lib
CONFIG += debug

INCLUDEPATH += ../commons

LIBS += -lqcustomplot -lpugixml
SOURCES += plotplugin.cpp \
    plotsettings.cpp \
    graphs.cpp
HEADERS += plotplugin.h ../commons/generalbuffer.h ../commons/signalbuffer.h \
    plotsettings.h \
    graphs.h
FORMS += \ 
    plotsettings.ui

QMAKE_CXXFLAGS += -std=c++14
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast
