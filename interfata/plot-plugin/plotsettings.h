#ifndef PLOTSETTINGS_H
#define PLOTSETTINGS_H

#include <QDialog>

#include <signalbuffer.h>

namespace Ui {
class PlotSettings;
}

class PlotSettings : public QDialog
{
    Q_OBJECT

public:
    explicit PlotSettings(QWidget *parent = 0);
    ~PlotSettings();

    std::vector<std::pair<std::string, QColor>> getGraphs() const;
    SignalBuffer::Domain getDomain() const;
    void setGraphs(const std::vector<std::pair<std::string, QColor>>& graphs);
    void setDomain(const SignalBuffer::Domain& domain);

private slots:
    void on_addButton_clicked();
    void on_removeButton_clicked();
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::PlotSettings *ui;
    static std::vector<QColor> colourList;
    std::vector<std::pair<std::string, QColor>> graphs;
};

#endif // PLOTSETTINGS_H
