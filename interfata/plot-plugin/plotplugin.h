#ifndef FFTPLUGIN_H
#define FFTPLUGIN_H

#include <classinterfaces.h>

#include <memory>
#include <thread>
#include <QTimer>

class QDialog;
class QWidget;
class QLabel;
class QHBoxLayout;
template <typename T> class CircularBuffer;

class FFTGraphPlugin;
class PlotSettings;
class FFTPlugin;
class SignalPlot;

////////////////////// FFTGraphPlugin ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

class PlotPlugin;

class ReplotTimer : public QTimer
{
public:
    ReplotTimer(SignalPlot* plot, std::mutex& mut);

protected:
    void timerEvent(QTimerEvent* event);

    SignalPlot* plot;
    std::mutex& mut;
};

class PlotGraph : public IPluginGraph
{
public:
    PlotGraph(PlotPlugin* parent);
    ~PlotGraph();

    void show();
    void reset();

    void start();
    void stop();
    void run();

    std::pair<
        std::vector<std::pair<std::string, SignalBuffer::Domain>>,
        std::vector<std::pair<std::string, SignalBuffer::Domain>>> getIOProp();
    SignalBuffer*& operator[](const std::string& s);
    std::vector<std::pair<std::string, QWidget*>> getWidgets();

    void save(pugi::xml_node node);
    void load(pugi::xml_node node);

    PlotPlugin* const parent;

protected:
    void update();

    std::unique_ptr<PlotSettings> gui;
    std::unique_ptr<SignalPlot> plot;

    std::mutex mut;
    ReplotTimer* timer;

    std::map<std::string, SignalBuffer*> buffers;
    SignalBuffer::Domain domain;
};

/////////////////// FFTPlugin ////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

class PlotPlugin : public IPlugin
{
public:
    PlotPlugin(QWidget* widget);

    QString getName();
    IPluginGraph* initGraphPlugin();
};

#endif
