/********************************************************************************
** Form generated from reading UI file 'plot.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLOT_H
#define UI_PLOT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PlotSettingsGui
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpinBox *freqSpin;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *okButton;

    void setupUi(QDialog *PlotSettingsGui)
    {
        if (PlotSettingsGui->objectName().isEmpty())
            PlotSettingsGui->setObjectName(QStringLiteral("PlotSettingsGui"));
        verticalLayout = new QVBoxLayout(PlotSettingsGui);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(PlotSettingsGui);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        freqSpin = new QSpinBox(PlotSettingsGui);
        freqSpin->setObjectName(QStringLiteral("freqSpin"));

        horizontalLayout->addWidget(freqSpin);

        label_2 = new QLabel(PlotSettingsGui);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        okButton = new QPushButton(PlotSettingsGui);
        okButton->setObjectName(QStringLiteral("okButton"));

        horizontalLayout_2->addWidget(okButton);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(PlotSettingsGui);

        QMetaObject::connectSlotsByName(PlotSettingsGui);
    } // setupUi

    void retranslateUi(QDialog *PlotSettingsGui)
    {
        PlotSettingsGui->setWindowTitle(QApplication::translate("PlotSettingsGui", "Dialog", 0));
        label->setText(QApplication::translate("PlotSettingsGui", "Frequency: ", 0));
        label_2->setText(QApplication::translate("PlotSettingsGui", "Hz", 0));
        okButton->setText(QApplication::translate("PlotSettingsGui", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class PlotSettingsGui: public Ui_PlotSettingsGui {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLOT_H
