/********************************************************************************
** Form generated from reading UI file 'graph.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GRAPH_H
#define UI_GRAPH_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_GraphSettingsGui
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpinBox *twindow;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_3;
    QSpinBox *windowShift;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_4;
    QCheckBox *spscb;
    QSpinBox *freqSpin;
    QCheckBox *hancb;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QPushButton *okButton;

    void setupUi(QDialog *GraphSettingsGui)
    {
        if (GraphSettingsGui->objectName().isEmpty())
            GraphSettingsGui->setObjectName(QStringLiteral("GraphSettingsGui"));
        GraphSettingsGui->resize(187, 196);
        verticalLayout = new QVBoxLayout(GraphSettingsGui);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(GraphSettingsGui);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        twindow = new QSpinBox(GraphSettingsGui);
        twindow->setObjectName(QStringLiteral("twindow"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(twindow->sizePolicy().hasHeightForWidth());
        twindow->setSizePolicy(sizePolicy);
        twindow->setMinimum(2);
        twindow->setMaximum(500000);
        twindow->setSingleStep(100);

        horizontalLayout->addWidget(twindow);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_3 = new QLabel(GraphSettingsGui);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_2->addWidget(label_3);

        windowShift = new QSpinBox(GraphSettingsGui);
        windowShift->setObjectName(QStringLiteral("windowShift"));
        sizePolicy.setHeightForWidth(windowShift->sizePolicy().hasHeightForWidth());
        windowShift->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(windowShift);

        label_4 = new QLabel(GraphSettingsGui);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_2->addWidget(label_4);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        spscb = new QCheckBox(GraphSettingsGui);
        spscb->setObjectName(QStringLiteral("spscb"));
        spscb->setChecked(true);

        horizontalLayout_4->addWidget(spscb);

        freqSpin = new QSpinBox(GraphSettingsGui);
        freqSpin->setObjectName(QStringLiteral("freqSpin"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(freqSpin->sizePolicy().hasHeightForWidth());
        freqSpin->setSizePolicy(sizePolicy1);
        freqSpin->setMaximum(200000);

        horizontalLayout_4->addWidget(freqSpin);


        verticalLayout->addLayout(horizontalLayout_4);

        hancb = new QCheckBox(GraphSettingsGui);
        hancb->setObjectName(QStringLiteral("hancb"));

        verticalLayout->addWidget(hancb);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        okButton = new QPushButton(GraphSettingsGui);
        okButton->setObjectName(QStringLiteral("okButton"));

        horizontalLayout_3->addWidget(okButton);


        verticalLayout->addLayout(horizontalLayout_3);


        retranslateUi(GraphSettingsGui);
        QObject::connect(spscb, SIGNAL(toggled(bool)), freqSpin, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(GraphSettingsGui);
    } // setupUi

    void retranslateUi(QDialog *GraphSettingsGui)
    {
        GraphSettingsGui->setWindowTitle(QApplication::translate("GraphSettingsGui", "Dialog", 0));
        label->setText(QApplication::translate("GraphSettingsGui", "Samples:", 0));
        label_3->setText(QApplication::translate("GraphSettingsGui", "Overlap:", 0));
        label_4->setText(QApplication::translate("GraphSettingsGui", "samps", 0));
        spscb->setText(QApplication::translate("GraphSettingsGui", "SPS:", 0));
        hancb->setText(QApplication::translate("GraphSettingsGui", "Use windowing", 0));
        okButton->setText(QApplication::translate("GraphSettingsGui", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class GraphSettingsGui: public Ui_GraphSettingsGui {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GRAPH_H
