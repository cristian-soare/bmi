#ifndef GRAPHS_H
#define GRAPHS_H

#include <qcustomplot.h>
#include <signalbuffer.h>

/////////////////////////// SignalPlottable ////////////////////////////
/////////////////////////////////////////////////////////////////////////

class SignalPlottable
{
public:
    SignalPlottable(SignalBuffer*& dentry);
    virtual ~SignalPlottable() {}

    virtual void run() = 0;
    void setPlotReady();

protected:
    SignalBuffer*& sbuffer;
    bool plotReady;
};

/////////////////////// SignalGraph //////////////////////////////////
//////////////////////////////////////////////////////////////////////

class SignalGraph : public QCPGraph, public SignalPlottable
{
public:
    SignalGraph(QColor color, QCPAxis* x, QCPAxis* y, const QString& name, SignalBuffer::Domain domain, SignalBuffer*& dentry);
    ~SignalGraph();

protected:
    void run();

    float density;

private:
    void timeRun();
    void frequencyRun();
};

/////////////////////// SignalBars ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

class SignalBars : public QCPBars, public SignalPlottable
{
public:
    SignalBars(QString color, QCPAxis* x, QCPAxis* y, SignalBuffer*& dentry);
    ~SignalBars();

    void swapAxis();

private:
    void run();
};

///////////////////////// SignalPlot /////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

class SignalPlot : public QCustomPlot
{
public:
    SignalPlot();
    virtual ~SignalPlot();

    virtual void replot() = 0;
    void wheelEvent(QWheelEvent* event);
};

class TimePlot : public SignalPlot
{
public:
    TimePlot();
    ~TimePlot();

    void replot();
};

class FrequencyPlot : public SignalPlot
{
public:
    FrequencyPlot(QCPAxis::ScaleType stype = QCPAxis::stLinear);
    ~FrequencyPlot();

    void replot();
};

class BarPlot : public SignalPlot
{
public:
    BarPlot(QVector<QString> names, bool vertical = true);
    ~BarPlot();

    void replot();

private:
    float count;
    QCPAxis *channelAxis, *valueAxis;
};

#endif // GRAPHS_H
