#include "plotsettings.h"
#include "ui_plotsettings.h"

#include <QLineEdit>
#include <QComboBox>

PlotSettings::PlotSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlotSettings)
{
    ui->setupUi(this);
}

PlotSettings::~PlotSettings()
{
    delete ui;
}

std::vector<QColor> PlotSettings::colourList = {
    Qt::red, Qt::green, Qt::blue, Qt::yellow, Qt::black
};

void PlotSettings::on_addButton_clicked()
{
    int row = ui->plotsWidget->rowCount();
    ui->plotsWidget->insertRow(row);

    auto name = new QTableWidgetItem();
    ui->plotsWidget->setItem(row, 0, name);

    auto colour = new QComboBox(this);
    int size = colour->style()->pixelMetric(QStyle::PM_SmallIconSize);
    QPixmap pixmap(size, size);
    for (unsigned int i = 0; i < colourList.size(); i++) {
        colour->addItem("", colourList[i]);
        pixmap.fill(colourList[i]);
        colour->setItemData(i, pixmap, Qt::DecorationRole);
    }
    ui->plotsWidget->setCellWidget(row, 1, colour);
}

void PlotSettings::on_removeButton_clicked()
{
    ui->plotsWidget->removeRow(ui->plotsWidget->currentRow());
}

std::vector<std::pair<std::string, QColor>> PlotSettings::getGraphs() const
{
    return graphs;
}

void PlotSettings::setGraphs(const std::vector<std::pair<std::string, QColor>>& graphs)
{
    this->graphs = graphs;
    on_buttonBox_rejected();
}

SignalBuffer::Domain PlotSettings::getDomain() const
{
    return static_cast<SignalBuffer::Domain>(ui->domainCombo->currentIndex());
}

void PlotSettings::setDomain(const SignalBuffer::Domain& domain)
{
    ui->domainCombo->setCurrentIndex(domain);
}

void PlotSettings::on_buttonBox_accepted()
{
    graphs.clear();
    for (int i = 0; i < ui->plotsWidget->rowCount(); i++) {
        auto name = ui->plotsWidget->item(i, 0)->text().toStdString();
        auto colour = colourList[static_cast<QComboBox*>(ui->plotsWidget->cellWidget(i, 1))->currentIndex()];
        graphs.push_back(std::make_pair(name, colour));
    }
}

void PlotSettings::on_buttonBox_rejected()
{
    while (ui->plotsWidget->rowCount())
        ui->plotsWidget->removeRow(0);

    for (const auto& i : graphs) {
        on_addButton_clicked();
        int row = ui->plotsWidget->rowCount() - 1;

        ui->plotsWidget->item(row, 0)->setText(QString::fromStdString(i.first));
        auto colIdx = std::find(colourList.begin(), colourList.end(), i.second) - colourList.begin();
        static_cast<QComboBox*>(ui->plotsWidget->cellWidget(row, 1))->setCurrentIndex(colIdx);
    }
}
