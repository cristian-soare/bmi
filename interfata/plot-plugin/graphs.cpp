#include <graphs.h>

#include <signalbuffer.h>

SignalPlottable::SignalPlottable(SignalBuffer*& dentry)
    : sbuffer(dentry)
    , plotReady(false) {}

void SignalPlottable::setPlotReady()
{
    plotReady = true;
}

SignalGraph::SignalGraph(QColor color, QCPAxis* x, QCPAxis* y, const QString& name, SignalBuffer::Domain domain, SignalBuffer*& dentry)
    : QCPGraph(x, y)
    , SignalPlottable(dentry)
{
    setPen(QPen(color));
    setAntialiasedFill(false);
    setName(name);

    if (domain == SignalBuffer::Frequency)
        setLineStyle(QCPGraph::lsImpulse);
}

SignalGraph::~SignalGraph() {}

void SignalGraph::run()
{
    switch (sbuffer->domain) {
    case SignalBuffer::Time: timeRun(); break;
    case SignalBuffer::Frequency: frequencyRun(); break;
    default: break;
    }
}

void SignalGraph::timeRun()
{
    SignalBuffer& dentry = *sbuffer;
    int size = dentry.beginRead();
    if (!size)
        return;

    double lastx = data()->count() > 0 ? data()->lastKey() : 0;
    for (int i = 0; i < size; i++) {
        if (dentry[i].first - lastx > 0.001) {
            addData(dentry[i].first, dentry[i].second);
            lastx = dentry[i].first;
        } else if (dentry[i].first - lastx < 0) {
            data()->clear();
            lastx = dentry[i].first;
        }
    }
    dentry.endRead(size);
}

void SignalGraph::frequencyRun()
{
    SignalBuffer& dentry = *sbuffer;
    int size = dentry.beginRead();
    if (!size || !plotReady)
        return;

    data()->clear();
    for (int i = size - 2; i >= 0 && dentry[i].first < dentry[i + 1].first; i--)
        addData(dentry[i].first, dentry[i].second);
    dentry.endRead();
    plotReady = false;
}

SignalBars::SignalBars(QString color, QCPAxis* x, QCPAxis* y, SignalBuffer*& dentry)
    : QCPBars(x, y)
    , SignalPlottable(dentry)
{
    QColor qc(color);
    setPen(QPen(qc));
    qc.setAlpha(qc.alpha() - 120);
    setBrush(qc);
    setAntialiasedFill(false);
    setName(QString::fromStdString(dentry->name));
}

SignalBars::~SignalBars() {}

void SignalBars::run()
{
    SignalBuffer& dentry = *sbuffer;
    int size = dentry.beginRead();
    for (int i = 0; i < size; i++)
        data()->insert(dentry[i].first, QCPBarData(dentry[i].first, dentry[i].second));
    dentry.endRead();
}

void SignalBars::swapAxis()
{
    QCPAxis* man = keyAxis();
    setKeyAxis(valueAxis());
    setValueAxis(man);
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////// SignalPlot //////////////////////////////////////////////

SignalPlot::SignalPlot()
{
    legend->setVisible(true);
    setInteraction(QCP::iRangeDrag);
    setInteraction(QCP::iRangeZoom);
}

SignalPlot::~SignalPlot()
{

}

void SignalPlot::wheelEvent(QWheelEvent* event)
{
    axisRect()->setRangeZoom(event->modifiers() == Qt::ShiftModifier ? Qt::Horizontal : Qt::Vertical);
    QCustomPlot::wheelEvent(event);
}

TimePlot::TimePlot()
    : SignalPlot() {}

TimePlot::~TimePlot() {}

void TimePlot::replot()
{
    double minimum = 0;

    for (int i = 0; i < graphCount(); i++)
        if (graph(i)->data()->size() > 0)
            minimum = std::max(graph(i)->data()->lastKey(), minimum);

    xAxis->setRange(minimum, minimum - xAxis->range().size());

    for (int i = 0; i < graphCount(); i++)
        graph(i)->removeDataBefore(minimum - xAxis->range().size());
    QCustomPlot::replot();
}

FrequencyPlot::FrequencyPlot(QCPAxis::ScaleType stype)
{
    xAxis->setScaleType(stype);
    xAxis->setScaleLogBase(10);
}

FrequencyPlot::~FrequencyPlot() {}

void FrequencyPlot::replot()
{
    for (int i = 0; i < graphCount(); i++)
        static_cast<SignalGraph*>(graph(i))->setPlotReady();
    yAxis->setRange(0, yAxis->range().upper - yAxis->range().lower);
    QCustomPlot::replot();
}

BarPlot::BarPlot(QVector<QString> names, bool vertical)
    : channelAxis(vertical ? xAxis : yAxis)
    , valueAxis(vertical ? yAxis : xAxis)
{
    channelAxis->setAutoTicks(false);
    channelAxis->setAutoTickLabels(false);

    QVector<double> ticks;
    for (int i = 0; i < names.size(); i++)
        ticks << i;
    channelAxis->setTickVector(ticks);
    channelAxis->setTickVectorLabels(names);

    count = names.size();
}

void BarPlot::replot()
{
    channelAxis->setRange(-1, count);
    valueAxis->setRange(0, valueAxis->range().upper - valueAxis->range().lower);
    QCustomPlot::replot();
}

BarPlot::~BarPlot() {}
