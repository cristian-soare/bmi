#include "guis.h"
#include "ui_plot.h"
#include "ui_graph.h"

#include "fftplugin.h"

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpinBox>
#include <QSpacerItem>
#include <QLabel>
#include <string>

using namespace std;

//////////////////// GraphSettingsGui //////////////////////////////////
////////////////////////////////////////////////////////////////////////

GraphSettingsGui::GraphSettingsGui(QWidget* parent)
	: QDialog(parent)
	, ui(new Ui::GraphSettingsGui)
{
	ui->setupUi(this);

	ui->windowShift->setMinimum(1); ui->windowShift->setMaximum(1000);
	ui->windowShift->setSingleStep(100);

	connect(ui->okButton, SIGNAL(clicked()), this, SLOT(okClicked()));
}

GraphSettingsGui::~GraphSettingsGui()
{

}

void GraphSettingsGui::setPlugin(FFTGraphPlugin* plugin)
{
	this->plugin = plugin;
	ui->twindow->setValue(plugin->timeWindow);
	ui->windowShift->setValue(plugin->windowShift);
	ui->hancb->setChecked(plugin->useWindowing);
	ui->spscb->setChecked(plugin->definedSPS);
	ui->freqSpin->setValue(plugin->samplesPerSec);
}

void GraphSettingsGui::okClicked()
{
	plugin->timeWindow = ui->twindow->value();
	plugin->windowShift = ui->windowShift->value();
	plugin->useWindowing = ui->hancb->isChecked();
	plugin->definedSPS = ui->spscb->isChecked();
	plugin->samplesPerSec = ui->freqSpin->value();
	close();
}
