#include "fft.h"

#include <cmath>

using namespace std;

FastFourierTransform::FastFourierTransform(int count)
	: count(count)
	, size(count)
	, fill(0)
	, data(new float[count * 2]) {}

FastFourierTransform::~FastFourierTransform()
{
	delete[] data;
}

void FastFourierTransform::setSampleCount(int x)
{
	count = x;
	if (count < size / 2) {
		delete[] data;
		data = new float[count * 2];
	}
}

void FastFourierTransform::clear()
{
	fill = 0;
	int n = count * 2;
	for (int i = 0; i < n; i++)
		data[i] = 0;
}

#define SWAP(a, b) cop = a; a = b; b = cop

void FastFourierTransform::bitReversal()
{
	int i, j, cop;
	int n, m;

	n = count * 2;
	j = 0;
    for (i = 0; i < count; i += 2) {
        if (j > i) {
            //swap the real part
            SWAP(data[j], data[i]);
            //swap the complex part
            SWAP(data[j + 1], data[i + 1]);
            // checks if the changes occurs in the first half
            // and use the mirrored effect on the second half
            if (j / 2 < n / 4) {
                //swap the real part
                SWAP(data[n - (i + 2)], data[n - (j + 2)]);
                //swap the complex part
                SWAP(data[n - (i + 2) + 1], data[n - (j + 2) + 1]);
            }
        }
        m = n / 2;
        while (m >= 2 && j >= m) {
            j -= m;
            m /= 2;
        }
        j += m;
    }
}

void FastFourierTransform::fft()
{
	int n, m, mmax, istep, i, j;
	double wtemp, wr, wpr, wpi, wi, theta;
	float tempr, tempi;

	n = count * 2;
	bitReversal();

	mmax = 2;
	while (n > mmax) {
		istep = mmax * 2;
		theta = 2 * M_PI / mmax;
		wtemp = sin(0.5 * theta);
		
		wpr = -2.0 * wtemp * wtemp;
		wpi = sin(theta);
		
		wr = 1.0;
		wi = 0.0;

		for (m = 1; m < mmax; m += 2) {
			for (i = m; i <= n; i += istep) {
				j = i + mmax;
				tempr = wr * data[j - 1] - wi * data[j];
                tempi = wr * data[j] + wi * data[j - 1];
                data[j - 1] = data[i - 1] - tempr;
                data[j] = data[i] - tempi;
                data[i - 1] += tempr;
                data[i] += tempi;
			}

			wr = (wtemp = wr) * wpr - wi * wpi + wr;
            wi = wi * wpr + wtemp * wpi + wi;
		}
		mmax = istep;
	}
}

const float* FastFourierTransform::run()
{
	fft();
	return data;
}
