#ifndef FFTPLUGIN_H
#define FFTPLUGIN_H

#include "classinterfaces.h"
#include <fftw3.h>

class QDialog;
class QWidget;
class QLabel;
class QHBoxLayout;
template <typename T> class CircularBuffer;

class FFTGraphPlugin;
class PlotSettingsGui;
class GraphSettingsGui;
class FFTPlugin;

////////////////////// FFTGraphPlugin ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

class FFTGraphPlugin : public IPluginGraph
{
	friend class GraphSettingsGui;

public:
	FFTGraphPlugin(FFTPlugin* parent);
	~FFTGraphPlugin();

	void show();
	void confirm();
	void reset();

    std::pair<
        std::vector<std::pair<std::string, SignalBuffer::Domain>>,
        std::vector<std::pair<std::string, SignalBuffer::Domain>>> getIOProp();
    SignalBuffer*& operator[](const std::string& s);
	void run();

	void save(pugi::xml_node node);
	void load(pugi::xml_node node);

private:
	void shiftInput();

	SignalBuffer *input, *output;
	int timeWindow, windowShift, samplesPerSec;
	double baseFreq;
	bool definedSPS, useWindowing;

	double* intime;
	fftw_complex *in, *out;
	fftw_plan plan;
	int filled;
	FFTPlugin* parent;
};

/////////////////// FFTPlugin ////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

class FFTPlugin : public IPlugin
{
public:
	FFTPlugin(QWidget* p);
	~FFTPlugin();

	QString getName();
	IPluginGraph* initGraphPlugin();
	void removeGraph(IPluginGraph* plot);

	GraphSettingsGui* graphGui;

private:
	QHBoxLayout* layout;
	std::vector<FFTGraphPlugin*> plugins;
};

#endif
