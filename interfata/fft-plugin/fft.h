#ifndef FFT_H
#define FFT_H

class FastFourierTransform
{
public:
	FastFourierTransform(int count);
	~FastFourierTransform();

	int getSampleCount();
	void setSampleCount(int x);
	
	void addData(const float& y);
	const float* run();

private:
	void clear();
	void bitReversal();
	void fft();

	int count, size, fill;
	float* data;
};

inline int FastFourierTransform::getSampleCount()
{
	return count;
}

inline void FastFourierTransform::addData(const float& y)
{
	data[fill] = y;
	fill += 2;
}

#endif
