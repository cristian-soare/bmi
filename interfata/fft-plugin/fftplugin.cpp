#include "fftplugin.h"

#include "fft.h"
#include "guis.h"

#include <QMessageBox>
#include <QHBoxLayout>
#include <QLabel>

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <cstring>

using namespace std;

//////////////////////////////// FFTPlugin ///////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

FFTPlugin::FFTPlugin(QWidget* p)
	: IPlugin(p)
	, graphGui(new GraphSettingsGui(p))
{
	layout = new QHBoxLayout(p);
	setLayout(layout);
	layout->setAlignment(Qt::AlignCenter);
	layout->addWidget(new QLabel("BMI FFT Plugin\n(C)2015 Cristian Soare"));
	layout->setSizeConstraint(QLayout::SetFixedSize);
}

FFTPlugin::~FFTPlugin()
{
	// delete layout;
	// delete label;
}

static FFTPlugin* globalPlugin;

extern "C" IPlugin* initPlugin(QWidget* parent)
{
	if (!globalPlugin)
		globalPlugin = new FFTPlugin(parent);
	return globalPlugin;
}

extern "C" void destroyPlugin()
{
	delete globalPlugin;
	globalPlugin = NULL;
}

QString FFTPlugin::getName()
{
	return QString("FFT plugin v 0.1");
}

IPluginGraph* FFTPlugin::initGraphPlugin()
{
	plugins.push_back(new FFTGraphPlugin(this));
	return plugins.back();
}

void FFTPlugin::removeGraph(IPluginGraph* plot)
{
	for (unsigned int i = 0; i < plugins.size(); i++)
		if (plot == plugins[i]) {
			plugins.erase(plugins.begin() + i);
			break;
		}
}

////////////////////////////// Graph plugin ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

FFTGraphPlugin::FFTGraphPlugin(FFTPlugin* plot)
	: parent(plot)
{
	in = out = NULL;
	timeWindow = 1024;
	windowShift = 200;
	samplesPerSec = 44100;
	useWindowing = false;
	input = output = nullptr;
}

FFTGraphPlugin::~FFTGraphPlugin()
{
	if (in) {
		fftw_destroy_plan(plan);
		delete[] intime;
		delete[] in;
		delete[] out;
	}
	parent->removeGraph(this);
	cout << "removed" << endl;
}

static int upper2Power(float lim)
{
	int i = 2;
	while (2 << i < lim)
		i++;
	return 2 << i;
}

void FFTGraphPlugin::show()
{
	globalPlugin->graphGui->setPlugin(this);
	globalPlugin->graphGui->exec();
}

void FFTGraphPlugin::confirm()
{
	timeWindow = upper2Power(timeWindow);
	baseFreq = definedSPS ? (float)samplesPerSec / timeWindow : 0;

	intime = new double[timeWindow];
	in = new fftw_complex[timeWindow]();
	out = new fftw_complex[timeWindow];
	plan = fftw_plan_dft_1d(timeWindow, in, out, FFTW_FORWARD, FFTW_MEASURE);

	cout << timeWindow << " " << windowShift << " " << baseFreq << " " << endl;
}

pair<vector<pair<string, SignalBuffer::Domain>>, vector<pair<string, SignalBuffer::Domain>>> FFTGraphPlugin::getIOProp()
{
	vector<pair<string, SignalBuffer::Domain>> ins; ins.push_back(make_pair("input", SignalBuffer::Time));
	vector<pair<string, SignalBuffer::Domain>> outs; outs.push_back(make_pair("output", SignalBuffer::Frequency));
	return make_pair(ins, outs);
}

SignalBuffer*& FFTGraphPlugin::operator[](const std::string& s)
{
	return s == "input" ? input : output;
}

#define POW2(x) ((x) * (x))

void FFTGraphPlugin::run()
{
	int i = 0, size = input->getSize();
	SignalBuffer& v = *input;

	while (i < size) {
		for (; i < size && filled < timeWindow; i++) {
			in[filled][0] = v[i].second;
			in[filled][1] = 0;
			intime[filled] = v[i].first;
			filled++;
		}

		if (filled == timeWindow) {
			if (useWindowing)
				for (int i = 0; i < filled; i++) {
					double multiplier = 0.5 * (1 - cos(2 * M_PI * i / (filled - 1)));
					in[i][0] *= multiplier;
				}

			fftw_execute(plan);
			shiftInput();

			double f = 0;
			if (baseFreq == 0) {
				for (int i = 1; i < filled; i++)
					baseFreq += intime[i] - intime[i - 1];

				baseFreq /= (filled - 1);
				baseFreq = 1. / baseFreq / timeWindow;
				cout << "settled base freq at " << baseFreq << endl;
			}

			for (int j = 0; j < timeWindow / 2; j++) {
				output->add(make_pair(f, sqrt(POW2(out[j][0]) + POW2(out[j][1])) / timeWindow));
				f += baseFreq;
			}
		}
	}
}

void FFTGraphPlugin::shiftInput()
{
	for (int i = timeWindow - windowShift; i < timeWindow; i++) {
		in[i - windowShift][0] = in[i][0]; in[i - windowShift][1] = in[i][1];
		intime[i - windowShift] = intime[i];
	}
	filled -= timeWindow - windowShift;
}

void FFTGraphPlugin::reset()
{
	confirm();
	filled = 0;
}

void FFTGraphPlugin::save(pugi::xml_node node)
{
	node.append_attribute("timeWindow") = timeWindow;
	node.append_attribute("windowShift") = windowShift;
	node.append_attribute("definedSPS") = definedSPS;
	node.append_attribute("samplesPerSec") = samplesPerSec;
	node.append_attribute("useWindowing") = useWindowing;
}

void FFTGraphPlugin::load(pugi::xml_node node)
{
	timeWindow = node.attribute("timeWindow").as_int();
	windowShift = node.attribute("windowShift").as_int();
	definedSPS = node.attribute("definedSPS").as_bool();
	samplesPerSec = node.attribute("samplesPerSec").as_int();
	useWindowing = node.attribute("useWindowing").as_bool();
}
