#ifndef GUIS_H
#define GUIS_H

#include <QDialog>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QPushButton>

class FFTPlotPlugin;
class FFTGraphPlugin;

namespace Ui
{
class PlotSettingsGui;
class GraphSettingsGui;
}

///////////////////// GraphSettingsGui //////////////////////////
/////////////////////////////////////////////////////////////////

class GraphSettingsGui : public QDialog
{
	Q_OBJECT

public:
	GraphSettingsGui(QWidget* parent);
	~GraphSettingsGui();

	void setPlugin(FFTGraphPlugin* plugin);

private slots:
	void okClicked();

private:
	Ui::GraphSettingsGui* ui;

	FFTGraphPlugin* plugin;
};

#endif
