QT += core gui widgets printsupport

TARGET = fft
TEMPLATE = lib
CONFIG += debug

INCLUDEPATH += ../commons

LIBS += -lfftw3
SOURCES += fftplugin.cpp guis.cpp
HEADERS += fftplugin.h guis.h ../commons/generalbuffer.h ../commons/signalbuffer.h
FORMS += plot.ui graph.ui

QMAKE_CXXFLAGS += -std=c++14
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast
