#include "dsp.h"

#include <sstream>
#include <parser/expressionparser.h>
#include <parser/parsetree.h>

using namespace std;

static DSPPlugin* globalPlugin;

extern "C" IPlugin* initPlugin(QWidget* parent)
{
	if (!globalPlugin)
		globalPlugin = new DSPPlugin(parent);
	return globalPlugin;
}

extern "C" void destroyPlugin()
{
	delete globalPlugin;
	globalPlugin = nullptr;
}

DSPPlugin::DSPPlugin(QWidget* p)
	: IPlugin(p)
{

}

DSPPlugin::~DSPPlugin()
{

}

QString DSPPlugin::getName()
{
	return QString("DSP v0.1");
}

IPluginGraph* DSPPlugin::initGraphPlugin()
{
	return new DSPGraphPlugin();
}

//////////////////// graph ///////////////////

DSPGraphPlugin::DSPGraphPlugin()
	: dspSettings(inputs, idx)
	, pt(nullptr) {}

DSPGraphPlugin::~DSPGraphPlugin() {}

void DSPGraphPlugin::show()
{
	if (dspSettings.exec() == QDialog::Rejected)
		return;

	delete pt;
	pt = dspSettings.getParseTree();
	oldestOffset = pt->getOldestIndex();
}

void DSPGraphPlugin::run()
{
	int size = inputs.begin()->second->getSize();
	for (auto& i : inputs) {
		int tsz = i.second->getSize();
		if (size > tsz)
			size = tsz;
	}

	for (idx = -oldestOffset; idx < size; idx++)
		output->add(make_pair(inputs.begin()->second->at(idx).first, pt->compute()));

	for (auto& i : inputs)
		i.second->endRead(size + oldestOffset);
}

pair<vector<pair<string, SignalBuffer::Domain>>, vector<pair<string, SignalBuffer::Domain>>> DSPGraphPlugin::getIOProp()
{
	vector<pair<string, SignalBuffer::Domain>> ins;
	for (auto& i : inputs)
		ins.push_back(make_pair(i.first, SignalBuffer::Time));
	vector<pair<string, SignalBuffer::Domain>> outs{make_pair("out", SignalBuffer::Time)};
	return make_pair(ins, outs);
}

SignalBuffer*& DSPGraphPlugin::operator[](const std::string& s)
{
	if (s.find("in") == 0)
		return inputs[s];
	return output;
}

void DSPGraphPlugin::save(pugi::xml_node node)
{
	node.append_attribute("inputCount") = dspSettings.getParams().second;
	node.append_attribute("formula") = dspSettings.getParams().first.c_str();
}

void DSPGraphPlugin::load(pugi::xml_node node)
{
	dspSettings.setParams(node.attribute("formula").value(), node.attribute("inputCount").as_int());
	pt = dspSettings.getParseTree();
	oldestOffset = pt->getOldestIndex();
}
