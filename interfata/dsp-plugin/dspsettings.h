#ifndef DSPSETTINGS_H
#define DSPSETTINGS_H

#include <QDialog>
#include <string>
#include <utility>

namespace Ui {
class DSPSettings;
}

class ParseTree;
class SignalBuffer;

class DSPSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DSPSettings(std::map<std::string, SignalBuffer*>& buffers, const int& offs, QWidget *parent = 0);
    ~DSPSettings();

    std::pair<std::string, int> getParams();
    void setParams(const std::string& formula, int inCount);
    ParseTree* getParseTree();

private slots:
    void on_showAssigned_pressed();

    void on_showAssigned_released();

private:
    void accept();
    void reject();

    Ui::DSPSettings *ui;
    QString formula;
    int inputCount;

    std::map<std::string, SignalBuffer*>& buffers;
    const int& offs;
    QString orig;
};

#endif // DSPSETTINGS_H
