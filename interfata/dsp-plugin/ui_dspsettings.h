/********************************************************************************
** Form generated from reading UI file 'dspsettings.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPSETTINGS_H
#define UI_DSPSETTINGS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DSPSettings
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_2;
    QSpinBox *inSpin;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *formulaLine;
    QCheckBox *showAssigned;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *DSPSettings)
    {
        if (DSPSettings->objectName().isEmpty())
            DSPSettings->setObjectName(QStringLiteral("DSPSettings"));
        DSPSettings->resize(344, 143);
        verticalLayout = new QVBoxLayout(DSPSettings);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_2 = new QLabel(DSPSettings);
        label_2->setObjectName(QStringLiteral("label_2"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);

        horizontalLayout_5->addWidget(label_2);

        inSpin = new QSpinBox(DSPSettings);
        inSpin->setObjectName(QStringLiteral("inSpin"));
        inSpin->setMinimum(1);

        horizontalLayout_5->addWidget(inSpin);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(DSPSettings);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        formulaLine = new QLineEdit(DSPSettings);
        formulaLine->setObjectName(QStringLiteral("formulaLine"));

        horizontalLayout->addWidget(formulaLine);


        verticalLayout->addLayout(horizontalLayout);

        showAssigned = new QCheckBox(DSPSettings);
        showAssigned->setObjectName(QStringLiteral("showAssigned"));

        verticalLayout->addWidget(showAssigned);

        buttonBox = new QDialogButtonBox(DSPSettings);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(DSPSettings);
        QObject::connect(buttonBox, SIGNAL(accepted()), DSPSettings, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DSPSettings, SLOT(reject()));

        QMetaObject::connectSlotsByName(DSPSettings);
    } // setupUi

    void retranslateUi(QDialog *DSPSettings)
    {
        DSPSettings->setWindowTitle(QApplication::translate("DSPSettings", "Dialog", 0));
        label_2->setText(QApplication::translate("DSPSettings", "Input count: ", 0));
        label->setText(QApplication::translate("DSPSettings", "Formula: ", 0));
        showAssigned->setText(QApplication::translate("DSPSettings", "Show assigned channels names", 0));
    } // retranslateUi

};

namespace Ui {
    class DSPSettings: public Ui_DSPSettings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPSETTINGS_H
