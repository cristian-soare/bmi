#include "dspsettings.h"
#include "ui_dspsettings.h"

#include <QMessageBox>
#include <sstream>
#include <parser/expressionparser.h>
#include <parser/parsetree.h>

using namespace std;

DSPSettings::DSPSettings(std::map<std::string, SignalBuffer*>& buffers, const int& offs, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DSPSettings),
    buffers(buffers),
    offs(offs)
{
    ui->setupUi(this);
}

DSPSettings::~DSPSettings()
{
    delete ui;
}

void DSPSettings::accept()
{
    if (!getParseTree())
        return;

    formula = ui->formulaLine->text();
    inputCount = ui->inSpin->value();
    QDialog::accept();
}

void DSPSettings::reject()
{
    ui->formulaLine->setText(formula);
    ui->inSpin->setValue(inputCount);
    QDialog::reject();
}

pair<string, int> DSPSettings::getParams()
{
    return make_pair(formula.toStdString(), inputCount);
}

void DSPSettings::setParams(const string& formula, int inCount)
{
    this->formula = QString::fromStdString(formula);
    inputCount = inCount;

    ui->formulaLine->setText(this->formula);
    ui->inSpin->setValue(inputCount);
}

ParseTree* DSPSettings::getParseTree()
{
    map<string, SignalBuffer*> nbuffers;
    if (buffers.size() != static_cast<unsigned int>(ui->inSpin->value())) {
        for (int i = 0; i < ui->inSpin->value(); i++) {
            ostringstream oss; oss << "in" << i;
            nbuffers[oss.str()] = nullptr;
        }
    } else {
        nbuffers = buffers;
    }

    ExpressionParser ep(nbuffers, offs);
    try {
        ParseTree* pt = ep.eval(ui->formulaLine->text().toStdString());
        pt->getOldestIndex();
        delete pt;

        buffers = nbuffers;
        ExpressionParser ep(buffers, offs);
        return ep.eval(ui->formulaLine->text().toStdString());
    } catch (const exception& e) {
        QMessageBox::critical(this, "Error", e.what());
        return nullptr;
    }
}

void DSPSettings::on_showAssigned_pressed()
{
    ui->showAssigned->setChecked(true);
    orig = ui->formulaLine->text();
    QString tmp = buffers.size() ? orig : "Channels not assigned";

    for (auto& i : buffers)
        if (i.second) {
            tmp.replace(QString::fromStdString(i.first), QString::fromStdString(i.second->name));
        } else {
            tmp = "Channels not assigned";
            break;
        }
    ui->formulaLine->setText(tmp);
}

void DSPSettings::on_showAssigned_released()
{
    ui->showAssigned->setChecked(false);
    ui->formulaLine->setText(orig);
}
