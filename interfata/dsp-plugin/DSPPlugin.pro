QT += core gui widgets printsupport

TARGET = dsp
TEMPLATE = lib
CONFIG += debug

INCLUDEPATH += ../commons

SOURCES += dsp.cpp \
	dspsettings.cpp \
	parser/expressionparser.cpp \
    parser/operator.cpp \
    parser/parserexceptions.cpp \
    parser/parsetree.cpp \
    parser/stringsplitter.cpp

HEADERS += dsp.h \
	dspsettings.h \
	../commons/classinterfaces.h \
	../commons/signalbuffer.h \
	parser/expressionparser.h \
    parser/operator.h \
    parser/parserexceptions.h \
    parser/parsetree.h \
    parser/stringsplitter.h

# FORMS += dspsettings.ui

QMAKE_CXXFLAGS += --std=c++14
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast
