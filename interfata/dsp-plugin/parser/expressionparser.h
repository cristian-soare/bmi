#ifndef EXPRESSIONPARSER_H
#define EXPRESSIONPARSER_H

#include "stringsplitter.h"
#include "operator.h"
#include <signalbuffer.h>
#include <stack>
#include <map>

class ParseTree;

class ExpressionParser
{
public:
	ExpressionParser(std::map<std::string, SignalBuffer*>& buffers, const int& offs);
	ParseTree* eval(const std::string& expr);

private:
	int index, paranthesis;
	StringSplitter ss;
	std::vector<std::string> delim;
	std::map<std::string, SignalBuffer*>& buffers;
	const int& offs;

	void processOperator(std::stack<ParseTree*>& res, std::stack<Operator*>& opers);
	ParseTree* parse();
	bool isValidOper(std::string& s);

	template <typename T>
	void freeMem(std::stack<T*>& v);
};

#endif
