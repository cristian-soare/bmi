#ifndef PARSETREE_H
#define PARSETREE_H

#include <vector>

class Operator;

class ParseTree
{
public:
	ParseTree(double val);
	ParseTree(double* val);
	ParseTree(Operator* op, ParseTree* l, ParseTree* r);
	~ParseTree();

	double compute(bool constant = false);
	int getOldestIndex();

	template <typename T>
	void getOperators(std::vector<T*>& ops);
	void getParamList(std::vector<ParseTree*>& trees);

private:
	int countOperands();

	double* rootValue;
	bool variable;
	Operator* const oper;
	ParseTree* const left;
	ParseTree* const right;
};

template <typename T>
void ParseTree::getOperators(std::vector<T*>& ops)
{
	if (left)
		left->getOperators<T>(ops);
	if (dynamic_cast<T*>(oper))
		ops.push_back(static_cast<T*>(oper));
	if (right)
		right->getOperators<T>(ops);
}

#endif