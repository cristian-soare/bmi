#ifndef STRINGSPLITTER_H
#define STRINGSPLITTER_H

#include <string>
#include <vector>
#include <iosfwd>

class StringSplitter
{
public:
	StringSplitter();
	void setDelim(std::vector<std::string>& valids);
	void setString(const std::string& str);
	bool read(std::string& s);

private:
	std::vector<std::string> valids; bool okValids;
	std::string str; bool okStr;
	int idx;

	int validMatched();
	int firstMatch();
	static bool cmpStringLen(std::string& a, std::string& b);
};

#endif
