#include "stringsplitter.h"

#include <cstring>
#include <algorithm>

using namespace std;

StringSplitter::StringSplitter()
	: okValids(false)
	, okStr(false) 
	, idx(0) {}

void StringSplitter::setDelim(vector<string>& valids)
{
	this->valids = valids;
	sort(this->valids.begin(), this->valids.end(), cmpStringLen);
	okValids = true;
}

void StringSplitter::setString(const string& str)
{
	this->str = str;
	okStr = true;
}

bool StringSplitter::read(string& s)
{
	if (!okValids || !okStr) return false;
	while (idx < (int)str.size() && str[idx] == ' ') ++idx;
	if (idx == (int)str.size()) return false;

	int currentIdx = idx;
	int pos = firstMatch();

	if (pos >= 0 && currentIdx == idx) {
		s = valids[pos];
		idx += valids[pos].size();
		return true;
	}

	if (idx - currentIdx > 0) {
		s = string(str.c_str() + currentIdx, idx - currentIdx);
		return true;
	}

	return false;
}

int StringSplitter::validMatched()
{
	for (unsigned int i = 0; i < valids.size(); ++i)
		if (valids[i].size() <= str.size() - idx
			&& !strncmp(valids[i].c_str(), str.c_str() + idx, valids[i].size()))
			return i;
	return -1;
}

int StringSplitter::firstMatch()
{
	for (; idx < (int)str.size() && str[idx] != ' '; ++idx) {
		int pos = validMatched();
		if (pos >= 0) return pos;
	}
	return -1;
}

bool StringSplitter::cmpStringLen(string& a, string& b)
{
	return a.size() > b.size();
}
