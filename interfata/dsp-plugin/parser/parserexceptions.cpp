#include "parserexceptions.h"

using namespace std;

const char EvaluatorEx::NEG_SQRT[] = "Negative value passed to square root";
const char EvaluatorEx::NEG_LOG[] = "Negative value passed to logarithm";
const char EvaluatorEx::ZERO_LOG[] = "Expression under logarithm evaluates to zero";
const char EvaluatorEx::DIV_ZERO[] = "Division by zero";
const char EvaluatorEx::VAR_IDX[] = "Variable expression used as array index";
const char EvaluatorEx::POZ_IDX[] = "Pozitive array index";
const char EvaluatorEx::PARAM_COUNT[] = "Invalid number of parameters";

const char SyntacticEx::INV_OP[] = "Invalid operator or operand";
const char SyntacticEx::INV_PAR[] = "Invalid parantheses";
const char SyntacticEx::INV_EXP[] = "Invalid expression";
