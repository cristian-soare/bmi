#ifndef OPERATOR_H
#define OPERATOR_H

#include <string>
#include <iosfwd>
#include <signalbuffer.h>

class ParseTree;

class Operator
{
public:
	enum TypeEnum {
		COMMA = 0,
		PLUS,
		MINUS_BINARY,
		PROD,
		DIV,
		MINUS_UNARY,
		POW,
		LOG,
		SQRT,
		SIN,
		COS,
		SUM,
		ARRAY,
	};

	static const std::string operators[];

	Operator(const std::string& tip);
	virtual ~Operator() {}

	double calc(double a, double b);
	double calc(double a);
	bool operator<(const Operator& o);
	bool isBinary();
	void unaryMinus();
	int getType();
	std::ostream& operator<<(std::ostream& out);

private:
	TypeEnum type;
};

class ArrayOp : public Operator
{
public:
	ArrayOp(const std::string& name, SignalBuffer*& sbuffer, bool val = true);

	double calc(int idx);
	void addOffset(const int* offs);

	const std::string name;

private:
	SignalBuffer*& sbuffer;
	std::vector<const int*> offsets;
	bool val;
};

class SumOp : public Operator
{
public:
	SumOp();

	bool hasParams();
	void addParams(std::vector<ParseTree*>& trees);
	double calc();

private:
	int count, idx;
	ParseTree* formula;
};

#endif