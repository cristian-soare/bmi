#ifndef PARSEREXCEPTIONS_H
#define PARSEREXCEPTIONS_H

#include <exception>

class ParserAbstractEx : public std::exception
{
public:
    ParserAbstractEx(const char* msg) throw()
        : msg(msg) {}

	virtual const char* what() const throw()
	{
        return msg;
    }

private:
    const char* msg;
};

class EvaluatorEx : public ParserAbstractEx
{
public:
    static const char NEG_SQRT[];
    static const char NEG_LOG[];
    static const char ZERO_LOG[];
    static const char DIV_ZERO[];
    static const char VAR_IDX[];
    static const char POZ_IDX[];
    static const char PARAM_COUNT[];

    EvaluatorEx(const char* msg) throw()
		: ParserAbstractEx(msg) {}
};

class SyntacticEx : public ParserAbstractEx
{
public:
    static const char INV_OP[];
    static const char INV_PAR[];
    static const char INV_EXP[];

    SyntacticEx(const char* msg) throw()
		: ParserAbstractEx(msg) {}
};

#endif
