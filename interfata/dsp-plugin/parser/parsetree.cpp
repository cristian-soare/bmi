#include "parsetree.h"

#include "parserexceptions.h"
#include "operator.h"

ParseTree::ParseTree(double val)
	: rootValue(new double(val))
	, variable(false)
	, oper(NULL)
	, left(NULL)
	, right(NULL) {}

ParseTree::ParseTree(double* val)
	: rootValue(val)
	, variable(true)
	, oper(NULL)
	, left(NULL)
	, right(NULL) {}

ParseTree::ParseTree(Operator* op, ParseTree* l, ParseTree* r)
	: rootValue(new double())
	, variable(false)
	, oper(op)
	, left(l)
	, right(r)
{
	int n = countOperands();
	if (op->isBinary() && n == 2) return;
	if (!op->isBinary() && n == 1) return;

	delete rootValue; delete oper;
	delete left; delete right;
	throw SyntacticEx(SyntacticEx::INV_OP);
}

ParseTree::~ParseTree()
{
	if (!variable) delete rootValue;
	delete oper;
	delete left;
	delete right;
}

double ParseTree::compute(bool constant)
{
	if (oper) {
		double r = right->compute();
		if (left) {
			double l = left->compute();
			*rootValue = oper->calc(l, r);
		} else if (oper->getType() == Operator::ARRAY) {
			if (constant)
				throw EvaluatorEx(EvaluatorEx::VAR_IDX);
			*rootValue = static_cast<ArrayOp*>(oper)->calc(r);
		} else if (oper->getType() == Operator::SUM) {
			if (!static_cast<SumOp*>(oper)->hasParams()) {
				std::vector<ParseTree*> trees;
				right->getParamList(trees);
				static_cast<SumOp*>(oper)->addParams(trees);
			}

			*rootValue = static_cast<SumOp*>(oper)->calc();
		} else {
			*rootValue = oper->calc(r);
		}
	}
	return *rootValue;
}

int ParseTree::getOldestIndex()
{
	if (oper) {
		if (oper->getType() == Operator::ARRAY) {
			int ret = right->compute(true);
			if (ret > 0)
				throw EvaluatorEx(EvaluatorEx::POZ_IDX);
			return ret;
		}

		int r = right->getOldestIndex();
		if (left)
			return std::min(left->getOldestIndex(), r);
		return r;
	}
	return 0;
}

void ParseTree::getParamList(std::vector<ParseTree*>& trees)
{
	if (oper->getType() != Operator::COMMA) {
		trees.push_back(this);
		return;
	}

	if (left)
		left->getParamList(trees);
	if (right)
		right->getParamList(trees);
}

int ParseTree::countOperands()
{
	int n = 0;
	if (left) n++;
	if (right) n++;
	return n;
}
