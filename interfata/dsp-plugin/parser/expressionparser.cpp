#include "expressionparser.h"

#include "parserexceptions.h"
#include "operator.h"
#include "parsetree.h"

#include <cstdlib>
#include <cctype>

using namespace std;

ExpressionParser::ExpressionParser(map<string, SignalBuffer*>& buffers, const int& offs)
	: buffers(buffers)
	, offs(offs)
{
	for (int i = 0; i < 12; i++)
		delim.push_back(Operator::operators[i]);
	delim.push_back("("); delim.push_back(")");
}

ParseTree* ExpressionParser::eval(const std::string& expr)
{
	index = paranthesis = 0;

	ss.setDelim(delim);
	ss.setString(expr);

	try {
		ParseTree* pt = parse();

		if (paranthesis)
			throw SyntacticEx(SyntacticEx::INV_PAR);
		return pt;
	} catch (ParserAbstractEx e) {
		throw e;
	}
}

void ExpressionParser::processOperator(stack<ParseTree*>& res, stack<Operator*>& opers)
{
	if (res.empty() || opers.empty() || (opers.top()->isBinary() && res.size() == 1)) {
		freeMem<Operator>(opers); freeMem<ParseTree>(res);
		throw SyntacticEx(SyntacticEx::INV_EXP);
	}

	Operator* o = opers.top(); opers.pop();
	ParseTree* r = res.top(); res.pop();
	ParseTree* l = NULL;

	if (o->isBinary()) {
		l = res.top();
		res.pop();
	}

	try {
		res.push(new ParseTree(o, l, r));
	} catch (ParserAbstractEx e) {
		freeMem<Operator>(opers); freeMem<ParseTree>(res);
		throw e;
	}
}

ParseTree* ExpressionParser::parse()
{
	stack<Operator*> opers;
	stack<ParseTree*> results;

	try {
		string s;
		while (ss.read(s)) {
			if (s == "(") {
				++index;
				++paranthesis;
				results.push(parse());
	 			continue;
			}

			if (s == ")") {
				--paranthesis;
				if (paranthesis < 0)
					throw SyntacticEx(SyntacticEx::INV_PAR);
				break;
			}

			double x;
			bool okx = true;
			try { x = stod(s); } catch (...) { okx = false; }

			if (okx) {
				results.push(new ParseTree(x));
			} else if (isValidOper(s)) {
				Operator* op = s != "sum" ? new Operator(s) : new SumOp();

				if (opers.empty() && results.empty())
					op->unaryMinus();

				while (true) {
					if (opers.empty() || *opers.top() < *op) {
						opers.push(op);
						break;
					}
					processOperator(results, opers);
				}
			} else if (buffers.find(s) != buffers.end()) {
				opers.push(new ArrayOp(s, buffers[s]));
				static_cast<ArrayOp*>(opers.top())->addOffset(&offs);
			} else if (s == "t") {
				opers.push(new ArrayOp(s, buffers.begin()->second, false));
				static_cast<ArrayOp*>(opers.top())->addOffset(&offs);
			} else {
				throw SyntacticEx(SyntacticEx::INV_EXP);
			}
		}

		while (!opers.empty())
			processOperator(results, opers);

		if (results.size() != 1)
			throw SyntacticEx(SyntacticEx::INV_EXP);
	} catch (ParserAbstractEx e) {
		freeMem<Operator>(opers); freeMem<ParseTree>(results);
		throw e;
	}

	return results.top();
}

bool ExpressionParser::isValidOper(string& s)
{
	for (unsigned int i = 0; i < delim.size(); i++)
		if (delim[i] == s)
			return true;
	return false;
}

template <typename T>
void ExpressionParser::freeMem(stack<T*>& v)
{
	while (!v.empty()) {
		delete v.top();
		v.pop();
	}
}
