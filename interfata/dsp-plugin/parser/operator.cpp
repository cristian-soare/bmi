#include "operator.h"

#include "parserexceptions.h"
#include "parsetree.h"

#include <string>
#include <cmath>

using namespace std;

const std::string Operator::operators[] = { ",", "+", "-", "*", "/", "-", "^", "log", "sqrt", "sin", "cos", "sum" };

Operator::Operator(const std::string& tip)
{
	if (tip == "+") type = PLUS;
	else if (tip == "-") type = MINUS_BINARY;
	else if (tip == "*") type = PROD;
	else if (tip == "/") type = DIV;
	else if (tip == "-") type = MINUS_UNARY;
	else if (tip == "^") type = POW;
	else if (tip == ",") type = COMMA;
	else if (tip == "log") type = LOG;
	else if (tip == "sqrt") type = SQRT;
	else if (tip == "sin") type = SIN;
	else if (tip == "cos") type = COS;
	else if (tip == "sum") type = SUM;
	else if (tip == "arr") type = ARRAY;
	else throw SyntacticEx(SyntacticEx::INV_OP);
}

double Operator::calc(double a, double b)
{
	switch (type) {
	case PLUS: return a + b;
	case MINUS_BINARY: return a - b;
	case PROD: return a * b;
	case DIV:
		if (b == 0)
			throw EvaluatorEx(EvaluatorEx::DIV_ZERO);
		return a / b;
	case POW: return pow(a, b);
	default: return 0;
	}
}

double Operator::calc(double a)
{
	switch (type) {
	case MINUS_UNARY: return -a;
	case LOG:
		if (a < 0) throw EvaluatorEx(EvaluatorEx::NEG_LOG);
		if (a == 0) throw EvaluatorEx(EvaluatorEx::ZERO_LOG);
		return log(a);

	case SQRT:
        if (a < 0) throw EvaluatorEx(EvaluatorEx::NEG_SQRT);
		return sqrt(a);

	case SIN: return sin(a);
	case COS: return cos(a);
	default: return 0;
	}
}

bool Operator::operator<(const Operator& o)
{
	return type < o.type || (type == POW && o.type == POW);
}

bool Operator::isBinary()
{
	return type <= 4 || type == 6;
}

void Operator::unaryMinus()
{
	if (type == MINUS_BINARY)
		type = MINUS_UNARY;
}

int Operator::getType()
{
	return type;
}

ostream& Operator::operator<<(ostream& out)
{
	out << operators[type];
	return out;
}

ArrayOp::ArrayOp(const string& name, SignalBuffer*& sbuffer, bool val)
	: Operator("arr")
	, name(name)
	, sbuffer(sbuffer)
	, val(val) {}

void ArrayOp::addOffset(const int* offs)
{
	offsets.push_back(offs);
}

double ArrayOp::calc(int idx)
{
	int offs = 0;
	for (const auto& i : offsets)
		offs += *i;
	return val ? sbuffer->at(offs + idx).second : sbuffer->at(offs+ idx).first;
}

SumOp::SumOp()
	: Operator("sum")
	, formula(nullptr) {}

bool SumOp::hasParams()
{
	return formula;
}

void SumOp::addParams(vector<ParseTree*>& trees)
{
	if (trees.size() != 2)
		throw EvaluatorEx(EvaluatorEx::PARAM_COUNT);

	count = trees[0]->compute(true);
	formula = trees[1];

	std::vector<ArrayOp*> channels;
	formula->getOperators<ArrayOp>(channels);
	for (auto& i : channels)
		i->addOffset(&idx);
}

double SumOp::calc()
{
	double sum = 0;
	for (idx = count; idx <= 0; idx++)
		sum += formula->compute();
	return sum;
}
