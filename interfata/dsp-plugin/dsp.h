#ifndef DSP_H
#define DSP_H

#include <classinterfaces.h>
#include <signalbuffer.h>
#include <dspsettings.h>

class DSPPlugin : public IPlugin
{
public:
	DSPPlugin(QWidget* p);
	~DSPPlugin();

	QString getName();
	IPluginGraph* initGraphPlugin();
};

class ParseTree;

class DSPGraphPlugin : public IPluginGraph
{
public:
	DSPGraphPlugin();
	~DSPGraphPlugin();

	void show();

	void save(pugi::xml_node node);
	void load(pugi::xml_node node);

	std::pair<
		std::vector<std::pair<std::string, SignalBuffer::Domain>>,
		std::vector<std::pair<std::string, SignalBuffer::Domain>>> getIOProp();
	SignalBuffer*& operator[](const std::string& s);
	void run();

private:
	DSPSettings dspSettings;
	std::map<std::string, SignalBuffer*> inputs;
	SignalBuffer* output;

	ParseTree* pt;
	int idx, oldestOffset;
};

#endif
