#include "svmsettings.h"
#include "ui_svmsettings.h"

#include <QFileDialog>
#include <QMessageBox>

SVMSettings::SVMSettings(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::SVMSettings)
{
	ui->setupUi(this);
}

SVMSettings::~SVMSettings()
{
	delete ui;
}

void SVMSettings::accept()
{
	path = ui->modelPath->text();
	svm_free_and_destroy_model(&model);
	model = svm_load_model(path.toStdString().c_str());

	if (!model) {
		QMessageBox::warning(this, "Error", "Invalid path");
		return;
	}
	QDialog::accept();
}

void SVMSettings::on_buttonBox_rejected()
{
	ui->modelPath->setText(path);
}

void SVMSettings::on_browse_clicked()
{
	ui->modelPath->setText(
				QFileDialog::getOpenFileName(
					this, "Model path", ui->modelPath->text()));
}

svm_model* SVMSettings::getModel()
{
	return model;
}

std::string SVMSettings::getPath()
{
	return path.toStdString();
}

void SVMSettings::setPath(const std::string& path)
{
	this->path = QString::fromStdString(path);
	on_buttonBox_rejected();
}
