#include "svmp.h"

#include <sstream>
#include <iostream>

using namespace std;

static SVMPlugin* globalPlugin;

extern "C" IPlugin* initPlugin(QWidget* parent)
{
	if (!globalPlugin)
		globalPlugin = new SVMPlugin(parent);
	return globalPlugin;
}

extern "C" void destroyPlugin()
{
	delete globalPlugin;
	globalPlugin = nullptr;
}

SVMPlugin::SVMPlugin(QWidget* p)
	: IPlugin(p)
{

}

SVMPlugin::~SVMPlugin()
{

}

QString SVMPlugin::getName()
{
	return QString("SVM v0.1");
}

IPluginGraph* SVMPlugin::initGraphPlugin()
{
	return new SVMGraphPlugin();
}

//////////////////// graph ///////////////////

SVMGraphPlugin::SVMGraphPlugin() {}

SVMGraphPlugin::~SVMGraphPlugin() {}

void SVMGraphPlugin::show()
{
	if (svmSettings.exec() == QDialog::Rejected)
		return;
	model = svmSettings.getModel();
	cout << "nr de indici " << svm_get_nr_sv(model) << endl;
}

void SVMGraphPlugin::run()
{
	// int size = inputs.begin()->second->getSize();
	// for (auto& i : inputs) {
	// 	int tsz = i.second->getSize();
	// 	if (size > tsz)
	// 		size = tsz;
	// }

	// for (idx = -oldestOffset; idx < size; idx++)
	// 	output->add(make_pair(inputs.begin()->second->at(idx).first, pt->compute()));

	// for (auto& i : inputs)
	// 	i.second->endRead(size + oldestOffset);
}

pair<vector<pair<string, SignalBuffer::Domain>>, vector<pair<string, SignalBuffer::Domain>>> SVMGraphPlugin::getIOProp()
{
	vector<pair<string, SignalBuffer::Domain>> ins;
	for (auto& i : inputs)
		ins.push_back(make_pair(i->name, SignalBuffer::Time));

	vector<pair<string, SignalBuffer::Domain>> outs{make_pair("out", SignalBuffer::Time)};
	return make_pair(ins, outs);
}

SignalBuffer*& SVMGraphPlugin::operator[](const std::string& s)
{
	for (auto& i : inputs)
		if (i->name == s)
			return i;
	return output;
}

void SVMGraphPlugin::save(pugi::xml_node node)
{
	node.append_attribute("modelPath") = svmSettings.getPath().c_str();
}

void SVMGraphPlugin::load(pugi::xml_node node)
{
	svmSettings.setPath(node.attribute("modelPath").value());
}
