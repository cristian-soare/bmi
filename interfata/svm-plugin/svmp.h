#ifndef DSP_H
#define DSP_H

#include <classinterfaces.h>
#include <signalbuffer.h>
#include <svmsettings.h>
#include <svm.h>

class SVMPlugin : public IPlugin
{
public:
	SVMPlugin(QWidget* p);
	~SVMPlugin();

	QString getName();
	IPluginGraph* initGraphPlugin();
};

class ParseTree;

class SVMGraphPlugin : public IPluginGraph
{
public:
	SVMGraphPlugin();
	~SVMGraphPlugin();

	void show();

	void save(pugi::xml_node node);
	void load(pugi::xml_node node);

	std::pair<
		std::vector<std::pair<std::string, SignalBuffer::Domain>>,
		std::vector<std::pair<std::string, SignalBuffer::Domain>>> getIOProp();
	SignalBuffer*& operator[](const std::string& s);
	void run();

private:
	SVMSettings svmSettings;
	std::vector<SignalBuffer*> inputs;
	SignalBuffer* output;
	svm_model* model = nullptr;
};

#endif
