QT += core gui widgets printsupport

TARGET = svmp
TEMPLATE = lib
CONFIG += debug

LIBS += -lsvm

INCLUDEPATH += ../commons

SOURCES += \
    svmsettings.cpp \
    svmp.cpp

HEADERS += \
    ../commons/classinterfaces.h \
    ../commons/signalbuffer.h \
    svmsettings.h \
    svmp.h

FORMS += \
    svmsettings.ui

QMAKE_CXXFLAGS += --std=c++14
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast
