#ifndef SVMSETTINGS_H
#define SVMSETTINGS_H

#include <QDialog>
#include <string>
#include <svm.h>

namespace Ui {
class SVMSettings;
}

class SVMSettings : public QDialog
{
	Q_OBJECT

public:
	explicit SVMSettings(QWidget *parent = 0);
	~SVMSettings();

	svm_model* getModel();
	std::string getPath();
	void setPath(const std::string& path);

private slots:
	void accept();
	void on_buttonBox_rejected();
	void on_browse_clicked();

private:
	Ui::SVMSettings *ui;

	QString path;
	svm_model* model = nullptr;
};

#endif // SVMSETTINGS_H
