#ifndef CLASSINTERFACES_H
#define CLASSINTERFACES_H

#include <vector>
#include <utility>
#include <tuple>
#include <QString>
#include <QDialog>
#include <pugixml.hpp>
#include <signalbuffer.h>

class IPluginGraph
{
public:
    virtual ~IPluginGraph() {}

    virtual void show() = 0;
    virtual void reset() {}

    virtual void start() {}
    virtual void stop() {}
    virtual void run() = 0;

    virtual std::pair<
        std::vector<std::pair<std::string, SignalBuffer::Domain>>,
        std::vector<std::pair<std::string, SignalBuffer::Domain>>> getIOProp() = 0;
    virtual SignalBuffer*& operator[](const std::string& s) = 0;
    virtual std::vector<std::pair<std::string, QWidget*>> getWidgets() { 
        return std::vector<std::pair<std::string, QWidget*>>(); }
    virtual void setNotifyStop(std::function<void()> f) { this->notStop = f; }

    virtual void save(pugi::xml_node node) { (void)node; }
    virtual void load(pugi::xml_node node) { (void)node; }

protected:
    std::function<void()> notStop;
};

class IPlugin : public QDialog
{
protected:
    IPlugin(QWidget* parent) : QDialog(parent) {}
    virtual ~IPlugin() {}

public:
    virtual QString getName() = 0;
    virtual IPluginGraph* initGraphPlugin() = 0;

    virtual void save(pugi::xml_node node) { (void)node; }
    virtual void load(pugi::xml_node node) { (void)node; }
};



#endif
