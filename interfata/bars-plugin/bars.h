#ifndef BARS_H
#define BARS_H

#include <classinterfaces.h>
#include <signalbuffer.h>
#include <barssettings.h>

class QWidget;

class BarsPlugin : public IPlugin
{
public:
	BarsPlugin(QWidget* p);
	~BarsPlugin();

	QString getName();
	IPluginGraph* initGraphPlugin();
};

class BarsGraphPlugin : public IPluginGraph
{
public:
	BarsGraphPlugin();
	~BarsGraphPlugin();

	void show();

	void save(pugi::xml_node node);
	void load(pugi::xml_node node);

    std::pair<
        std::vector<std::pair<std::string, SignalBuffer::Domain>>,
        std::vector<std::pair<std::string, SignalBuffer::Domain>>> getIOProp();
    SignalBuffer*& operator[](const std::string& s);
	void run();

private:
	void adaptConfig();

	std::vector<std::pair<std::string, SignalBuffer::Domain>> ins, outs;
	BarsSettings bsettings;

	std::pair<float, float> freqLimits;
	std::vector<float> lastVals;
	std::map<std::string, SignalBuffer*> inputs;
	SignalBuffer* output;
};


#endif
