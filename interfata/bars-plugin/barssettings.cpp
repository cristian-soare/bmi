#include "barssettings.h"
#include "ui_barssettings.h"

using namespace std;

BarsSettings::BarsSettings(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::BarsSettings)
{
	ui->setupUi(this);
	freqLower = 1;
	freqUpper = 300;
	inputs = 2;
}

BarsSettings::~BarsSettings()
{
	delete ui;
}

void BarsSettings::on_buttonBox_accepted()
{
	freqLower = ui->lowerFreqSpin->value();
	freqUpper = ui->upperFreqSpin->value();
	inputs = ui->numberOfInputsSpin->value();
}

void BarsSettings::on_buttonBox_rejected()
{
	ui->lowerFreqSpin->setValue(freqLower);
	ui->upperFreqSpin->setValue(freqUpper);
	ui->numberOfInputsSpin->setValue(inputs);
}

pair<float, float> BarsSettings::getFreqLimits()
{
	return make_pair(freqLower, freqUpper);
}

int BarsSettings::getInputCount()
{
	return inputs;
}

void BarsSettings::setFreqLimits(pair<float, float> freqLimits)
{
	freqLower = freqLimits.first;
	freqUpper = freqLimits.second;
	on_buttonBox_rejected();
}

void BarsSettings::setInputCount(int x)
{
	inputs = x;
	on_buttonBox_rejected();
}
