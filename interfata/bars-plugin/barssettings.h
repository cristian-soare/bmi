#ifndef BARSSETTINGS_H
#define BARSSETTINGS_H

#include <QDialog>

namespace Ui {
class BarsSettings;
}

class BarsSettings : public QDialog
{
	Q_OBJECT

public:
	explicit BarsSettings(QWidget *parent = 0);
	~BarsSettings();

	std::pair<float, float> getFreqLimits();
	int getInputCount();

	void setFreqLimits(std::pair<float, float> freqLimits);
	void setInputCount(int x);

private slots:
	void on_buttonBox_accepted();
	void on_buttonBox_rejected();

private:
	Ui::BarsSettings *ui;
	int freqLower, freqUpper, inputs;
};

#endif // BARSSETTINGS_H
