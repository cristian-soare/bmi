#include "bars.h"

#include <sstream>

using namespace std;

static BarsPlugin* globalPlugin;

extern "C" IPlugin* initPlugin(QWidget* parent)
{
	if (!globalPlugin)
		globalPlugin = new BarsPlugin(parent);
	return globalPlugin;
}

extern "C" void destroyPlugin()
{
	delete globalPlugin;
	globalPlugin = NULL;
}

BarsPlugin::BarsPlugin(QWidget* p)
	: IPlugin(p)
{

}

BarsPlugin::~BarsPlugin()
{

}

QString BarsPlugin::getName()
{
	return QString("Mean frequency v0.1");
}

IPluginGraph* BarsPlugin::initGraphPlugin()
{
	return new BarsGraphPlugin();
}

BarsGraphPlugin::BarsGraphPlugin()
	: outs({ make_pair("output", SignalBuffer::Single) }) {}

BarsGraphPlugin::~BarsGraphPlugin() {}

void BarsGraphPlugin::show()
{
	bsettings.exec();
	adaptConfig();
}

void BarsGraphPlugin::adaptConfig()
{
	ins.clear();
	for (int i = 0; i < bsettings.getInputCount(); i++) {
		ostringstream oss;
		oss << "input" << i;
		ins.push_back(make_pair(oss.str(), SignalBuffer::Frequency));
	}
	freqLimits = bsettings.getFreqLimits();
	lastVals = vector<float>(ins.size(), 0);
}

pair<vector<pair<string, SignalBuffer::Domain>>, vector<pair<string, SignalBuffer::Domain>>> BarsGraphPlugin::getIOProp()
{
	return make_pair(ins, outs);
}

void BarsGraphPlugin::run()
{
	int inputIdx = 0;
	for (auto in : inputs) {
		SignalBuffer& v = *in.second;

		int size = v.getSize();
		for (int i = 1; i < size; i++) {
			float sum = 0;
			int count = 0;

			for (; i < size && v[i].first > v[i - 1].first; i++)
				if (freqLimits.first < v[i].first && v[i].first < freqLimits.second) {
					sum += v[i].second;
					count++;
				}

			if (count) {
				sum /= count;
				sum -= (sum - lastVals[inputIdx]) * 0.8;
				lastVals[inputIdx] = sum;
				output->add(make_pair(inputIdx, sum));
			}
		}

		inputIdx++;
	}
}

SignalBuffer*& BarsGraphPlugin::operator[](const string& s)
{
	if (s.find("input") == 0)
		return inputs[s];
	return output;
}

void BarsGraphPlugin::save(pugi::xml_node node)
{
	node.append_attribute("freqLower") = freqLimits.first;
	node.append_attribute("freqUpper") = freqLimits.second;
	node.append_attribute("inputCount") = static_cast<int>(ins.size());
}

void BarsGraphPlugin::load(pugi::xml_node node)
{
	bsettings.setFreqLimits(make_pair(node.attribute("freqLower").as_int(), node.attribute("freqUpper").as_int()));
	bsettings.setInputCount(node.attribute("inputCount").as_int());
	adaptConfig();
}
