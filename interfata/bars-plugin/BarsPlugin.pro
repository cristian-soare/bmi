QT += core gui widgets printsupport

TARGET = bars
TEMPLATE = lib
CONFIG += debug

INCLUDEPATH += ../commons

SOURCES += bars.cpp barssettings.cpp
HEADERS += ../commons/generalbuffer.h ../commons/signalbuffer.h bars.h barssettings.h
FORMS += barssettings.ui

QMAKE_CXXFLAGS += --std=c++14
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -Ofast
