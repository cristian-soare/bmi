#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <math.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <signal.h>

#include <errno.h>
#include <unistd.h>
#include <sys/time.h>

#define SPI_CHANNEL 0
#define SPI_SPEED 4000000

#define BUFF_LEN 8000
#define WAIT_MICROS 10000
#define PORT 5000

#define CS_PIN 2
#define DRDY_PIN  0

/*define command bits, first 3 bits count*/
#define CMD_CH_READ_DIR 0b00010000
#define CMD_CH_READ_REG 0b00110000 /*1 MUL must by 1 */
#define CMD_RG_READ     0b01000000
#define CMD_RG_WRITE    0b01100000
#define CMD_RESET       0b11000000 /* 0b110xxxxx,  x=rest don't care */

#define CONFIG0 0x00 /* 0 SPIRST MUXMOD BYPAS CLKENB CHOP STAT 0 */ /* 0000001010 */
#define CONFIG1 0x01 /* IDLMOD DLY2 DLY1 DLY0 SBSC1 SBSC0 DRATE1 DRATE0 */
#define MUXSCH  0x02 /* AINP3 AINP2 AINP1 AINP0 AINN3 AINN2 AINN1 AINN0 */
#define MUXDIF  0x03 /* DIFF7 DIFF6 DIFF5 DIFF4 DIFF3 DIFF2 DIFF1 DIFF0 */
#define MUXSG0  0x04 /* AIN7 AIN6 AIN5 AIN4 AIN3 AIN2 AIN1 AIN0 */ /* 11111111 */
#define MUXSG1  0x05 /* AIN15 AIN14 AIN13 AIN12 AIN11 AIN10 AIN9 AIN8 */
#define SYSRED  0x06 /* 0 0 REF GAIN TEMP VCC 0 OFFSET */
#define GPIOC   0x07 /* CIO7 CIO6 CIO5 CIO4 CIO3 CIO2 CIO1 CIO0 */
#define GPIOD   0x08 /* DIO7 DIO6 DIO5 DIO4 DIO3 DIO2 DIO1 DIO0 */
#define ID      0x09 /* ID7 ID6 ID5 ID4 ID3 ID2 ID1 ID0 */

#define GRAPH

int listen_connect(void)
{
	int listenfd = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int));
	
	struct sockaddr_in serv_addr;
	memset((void*)&serv_addr, 0, sizeof(struct sockaddr_in));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(PORT);

	bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(struct sockaddr_in));
	printf("listening\n");
	listen(listenfd, 10);

	int sockfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
	close(listenfd);
	return sockfd;
}

struct data_acquiring {
	int sockfd, ongoing;
	pthread_mutex_t mut;
        struct timeval t;
};

static struct data_acquiring dacq;

void set_direct_read(int ok)
{
    digitalWrite(CS_PIN, HIGH);
//     usleep(10);
    digitalWrite(CS_PIN, LOW);
    
    if (ok) {
        unsigned char x[] = { 0xff, 0xff, 0xff, 0xff };
        wiringPiSPIDataRW(SPI_CHANNEL, x, 4);
    }
}

unsigned char read_reg(unsigned char reg)
{
	unsigned char cmd[] = { reg | CMD_RG_READ, 0 };
	wiringPiSPIDataRW(SPI_CHANNEL, cmd, 2);
	printf("reading %d %d\n", reg, cmd[1]);
	return cmd[1];
}

void write_reg(unsigned char add, unsigned char val)
{
	unsigned char cmd[] = { add | CMD_RG_WRITE, val };
	wiringPiSPIDataRW(SPI_CHANNEL, cmd, 2);
	printf("writing %d %d\n", add, val);
}

int timer = 0;
// unsigned char cacat[4];

void* proba(void* asd)
{
    waitForInterrupt(DRDY_PIN, -1);
    timer++;
    return NULL;
}

void acquire_data(void)
{  
    static unsigned char buff[BUFF_LEN];
    static unsigned int i = 0;
    static sig_atomic_t ok = 0;
    
    if (ok) {
        ok = 0;
        return;
    }
    
//     pthread_mutex_lock(&dacq.mut);
//     if (!dacq.ongoing)
//         goto release;

//     unsigned char asd[5];
//     asd[0] = CMD_CH_READ_REG;
//     wiringPiSPIDataRW(SPI_CHANNEL, asd, 5);
//     memcpy(buff + sizeof(int) + i, asd + 1, 4);
    buff[sizeof(int) + i] = 0;
    wiringPiSPIDataRW(SPI_CHANNEL, buff + sizeof(int) + i, 4);
    
        struct timeval t2;
        gettimeofday(&t2, NULL);
//         
        int micros = (t2.tv_sec - dacq.t.tv_sec) * 100000;
        micros += (t2.tv_usec - dacq.t.tv_usec) / 10;
        *(int*)(buff + i) = micros;
        if (buff[sizeof(int) + i] == 150)
            i += sizeof(int) + 4;
        
         timer++;
        
        if (i == BUFF_LEN) {
            i = 0;
//             digitalWrite(CS_PIN, HIGH);
            ok = 1;
#ifdef GRAPH
            int tosend = BUFF_LEN;
            while ((tosend -= send(dacq.sockfd, buff + BUFF_LEN - tosend, tosend, 0)));
#else
            for (int j = 0; j < BUFF_LEN; j += 8)
                printf("%d %d %d %d %d\n", *(int*)(buff + j), buff[j + 4], buff[j + 5], buff[j + 6], buff[j + 7]);
            printf("\n");
#endif
//             digitalWrite(CS_PIN, LOW);
        }
}

#define SYNC 0xA0
#define READ 0xA1
#define WRITE 0xA2
#define ENDP 0xA3
#define LCMD 4

unsigned char get_checksum(unsigned char* buff, int len)
{
    unsigned char b = 0;
    for (int i = 0; i < len - 1; i++)
        b ^= buff[i];
    return b;
}

void reset(void)
{
    unsigned char cmd = CMD_RESET;
    wiringPiSPIDataRW(SPI_CHANNEL, &cmd, 1);
    usleep(WAIT_MICROS);
}

void change_channels(unsigned char m1, unsigned char m0)
{
    write_reg(MUXSG0, m0);
    usleep(WAIT_MICROS);

    write_reg(MUXSG1, m1);
    usleep(WAIT_MICROS);
}

void init_ads(void)
{
    pinMode(CS_PIN, OUTPUT);
    digitalWrite(CS_PIN, LOW);
    
    reset();
    write_reg(CONFIG0, 0b00001010);
    write_reg(CONFIG1, 0b10000011);
    
    write_reg(MUXSCH, 1);
    change_channels(0b01000000, 0b000000000);
    usleep(WAIT_MICROS);
}

void receive_commands(struct data_acquiring* dacq)
{
    unsigned char resp[LCMD] = { SYNC, 1, 1, 1 };

    int ok = 1;
    while (ok) {
            unsigned char v[4];
            if (recv(dacq->sockfd, v, LCMD, 0) < LCMD)
                    continue;

            pthread_mutex_lock(&dacq->mut);
            usleep(300);
            switch (v[0]) {
            case READ:
                    resp[1] = v[1];
                    resp[2] = read_reg(v[1]);
                    resp[3] = get_checksum(resp, LCMD);

                    send(dacq->sockfd, resp, LCMD, 0);
                    break;

            case WRITE:
                    write_reg(v[1], v[2]);
                    break;

            case ENDP:
                    ok = 0;
                    break;

            default:
                    break;
            }
            usleep(300);
            pthread_mutex_unlock(&dacq->mut);
    }
}

int main(int argc, char* argv[])
{
    wiringPiSetup();
    wiringPiSPISetup(SPI_CHANNEL, SPI_SPEED);
    piHiPri(0);
     
    init_ads();
    gettimeofday(&dacq.t, NULL);
    
    for (int i = 0; i < 6; i++)
        printf("citit %d\n", read_reg(i));
    
//     pthread_t thr;
//     pthread_create(&thr, NULL, proba, NULL);
//     set_direct_read(1);
    
#ifndef GRAPH
    dacq.ongoing = 1;
            wiringPiISR(DRDY_PIN, INT_EDGE_FALLING, acquire_data);
    while (1) {
        printf("%d\n", timer);
        timer = 0;
        usleep(1000000);
    }
#endif
//     
    while (1) {
            dacq.sockfd = listen_connect();
            dacq.ongoing = 1;
            set_direct_read(1);
            wiringPiISR(DRDY_PIN, INT_EDGE_FALLING, acquire_data);
            pthread_mutex_init(&dacq.mut, NULL);

            printf("connected\n");

            receive_commands(&dacq);

            printf("done\n");
            dacq.ongoing = 0;

            unsigned char cmd[] = { SYNC, 0, 0, 0 };
            printf("trimit en\n");
            cmd[3] = get_checksum(cmd, LCMD);
            send(dacq.sockfd, cmd, LCMD, 0);
            pthread_mutex_destroy(&dacq.mut);
            close(dacq.sockfd);
    }
    return 0;
}
